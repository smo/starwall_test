! ----------------------------------------------------------------------
      subroutine read_2d_coil_data
! ----------------------------------------------------------------------
!
!     purpose:
!
!
! ----------------------------------------------------------------------
       use icontr
       use contr_su
       use fila
! ----------------------------------------------------------------------
      implicit none
      integer                          :: i,j,k,k1,kc
      integer                          :: ier
      real                             :: pi2,alv,si,co
      real                             :: r1,r2,r3,r4,z1,z2,z3,z4
      character(80)                    :: txt
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
!     real coil data
!
    open(unit=60,file='coil.txt',status='old')
       read(60,*) txt 
       read(60,*) n_points
       read(60,*) txt 
       read(60,*) txt 
       read(60,*) ncoil,nel
       read(60,*) txt 
       read(60,*) txt 
     allocate(rco(ncoil),zco(ncoil),dr(ncoil),dz(ncoil),current(ncoil)     & 
             ,nufila(ncoil))
      do i=1,ncoil
       read(60,*) rco(i),zco(i),dr(i),dz(i),current(i),nufila(i)
      enddo
    close(60)
! ----------------------------------------------------------------------
       pi2 = 4.*asin(1.)
       nu_coil =0
     do i=1,ncoil
       if(current(i).ne.0.) then 
        nu_coil = nu_coil+nufila(i)
       endif
     enddo
   allocate(cur(nu_coil))
          j=0
    do i=1,ncoil
      if(current(i).ne.0.) then 
       do k=1,nufila(i)
           j =j +1
          cur(j) = current(i)/nufila(i) 
     enddo
       endif
     enddo
  allocate(x_fila(nel,nu_coil,2),y_fila(nel,nu_coil,2),z_fila(nel,nu_coil,2))
       alv = pi2/float(nel-1)
         kc=0
      do i=1,ncoil
       if(current(i).ne.0.) then
        do k=1,nufila(i)
            kc =kc +1
           r1 = rco(i)
           r2 = rco(i)
           z1 = zco(i)+.5*dz(i) -(k-1)*dz(i)/nufila(i)
           z2 = zco(i)+.5*dz(i) -(k  )*dz(i)/nufila(i)
         do j=0,nel-1
           co = cos(alv*j)
           si = sin(alv*j)
           x_fila(j+1,kc,1) = r1*co
           y_fila(j+1,kc,1) = r1*si
           z_fila(j+1,kc,1) = z1
           x_fila(j+1,kc,2) = r2*co
           y_fila(j+1,kc,2) = r2*si
           z_fila(j+1,kc,2) = z2
         enddo
        enddo
       endif
      enddo
    close(60)
     end subroutine  read_2d_coil_data 
