!----------------------------------------------------------------------
subroutine real_space2bezier(bez,f,N_bnd,n_points,ndim)
! ----------------------------------------------------------------------
!   purpose:                                                  12/08/09
!
! ----------------------------------------------------------------------
     implicit none
     integer                              :: N_bnd,n_points,ndim
      real,dimension(2*N_bnd,       ndim) :: bez 
      real,dimension(N_bnd*n_points,ndim) :: f 
! ----------------------------------------------------------------------
      real,dimension(2*N_bnd,2*N_bnd)     :: a,t 
      real,dimension(2*N_bnd,   ndim)     :: b 
      real,dimension(2,2)                 :: H,H_s,H_ss 
      real,dimension(n_points )           :: h11,h12,h21,h22
      real                                :: s,fnp,a0,a1,a2,a3,a4
      integer                             :: i,idim,im,im2,ip,ip2,j,k,nd
      integer                             :: i1,i2,i3,i4,i5,i6,info
! ----------------------------------------------------------------------
          a0 =      26./35.
          a1 =       6./35. 
          a2 =   .5-13./35.
          a3 =  .25-12./35.
          a4 =      -9./(4.*35.)
           a = 0.
         do i=1,N_bnd
            i1 = 2*i-1
            i2 = i1 + 1
            i3 = mod(i1+1,         2*N_bnd) + 1
            i4 = i3 + 1
            i5 = mod(i1+2*N_bnd -3,2*N_bnd) + 1
            i6 = i5 + 1
           a(i1,i1) =  a0
           a(i1,i3) =  a2
           a(i1,i4) =  a3
           a(i1,i5) =  a2
           a(i1,i6) = -a3
                       
           a(i2,i2) =  a1
           a(i2,i3) = -a3
           a(i2,i4) =  a4
           a(i2,i5) =  a3
           a(i2,i6) =  a4
           enddo 
                  t = 0.
           do  i=1,2*N_bnd
             t(i,i) = 1.
           enddo
                nd  = 2*N_bnd
!      call  dpof('U',a,nd,nd)
!      call dposm('U',a,nd,nd,t,nd,nd)
      call dpotrf('U',nd,a,nd,info)
      call dpotrs('U',nd,nd,a,nd,t,nd,info)
! ----------------------------------------------------------------------
         b  = 0.
       fnp  = 1./float(n_points)
      do j=1,n_points
         s = float(j-1)/float(n_points)
       call basisfunctions1(s,H,H_s,H_ss)
         h11(j) =  H(1,1)*fnp
         h12(j) =  H(1,2)*fnp
         h21(j) =  H(2,1)*fnp
         h22(j) = -H(2,2)*fnp
      enddo
      do i = 1, N_bnd
          im  = 2*i -1
          ip  = im + 1   
          im2 = mod(im+1,2*N_bnd) + 1
          ip2 = im2 + 1
       do k=1,n_points
           j = k+n_points*(i-1)
        do idim=1,ndim
             b(im, idim) =  b(im, idim) + h11(k)*f(j,idim)
             b(im2,idim) =  b(im2,idim) + h21(k)*f(j,idim)
             b(ip, idim) =  b(ip, idim) + h12(k)*f(j,idim)
             b(ip2,idim) =  b(ip2,idim) + h22(k)*f(j,idim)
         enddo
        enddo
       enddo
         bez = 0.
       do i=1,2*N_bnd
        do k=1,2*N_bnd
         do idim=1,ndim
            bez(i,idim) = bez(i,idim) + t(i,k)*b(k,idim) 
         enddo
        enddo
       enddo
end subroutine real_space2bezier
