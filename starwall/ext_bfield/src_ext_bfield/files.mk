F_MAIN_SRC = mod_coil2d.f90\
             mod_icontr.f90\
             mod_fila.f90\
             mod_gauss.f90\
             mod_contr_su.f90\
             control_boundary.f90\
             read_2d_coil_data.f90\
             basisfunctions1.f90\
             read_coils.f90\
             coil_2d.f90\
             bfield_c.f90\
             real_space2bezier.f90\
             matrix.f90\
             main.f90
 
F_MAIN_OBJ = $(addprefix $(OBJ_DIR)/,$(F_MAIN_SRC:.f90=.o))

MAIN_OBJ = $(F_MAIN_OBJ) 

########################################################################
#     D E P E N D E N C I E S                                          #
########################################################################
$(OBJ_DIR)/read_coils.o: \
            $(OBJ_DIR)/mod_coil2d.o 
$(OBJ_DIR)/bfield_coil_2d..o: \
            $(OBJ_DIR)/mod_coil2d.o 
$(OBJ_DIR)/control_boundary.o: \
            $(OBJ_DIR)/mod_icontr.o $(OBJ_DIR)/mod_contr_su.o\
            $(OBJ_DIR)/mod_gauss.o
$(OBJ_DIR)/matrix.o: \
            $(OBJ_DIR)/mod_icontr.o $(OBJ_DIR)/mod_contr_su.o\
            $(OBJ_DIR)/mod_coil2d.o $(OBJ_DIR)/mod_fila.o
$(OBJ_DIR)/read_2d_coil_data.o: \
            $(OBJ_DIR)/mod_icontr.o $(OBJ_DIR)/mod_contr_su.o\
            $(OBJ_DIR)/mod_fila.o 
