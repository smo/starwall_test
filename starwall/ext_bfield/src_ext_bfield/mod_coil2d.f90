module coil2d
   implicit none
   integer                            :: ntri_c,ncoils
   integer,dimension(:  ),allocatable :: jtri_c
   real   ,dimension(:,:,:),allocatable :: x_coil,y_coil,z_coil,phi_coil
   real   ,dimension(:  ),allocatable :: jcoil
end module coil2d
