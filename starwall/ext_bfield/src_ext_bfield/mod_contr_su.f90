module contr_su

   implicit none
   integer                           :: n_points,nu,N_bnd
   real,dimension(:,:,:),allocatable :: Lij
   real,dimension(    :),allocatable :: Rc,Zc,Rc_s,Zc_s,su
   real,dimension(    :),allocatable :: x,y,z,xu,yu,zu
   real,dimension(  2,2)             :: H, H_s, H_ss

end module contr_su
