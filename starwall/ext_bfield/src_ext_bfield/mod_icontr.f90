module  icontr
   implicit none
   integer,parameter :: inp=5, outp=6
   integer           :: n_harm,nd_harm,nd_harm0
   integer           :: nv,iwall,nwall
   integer           :: ipote, i_response
   real              :: coilres,delta
   integer,dimension(:),allocatable :: n_tor
   real,   dimension(:),allocatable :: wallres
end module icontr
