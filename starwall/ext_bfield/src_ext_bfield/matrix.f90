! ----------------------------------------------------------------------
subroutine matrix
! ----------------------------------------------------------------------
!     purpose:                                              15/09/10
!
! ----------------------------------------------------------------------
      use icontr
      use contr_su
      use coil2d
      use fila
! ----------------------------------------------------------------------
      implicit none
! ----------------------------------------------------------------------
      real,dimension(2*(nel-1),      3) ::  xp,yp,zp,phi
      real,dimension(nu       ,nu_coil) ::  b_normal,b_parall
      real,dimension(nu       ,  ncoil) ::  bc_n,bc_p
      real,dimension(2*N_bnd  ,  ncoil) ::  bezbc_n,bezbc_p
      real,dimension(               nu) ::  bx,by,bz,xx,yy,zz
      integer                           :: i,ier,j,k,ne2,kc
! ----------------------------------------------------------------------
     write(6,*) 'compute matrix'
       do i=1,nu 
         xx(i)=x(i)
         yy(i)=y(i)
         zz(i)=z(i)
        enddo
        ne2 = 2*(nel-1)
      do j=1,nu_coil
        do i=1,ne2
         do k=1,3
            xp(i,k) =   x_coil(i,k,j)
            yp(i,k) =   y_coil(i,k,j)
            zp(i,k) =   z_coil(i,k,j)
           phi(i,k) = phi_coil(i,k,j)
         enddo
        enddo
       call bfield_c(xx,yy,zz,bx,by,bz,nu,xp,yp,zp,phi,ne2)
      do i=1,nu
           b_normal(i,j) = bx(i)*zu(i)-bz(i)*xu(i)
           b_parall(i,j) = bx(i)*xu(i)+bz(i)*zu(i)
      enddo
      enddo
         kc =0
         bc_n = 0.
         bc_p = 0.
      do j=1,ncoil
       do k=1,nufila(j)
          kc=kc+1     
!        write(6,*) j,kc,cur(kc)
        do i=1,nu
             bc_n(i,j) = bc_n(i,j) + b_normal(i,kc)
             bc_p(i,j) = bc_p(i,j) + b_parall(i,kc)
        enddo
       enddo
      enddo
      
     call real_space2bezier(bezbc_n,bc_n,N_bnd,n_points,ncoil)
     call real_space2bezier(bezbc_p,bc_p,N_bnd,n_points,ncoil)

!-------------------------------------------------------------------------

  open(60,file='response_bext_normal',form="formatted",iostat=ier)
            write(60,*) 'boundary elements','  coils'
            write(60,'(2i12)') N_bnd,ncoil
      do k=1,2*N_bnd
       do j=1,ncoil
        write(60,'(2i6,1pe14.6)') k,j,bezbc_n(k,j)
       enddo
      enddo
  close(60)
  open(60,file='response_bext_parall',form="formatted",iostat=ier)
            write(60,*) 'boundary elements','  coils'
            write(60,'(2i12)') N_bnd,ncoil
      do k=1,2*N_bnd
       do j=1,ncoil
        write(60,'(2i6,1pe14.6)') k,j,bezbc_p(k,j)
       enddo
      enddo
  close(60)

!--------------------------------------------------------------------------
   end subroutine matrix
