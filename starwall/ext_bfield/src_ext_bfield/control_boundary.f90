subroutine control_boundary
!------------------------------------------------------------------------------
   use icontr
   use gauss
   use contr_su
!------------------------------------------------------------------------------
 implicit none
 real,dimension(:,:,:),allocatable :: Rij, Zij
 real                              :: pi2,alv,fnv,fnp,sq,co,si,s
 integer                           :: i, j, k, l, iv1, iv2,index,ibnd,ku,kv,nuv
!------------------------------------------------------------------------------
 open(22,file='boundary.txt')
 
 read(22,*) n_bnd


 allocate(Lij(2,2,n_bnd),Rij(2,2,n_bnd), Zij(2,2,n_bnd))

!----- Rij  : first index counts the two nodes of a boundary element
!             second index counts the basis function H
!             third index counts the number of the  boundary element

 do i=1,n_bnd

  read(22,*) ibnd,iv1,iv2, &
    Rij(1,1,i), Zij(1,1,i), Rij(1,2,i), Zij(1,2,i), Lij(1,1,i), Lij(1,2,i),   &
    Rij(2,1,i), Zij(2,1,i), Rij(2,2,i), Zij(2,2,i), Lij(2,1,i), Lij(2,2,i)   
			   					
 enddo

 close(22)


! example reconstruction of the boundary
      nu  = n_bnd*n_points
 allocate(Rc  (nu),Zc  (nu))
 allocate(Rc_s(nu),Zc_s(nu))
   delta =0.
 do i=1,n_bnd
  
  do j=1, n_points
   
    index = (i-1)*n_points + j
   
    s = (j-1)/float(n_points)
    
    call basisfunctions1(s,H,H_s,H_ss)    
    
    Rc  (index) = 0
    Zc  (index) = 0
    Rc_s(index) = 0
    Zc_s(index) = 0
    
    do k=1,2      ! contribution from the two nodes from a boundary element

      do l=1,2

        Rc  (index) = Rc  (index) + Rij(k,l,i) * Lij(k,l,i) *   H(k,l)
        Zc  (index) = Zc  (index) + Zij(k,l,i) * Lij(k,l,i) *   H(k,l)
	
        Rc_s(index) = Rc_s(index) + Rij(k,l,i) * Lij(k,l,i) * H_s(k,l)
        Zc_s(index) = Zc_s(index) + Zij(k,l,i) * Lij(k,l,i) * H_s(k,l)
      enddo
    
    enddo

  enddo

 enddo
        
        pi2 = 4.*asin(1.)
         nv = 2
        fnv = 1./float(nv)
        fnp = 1./float(n_points)
        alv = pi2*fnv
        nuv = nu*nv
      allocate(x(nuv),y(nuv),z(nuv),xu(nuv),yu(nuv),zu(nuv))
       do kv=1,nv
            co = cos(alv*(kv-1))
            si = sin(alv*(kv-1))
        do ku=1,nu
             j = ku+nu*(kv-1)
            sq= 1./sqrt(Rc_s(ku)**2+Zc_s(ku)**2)
          x(j) = Rc(ku)*co
          y(j) = Rc(ku)*si
          z(j) = Zc(ku) 
         xu(j) = sq*Rc_s(ku)*co
         yu(j) = sq*Rc_s(ku)*si
         zu(j) = sq*Zc_s(ku)
        enddo
        enddo
 deallocate(Zij, Rij)
  write(outp,*) 'Control_surface'
  write(outp,*) '---------------'
  write(outp,*) 'number of boundary elements                     N_bnd= ',N_bnd
  write(outp,*) 'number of grid points per boundary element:  n_points= ',n_points
  write(outp,*) 'number of poloidal grid points nu= N_bnd*n_points: nu= ',nu
end subroutine control_boundary
