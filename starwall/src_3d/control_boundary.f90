subroutine control_boundary
!------------------------------------------------------------------------------
  use icontr
  use contr_su
 
  use mpi_v
  use mpi
!------------------------------------------------------------------------------
implicit none
real,dimension(:,:,:),allocatable :: Rij, Zij
real                              :: pi2,alv,fnv,fnp,sq,sq1,co,si,s
integer                           :: i, j, k, l, index,ibnd,ku,kv,nuv
integer                           :: j_exists
logical                           :: exists

!------------------------------------------------------------------------------
!Only master task open and read data
if(rank==0) open(22,file='boundary.txt')
if(rank==0) read(22,*) n_bnd, n_bnd_nodes        ! the number of boundary elements, nodes
  
  !==================================================================  
  call MPI_BCAST(n_bnd,       1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(n_bnd_nodes, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  !==================================================================

allocate(Lij(2,2,n_bnd),Rij(2,2,n_bnd), Zij(2,2,n_bnd), bnd_node(n_bnd,2),stat=ier)
        if (ier /= 0) then
             print *,'cntrol_boundary.f90: Can not allocate local arrays Lij, ...,Roj, rank=',rank
             stop
        endif


!----- Rij  : first index counts the two nodes of a boundary element
!             second index counts the basis function H
!             third index counts the number of the  boundary element

!Only master task read data
if(rank==0) then
   do i=1,n_bnd
        read(22,*) ibnd, bnd_node(i,1), bnd_node(i,2), &
             Rij(1,1,i), Zij(1,1,i), Rij(1,2,i), Zij(1,2,i), Lij(1,1,i), Lij(1,2,i), &
             Rij(2,1,i), Zij(2,1,i), Rij(2,2,i), Zij(2,2,i), Lij(2,1,i), Lij(2,2,i)
   enddo
endif

!variable ibnd is not used in the rest part of the code 
!===============================================================================
call MPI_BCAST(bnd_node, n_bnd*2,   MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
call MPI_BCAST(Rij,      n_bnd*2*2, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
call MPI_BCAST(Zij,      n_bnd*2*2, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
call MPI_BCAST(Lij,      n_bnd*2*2, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
!===============================================================================

allocate(bnd_node_index(n_bnd_nodes,2),bnd_node_ndof(n_bnd_nodes),stat=ier)
  if (ier /= 0) then
         print *,'cntrol_boundary.f90: Can not allocate local arrays bnd_node_index, rank=',rank
         stop
  endif


if(rank==0) then
   do i=1,n_bnd_nodes
        read(22,*) j,bnd_node_index(i,:),bnd_node_ndof(i)! the index in the starwall response (as requested by JOREK)
   enddo 
endif
if(rank==0) close(22)


!===============================================================================
call MPI_BCAST(bnd_node_index, n_bnd_nodes*2, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
call MPI_BCAST(bnd_node_ndof,  n_bnd_nodes,   MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
!===============================================================================

n_dof_bnd = 0    ! total number of degrees of freedom (Bezier coefficients) on the boundary

do i=1,n_bnd_nodes
  exists = .false.
  do j=1, i-1
    if (bnd_node_index(i,1) .eq. bnd_node_index(j,1)) then
      exists = .true.
      j_exists = j
      exit
    endif
  enddo
  if (.not. exists) then
    n_dof_bnd = n_dof_bnd + 2
  else
    n_dof_bnd = n_dof_bnd + 1
  endif
enddo

if(rank==0) write(*,*) 'total number of degrees of freedom on boundary : ',n_dof_bnd,maxval(bnd_node_index)
if(rank==0) write(*,*) 'total number of nodes on the boundary          : ',n_bnd_nodes


! example reconstruction of the boundary
nu  = n_bnd*n_points                ! n_bnd : number of boundary elements from JOREK, npoints per element
pi2 = 4.*asin(1.)
fnv = 1./float(nv)
fnp = 1./float(n_points)
alv = pi2*fnv
nuv = nu*nv

allocate(Rc(nu),Zc(nu),Rc_s(nu),Zc_s(nu),x(nuv),y(nuv),z(nuv),xu(nuv),yu(nuv),zu(nuv),stat=ier)
  if (ier /= 0) then
         print *,'cntrol_boundary.f90: Can not allocate local arrays Rc,... rank=',rank
         stop
  endif


do i=1,n_bnd

  do j=1, n_points

    index = (i-1)*n_points + j
    s = (j-1)/float(n_points)

    call basisfunctions1(s,H,H_s,H_ss)

    Rc  (index) = 0
    Zc  (index) = 0
    Rc_s(index) = 0
    Zc_s(index) = 0

    do k=1,2    ! contribution from the two nodes from a boundary element

      do l=1,2

        Rc  (index) = Rc  (index) + Rij(k,l,i) * Lij(k,l,i) *   H(k,l)
        Zc  (index) = Zc  (index) + Zij(k,l,i) * Lij(k,l,i) *   H(k,l)	
        Rc_s(index) = Rc_s(index) + Rij(k,l,i) * Lij(k,l,i) * H_s(k,l)
        Zc_s(index) = Zc_s(index) + Zij(k,l,i) * Lij(k,l,i) * H_s(k,l)

      enddo

    enddo

  enddo

enddo

do kv=1,nv
  co = cos(alv*(kv-1))
  si = sin(alv*(kv-1))
  do ku=1,nu
    j = ku+nu*(kv-1)
    sq1= 1./sqrt(Rc_s(ku)**2+Zc_s(ku)**2)
    sq = delta*sq1
    x(j) = (Rc(ku) + sq*Zc_s(ku))*co
    y(j) = (Rc(ku) + sq*Zc_s(ku))*si
    z(j) =  Zc(ku) - sq*Rc_s(ku)
    xu(j) = sq1*Rc_s(ku)*co
    yu(j) = sq1*Rc_s(ku)*si
    zu(j) = sq1*Zc_s(ku)
  enddo
enddo

deallocate(Zij, Rij)

if(rank==0) write(*,*) 'control_surface'
if(rank==0) write(*,*) '---------------'
if(rank==0) write(*,*) 'number of boundary elements                     N_bnd= ',N_bnd
if(rank==0) write(*,*) 'number of grid points per boundary element:  n_points= ',n_points
if(rank==0) write(*,*) 'number of poloidal grid points nu= N_bnd*n_points: nu= ',nu
if(rank==0) write(*,*) 'number of toroidal grid points:                    nv= ',nv
if(rank==0) write(*,*) ' '
if(rank==0) write(*,*) 'B_par or Phi is computed on a surface shifted by delta' 
if(rank==0) write(*,'(" into the vacuum domain - delta= ",f8.3)') delta

end subroutine control_boundary
