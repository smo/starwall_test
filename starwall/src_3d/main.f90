program  starwall
! ----------------------------------------------------------------------
   use icontr
   use mpi_v
   use mpi
   use coils
   use resistive
   use tri_p
   use tri_w
   use time

   implicit none
   
   ! Initizlization of MPI and Scalapack process grid
   call MPI_and_Scalapack_Init

   atime(2)=MPI_WTIME() 
   ! --- Read input parameters from namelist file.
   call input

   ! --- Read external coils
   if ( coils_present ) then
     call get_coil_data(pol_coil_file, rmp_coil_file, voltage_coil_file, diag_coil_file, rank)
     call export_coil_triangles(rank, vtk=.true., ascii=.true.)
     call cleanup_coil_module(rank)
   end if
   
   ! --- Read the JOREK control boundary.
   call control_boundary
 

   atime(3)=MPI_WTIME()
   if(rank==0) write(*,*) 'All Input=',(atime(3)-atime(2)), '  seconds test'
 

  ! --- Generation of the control surface triangles
   call tri_contr_surf
  
   ! --- Discretization of the wall (generate wall triangles manually)
   if ((nwall .gt. 0) .and. (iwall .eq. 1)) then 
     call surface_wall
   end if

   if ( (rank==0) .and. (format_type=='formatted') .and. (max(ntri_w,ntri_p)>40000) ) then
     write(*,*) 'ERROR: Matrices would be too large for ASCII output.'
     stop
   end if
   


   ! --- Read wall triangles
   !if(nwall.gt.0.and.iwall.eq.2)  then 
   !call read_wall_triangles
   !end if
   if (rank==0) call wall2vtk
   ! --- Solve Laplace equation Delta Phi = 0 as a variational problem to determine
   !    the unkowns (current potentials Phi on wall triangle vertices)
   !    Boundary conditions: B_perp on control surface from JOREK
   !                         B_perp on wall is zero (ideal conductor)

   ! Check if the amount of MPI tasks more than array dimencions
   call control_array_distribution

   ! Roughly predict amout of GB needed for the calculation and output
   ! starwall-response file
   call memory_prediction


    atime(4)=MPI_WTIME()
    if(rank==0) write(*,*) 'All surface_wall=',(atime(4)-atime(3)), '  seconds'


   ! Main solver
   call solver

   ! --- Write out the response matrices
   if (i_response .le. 1) then
       !call ideal_wall_response
   endif
   if (i_response .eq. 2) then
     call resistive_wall_response
   endif

   call MPI_FINALIZE(ier)

end program starwall


