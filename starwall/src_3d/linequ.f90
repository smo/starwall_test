!----------------------------------------------------------------------
subroutine linequ(a,n,rs,nrhs,info)
!----------------------------------------------------------------------
!
!     purpose:
!
!
! ---------------------------------------------------------------------
      implicit none
      real,dimension(n,n   ) :: a
      real,dimension(n,nrhs) :: rs
      real,dimension(n) ::  aux 
      integer,dimension(n)   :: ipvt
      integer                :: n,nrhs,info,i,k
      character              :: transa
      real    :: rcond,det
!----------------------------------------------------------------------
      write(6,*) 'do linequ'
      transa = 'N'
!     call dgefcd(a,n,n,ipvt,2,rcond,det,aux,n)
!     write(6,*) rcond,det
!     stop
      call dgetrf(n,n,a,n,ipvt,info)
      write(6,*) 'dgetrf done info=',info
      call dgetrs(transa,n,nrhs,a,n,ipvt,rs,n,info)
      write(6,*) 'dgetrs done info=',info
!---------------------------------------------------
      write(6,*) 'linequ  done'
end subroutine linequ 
