subroutine write_response
use resistive

!-----------------------------------------------------------------------
implicit none
!-----------------------------------------------------------------------

if (format_type == 'formatted' ) then
   call write_response_formatted
else 
   call write_response_unformatted
endif

end subroutine write_response
