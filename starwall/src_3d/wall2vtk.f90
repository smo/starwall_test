subroutine wall2vtk

  use mpi_v
  use tri_w
  
  implicit none 
  140 format(a) 
  141 format(a,i8,a)
  142 format(3es24.16)
  143 format(a,2i8)
  144 format(4i8)
  integer :: i, j 
  real, allocatable :: xyzpot_w(:,:)
  allocate( xyzpot_w(npot_w,3) )
  do i = 1, ntri_w
    do j = 1, 3
      xyzpot_w(jpot_w(i,j),:) = (/ xw(i,j), yw(i,j), zw(i,j) /)
    end do
  end do

  open(60, file='wall.vtk')
  write(60,140) '# vtk DataFile Version 2.0'
  write(60,140) 'testdata'
  write(60,140) 'ASCII'
  write(60,140) 'DATASET POLYDATA'
  write(60,141) 'POINTS', npot_w, ' float'
  do i = 1, npot_w
    write(60,142) xyzpot_w(i,:)
  end do
  write(60,143) 'POLYGONS', ntri_w, ntri_w*4
  do i = 1, ntri_w
    write(60,144) 3, jpot_w(i,:)-1
  end do
  write(60,141) 'POINT_DATA', npot_w
  write(60,140) 'SCALARS potentials float'
  write(60,140) 'LOOKUP_TABLE default'
  do i = 1, npot_w
    write(60,144) mod(i,3) !###
  end do
  close(60)
  
  deallocate(xyzpot_w)


end subroutine wall2vtk

