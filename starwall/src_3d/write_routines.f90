!> Contains routines for writing data to the binary response file using MPI I/O.
module write_routines

use mpi_v
use resistive
use mpi

implicit none

contains



!====================================================================================
subroutine write_header

implicit none

  call MPI_File_write(file_handle, 42 , 1 , MPI_INTEGER,status,ier)
  disp=disp+sizeof(42)

  call MPI_File_write(file_handle, 42.d0 ,1, MPI_DOUBLE,status,ier)
  disp=disp+sizeof(42.d0)

  char512='#@content STARWALL VACUUM RESPONSE DATA FOR THE JOREK CODE'
  call MPI_File_write(file_handle, char512,sizeof(char512),MPI_CHARACTER,status,ier)
  disp=disp+sizeof(char512)
  write(*,*)'    write header done'

end subroutine write_header
!====================================================================================



!====================================================================================
subroutine write_int_parameter(parameter_name, int_value)

implicit none

character(len=*), intent(in) :: parameter_name
integer,intent(in) :: int_value

  character(len=12) :: marker='#@intparam'
  character(len=24) :: name
  name=parameter_name
  char36=marker // name

  call MPI_File_write(file_handle,char36,sizeof(char36),MPI_CHARACTER,status,ier)
  disp=disp+sizeof(char36)
    
  call MPI_File_write(file_handle,int_value,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(int_value)
  write(*,*) "    write ", char36, "done"
 
end subroutine write_int_parameter
!====================================================================================



!====================================================================================
subroutine write_1d_int_arr_sequential(array_name,dim,array_type,size1,size2,int1d)

implicit none

character(len=*), intent(in) :: array_name
integer,intent(in) :: dim
character(len=*), intent(in) :: array_type
integer,intent(in) :: size1
integer,intent(in) :: size2
integer, intent(in):: int1d(size1)

character(len=12) :: marker='#@array'
character(len=24) :: name
  
  name=array_name
  char36=marker // name
  char24=array_type

  call MPI_File_write(file_handle,char36,sizeof(char36),MPI_CHARACTER,status,ier)
  disp=disp+sizeof(char36)

  call MPI_File_write(file_handle,dim,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(dim)

  call MPI_File_write(file_handle,char24,sizeof(char24),MPI_CHARACTER,status,ier)
  disp=disp+sizeof(char24)

  call MPI_File_write(file_handle,size1,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(size1)

  call MPI_File_write(file_handle,size2,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(size2)

  call MPI_File_write(file_handle,int1d,size1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(int1d(1))*size1
  write(*,*) "    write ", char36, "done"

end subroutine write_1d_int_arr_sequential
!====================================================================================



!====================================================================================
subroutine write_2d_int_arr_sequential(array_name,dim,array_type,size1,size2,int2d)

implicit none

character(len=*), intent(in) :: array_name
integer,intent(in) :: dim
character(len=*), intent(in) :: array_type
integer,intent(in) :: size1
integer,intent(in) :: size2
integer, intent(in):: int2d(size1,size2)

character(len=12) :: marker='#@array'
character(len=24) :: name

  name=array_name
  char36=marker // name
  char24=array_type

  call MPI_File_write(file_handle,char36,sizeof(char36),MPI_CHARACTER,status,ier)
  disp=disp+sizeof(char36)

  call MPI_File_write(file_handle,dim,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(dim)

  call MPI_File_write(file_handle,char24,sizeof(char24),MPI_CHARACTER,status,ier)
  disp=disp+sizeof(char24)

  call MPI_File_write(file_handle,size1,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(size1)

  call MPI_File_write(file_handle,size2,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(size2)

  call MPI_File_write(file_handle,int2d,size1*size2,MPI_INTEGER,status,ier)
  disp=disp+sizeof(int2d(1,1))*size1*size2

  write(*,*) "    write ", char36, "done"

end subroutine write_2d_int_arr_sequential
!====================================================================================



!====================================================================================
subroutine write_1d_float_arr_sequential(array_name,dim,array_type,size1,size2,float1d)

implicit none

character(len=*), intent(in) :: array_name
integer,intent(in) :: dim
character(len=*), intent(in) :: array_type
integer,intent(in) :: size1
integer,intent(in) :: size2
real, intent(in):: float1d(size1)

character(len=12) :: marker='#@array'
character(len=24) :: name

  name=array_name
  char36=marker // name
  char24=array_type

  call MPI_File_write(file_handle,char36,sizeof(char36),MPI_CHARACTER,status,ier)
  disp=disp+sizeof(char36)

  call MPI_File_write(file_handle,dim,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(dim)

  call MPI_File_write(file_handle,char24,sizeof(char24),MPI_CHARACTER,status,ier)
  disp=disp+sizeof(char24)

  call MPI_File_write(file_handle,size1,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(size1)

  call MPI_File_write(file_handle,size2,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(size2)

  call MPI_File_write(file_handle,float1d,size1,MPI_DOUBLE_PRECISION,status,ier)
  disp=disp+sizeof(float1d(1))*size1

  write(*,*) "    write ", char36, "done"

end subroutine write_1d_float_arr_sequential
!====================================================================================



!====================================================================================
subroutine write_2d_float_arr_sequential(array_name,dim,array_type,size1,size2,float2d)

implicit none

character(len=*), intent(in) :: array_name
integer,intent(in) :: dim
character(len=*), intent(in) :: array_type
integer,intent(in) :: size1
integer,intent(in) :: size2
real, intent(in):: float2d(size1,size2)

character(len=12) :: marker='#@array'
character(len=24) :: name

  name=array_name
  char36=marker // name
  char24=array_type

  call MPI_File_write(file_handle,char36,sizeof(char36),MPI_CHARACTER,status,ier)
  disp=disp+sizeof(char36)

  call MPI_File_write(file_handle,dim,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(dim)

  call MPI_File_write(file_handle,char24,sizeof(char24),MPI_CHARACTER,status,ier)
  disp=disp+sizeof(char24)

  call MPI_File_write(file_handle,size1,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(size1)

  call MPI_File_write(file_handle,size2,1,MPI_INTEGER,status,ier)
  disp=disp+sizeof(size2)

  call MPI_File_write(file_handle,float2d,size1*size2,MPI_DOUBLE_PRECISION,status,ier)
  disp=disp+sizeof(float2d)

  write(*,*) "    write ", char36, "done"

end subroutine write_2d_float_arr_sequential
!====================================================================================


!====================================================================================
subroutine write_2d_float_arr_parallel(array_name,dim,array_type,size1,size2,loc_size1,loc_size2,float2d)

implicit none

character(len=*), intent(in) :: array_name
integer,intent(in) :: dim
character(len=*), intent(in) :: array_type
integer,intent(in) :: size1
integer,intent(in) :: size2
integer,intent(in) :: loc_size1
integer,intent(in) :: loc_size2

real, intent(in):: float2d(loc_size1,loc_size2)

character(len=12) :: marker='#@array'
character(len=24) :: name

  name=array_name
  char36=marker // name
  char24=array_type


  ! Here the standard starwall-response header is written by master rank in the following
  ! format: #@array     ', 's_ww                    ', 2, 'float                   
 
  if(rank==0) then

    call MPI_File_write(file_handle,char36,sizeof(char36),MPI_CHARACTER,status,ier)
    disp=disp+sizeof(char36)

    call MPI_File_write(file_handle,dim,1,MPI_INTEGER,status,ier)
    disp=disp+sizeof(dim)

    call MPI_File_write(file_handle,char24,sizeof(char24),MPI_CHARACTER,status,ier)
    disp=disp+sizeof(char24)

    call MPI_File_write(file_handle,size1,1,MPI_INTEGER,status,ier)
    disp=disp+sizeof(size1)

    call MPI_File_write(file_handle,size2,1,MPI_INTEGER,status,ier)
    disp=disp+sizeof(size2)

  endif  

  ! The displasment from the beggining of the output file is broadcasted to all MPI ranks.
  call MPI_BCAST(disp, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier )

  ! Auxilirary variables for block-cycling distribution output. 
  ! The darray data structure is defined the ScaLAPACK matrices distribution scheme. 

  ! dims     -> global matrix sizes; 
  ! distribs = [MPI_DISTRIBUTE_CYCLIC, MPI_DISTRIBUTE_CYCLIC]_-> block-cycling distribution &
  !            defined in the write_response_unformated.f90
  ! dargs    = [NB, NB] -> block-cycling distribution block size &
  !            defined in the write_response_unformated.f90
  ! pdims    = [NPROW, NPCOL] -> number of MPI tasks in row and column process grid divission 
 

  dims=[size1, size2]
  loc_arr_size=loc_size1*loc_size2
  call MPI_Type_create_darray(numtasks, rank, 2, dims, distribs, &
                              dargs,pdims,MPI_ORDER_FORTRAN,     &
                              MPI_DOUBLE_PRECISION, my_darray, ier)

  call MPI_Type_commit(my_darray,ier)
 
  ! Set independent file view for each MPI task in such a way that complex data
  ! as block-cycling distribution can be written by one operation 
  call MPI_File_set_view(file_handle, disp, MPI_DOUBLE_PRECISION,my_darray, &
                         "native",MPI_INFO_NULL, ier)

  ! Collective writing to the file. All MPI tasks simultaneusly write local
  ! submatrices in the correct place. 
  call MPI_File_write_all(file_handle, float2d, loc_arr_size, MPI_DOUBLE_PRECISION,status, ier)
  disp=disp+sizeof(float2d(1,1))*size1*size2

  ! Return to ordinary file view
  call MPI_File_set_view(file_handle, disp, MPI_BYTE, MPI_BYTE, "native",MPI_INFO_NULL, ier);

  if (rank==0)   write(*,*) "    write ", char36, "done"
  
end subroutine write_2d_float_arr_parallel
!====================================================================================

end module write_routines
