!----------------------------------------------------------------------
 subroutine matrix_cp 
! ----------------------------------------------------------------------
!   purpose:                                                  25/06/02
!
!
! ----------------------------------------------------------------------
      use coil_tria
      use tri_p
      use solv
   
      use tri
      use tri_b
      use mpi_v
      
! ----------------------------------------------------------------------
      implicit none

      real   ,dimension(:,:),allocatable :: dxp,dyp,dzp
      real   ,dimension(:  ),allocatable :: jx,jy,jz,jx_p,jy_p,jz_p
      real                               :: dx1,dy1,dz1,dx2,dy2,dz2
      real                               :: dx3,dy3,dz3,temp
      integer                            :: i,ic,ic0,ic1,j,k,jp

      real :: dima_sca=0, dima_sca_b=0
      integer :: j_loc,i_loc
      logical :: inside_j,inside_i

! ----------------------------------------------------------------------
     if(rank==0) write(6,*)  'compute matrix_cp  ', 'ntri_p=', ntri_p, 'ntri_c=', ntri_c

! ----------------------------------------------------------------------
      allocate(dxp(ntri_p,3),dyp(ntri_p,3),dzp(ntri_p,3)               &
              ,jx(ntri_c),    jy(ntri_c),  jz(ntri_c)                  &
              ,jx_p(ntri_p),jy_p(ntri_p),jz_p(ntri_p),stat=ier)
              if (ier /= 0) then
                    print *,'Matrix_cp.f90: Can not allocate local arrays nx, ..., zm'
                    stop
               endif
 

          jx=0.; jy=0.; jz=0.; jx_p=0.; jy_p=0.; jz_p=0. 
!-----------------------------------------------------------------------
! ----------------------------------------------------------------------

! Additional arrays that will be used for tri_induct subroutine
! For the production run with n_tri_w=5*10^5 the size of all array will be around 300Mb
! if they will not be distributed among MPI tasks

 allocate(nx(ntri_c),ny(ntri_c),nz(ntri_c),tx1(ntri_c),         &
          ty1(ntri_c),tz1(ntri_c),tx2(ntri_c),ty2(ntri_c),      &
          tz2(ntri_c),tx3(ntri_c),ty3(ntri_c),tz3(ntri_c),      &
          d221(ntri_c),d232(ntri_c),d213(ntri_c),area(ntri_c),  &
          xm(7*ntri_p), ym(7*ntri_p), zm(7*ntri_p),  stat=ier)
          if (ier /= 0) then
             print *,'Matrix_cp.f90: Can not allocate local arrays nx, ..., zm'
             stop
          endif

nx=0.; ny=0.; nz=0.; tx1=0.; ty1=0.;tz1=0.; tx2=0.
ty2=0.; tz2=0.; tx3=0.; ty3=0.; tz3=0.; d221=0.
d232=0.; d213=0.;area=0.; xm=0.; ym=0.; zm=0.


 allocate(nx_b(ntri_p),ny_b(ntri_p),nz_b(ntri_p),tx1_b(ntri_p),         &
          ty1_b(ntri_p),tz1_b(ntri_p),tx2_b(ntri_p),ty2_b(ntri_p),      &
          tz2_b(ntri_p),tx3_b(ntri_p),ty3_b(ntri_p),tz3_b(ntri_p),      &
          d221_b(ntri_p),d232_b(ntri_p),d213_b(ntri_p),area_b(ntri_p),  &
          xm_b(7*ntri_c), ym_b(7*ntri_c), zm_b(7*ntri_c),stat=ier)
          if (ier /= 0) then
             print *,'Matrix_cp.f90: Can not allocate local arrays nx_b, ..., zm_b'
             stop
         endif

nx_b=0.; ny_b=0.; nz_b=0.; tx1_b=0.; ty1_b=0.;tz1_b=0.; tx2_b=0.
ty2_b=0.; tz2_b=0.; tx3_b=0.; ty3_b=0.; tz3_b=0.; d221_b=0.
d232_b=0.; d213_b=0.;area_b=0.; xm_b=0.; ym_b=0.; zm_b=0.
! ----------------------------------------------------------------------


      do j = 1,ntri_p
         dxp(j,1) = xp(j,2) - xp(j,3)
         dyp(j,1) = yp(j,2) - yp(j,3)
         dzp(j,1) = zp(j,2) - zp(j,3)
         dxp(j,2) = xp(j,3) - xp(j,1)
         dyp(j,2) = yp(j,3) - yp(j,1)
         dzp(j,2) = zp(j,3) - zp(j,1)
         dxp(j,3) = xp(j,1) - xp(j,2)
         dyp(j,3) = yp(j,1) - yp(j,2)
         dzp(j,3) = zp(j,1) - zp(j,2)
     
         call tri_induct_1(j,xp,yp,zp,ntri_p)
         call tri_induct_2_b(ntri_p,j,xp,yp,zp)

      enddo
      
      do j=1,ntri_c
         dx1 = x_coil(j,2)-x_coil(j,3)
         dy1 = y_coil(j,2)-y_coil(j,3)
         dz1 = z_coil(j,2)-z_coil(j,3)
         dx2 = x_coil(j,3)-x_coil(j,1)
         dy2 = y_coil(j,3)-y_coil(j,1)
         dz2 = z_coil(j,3)-z_coil(j,1)
         dx3 = x_coil(j,1)-x_coil(j,2)
         dy3 = y_coil(j,1)-y_coil(j,2)
         dz3 = z_coil(j,1)-z_coil(j,2)

         jx(j) = dx1*phi_coil(j,1)+dx2*phi_coil(j,2)+dx3*phi_coil(j,3)
         jy(j) = dy1*phi_coil(j,1)+dy2*phi_coil(j,2)+dy3*phi_coil(j,3)
         jz(j) = dz1*phi_coil(j,1)+dz2*phi_coil(j,2)+dz3*phi_coil(j,3)
           
         call tri_induct_1_b(j,x_coil,y_coil,z_coil,ntri_c)
         call tri_induct_2(ntri_c,j,x_coil,y_coil,z_coil)

       enddo

      ic0 =0
      do j  =1,ncoil
         call  ScaLAPACK_mapping_i(j,i_loc,inside_i)
         if (inside_i) then! If not inside       

         do ic =1,jtri_c(j)
            ic1 = ic0 +ic
            do k=1,3
               do i=1,ntri_p
                  jp =jpot_p(i,k)+1
                  call ScaLAPACK_mapping_j(jp,j_loc,inside_j)
                  if (inside_j) then! If not inside
                                  
                         dima_sca=0
                         dima_sca_b=0
                        
                         call tri_induct_3_2(ntri_c,ic1,i,x_coil,y_coil,z_coil,dima_sca)  !dima
                         call tri_induct_3_2_b(ntri_p,i,ic1,xp,yp,zp,dima_sca_b) !dimb


                         temp =  (dxp(i,k)*jx(ic1)                         & 
                                + dyp(i,k)*jy(ic1)                         &
                                + dzp(i,k)*jz(ic1))*.5*(dima_sca+dima_sca_b)
                         if(jpot_p(i,k).ne.npot_p) &
                            a_wp_loc(i_loc,j_loc) = a_wp_loc(i_loc,j_loc) + temp
              
                   endif 
               enddo
            enddo
          enddo
          endif
          ic0 = ic0+jtri_c(j)
      enddo


!-------------------------------------------------------------------------
!
    do i = 1,ntri_p
      jx_p(i) = phi0_p(i,1)*dxp(i,1)+phi0_p(i,2)*dxp(i,2)+phi0_p(i,3)*dxp(i,3)
      jy_p(i) = phi0_p(i,1)*dyp(i,1)+phi0_p(i,2)*dyp(i,2)+phi0_p(i,3)*dyp(i,3)
      jz_p(i) = phi0_p(i,1)*dzp(i,1)+phi0_p(i,2)*dzp(i,2)+phi0_p(i,3)*dzp(i,3)
    enddo

      ic0 =0
      do j  =1,ncoil
         call  ScaLAPACK_mapping_i(j,i_loc,inside_i)
         if (inside_i) then! If not inside   
            call ScaLAPACK_mapping_j(1,j_loc,inside_j)
                  if (inside_j) then! If not inside
                  
                     do ic =1,jtri_c(j)
                        ic1 = ic0 +ic
                        do i=1,ntri_p
                 
                         dima_sca=0
                         dima_sca_b=0

                         call tri_induct_3_2(ntri_c,ic1,i,x_coil,y_coil,z_coil,dima_sca)  !dima
                         call tri_induct_3_2_b(ntri_p,i,ic1,xp,yp,zp,dima_sca_b) !dimb

                            temp =  (jx_p(i)*jx(ic1)                         &
                                   + jy_p(i)*jy(ic1)                         &
                                   + jz_p(i)*jz(ic1))*.5*(dima_sca+dima_sca_b)
   
                          a_wp_loc(i_loc,j_loc) = a_wp_loc(i_loc,j_loc) + temp
 

                          enddo
                       enddo
                  endif
          endif
          ic0 = ic0+jtri_c(j)
      enddo


      deallocate(nx,ny,nz,tx1,ty1,tz1,tx2,ty2, &
          tz2,tx3,ty3,tz3,d221,d232,d213,area,xm,ym,zm)

      deallocate(nx_b,ny_b,nz_b,tx1_b,ty1_b,tz1_b,tx2_b,ty2_b, &
          tz2_b,tx3_b,ty3_b,tz3_b,d221_b,d232_b,d213_b,area_b,xm_b,ym_b,zm_b)

      deallocate(jz_p,jy_p,jx_p,jz,jy,jx,dzp,dyp,dxp)
!-----------------------------------------------------------------------
if(rank==0) write(*,*) 'matrix_cp done'
if(rank==0) write(*,*) '==============================================================='

end subroutine  matrix_cp
