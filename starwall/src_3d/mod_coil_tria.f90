!> Module duplicating information of module coils for use in matrix construction routines.
!!
!! Routine collect_all_coil_info of the module coils fills the data structures below.
module coil_tria
  implicit none
  public
  integer              :: ncoil                      !< Total number of coils
  integer              :: ntri_c                     !< Total number of coil triangles
  integer, allocatable :: jtri_c(:)                  !< Number of triangles per coil
  real,    allocatable :: x_coil(:,:)                !< x-position of coil triangle nodes
  real,    allocatable :: y_coil(:,:)                !< y-position of coil triangle nodes
  real,    allocatable :: z_coil(:,:)                !< z-position of coil triangle nodes
  real,    allocatable :: phi_coil(:,:)              !< "Potential" to account for bands and turns
  real,    allocatable :: eta_thin_coil(:)           !< Thin wall resistivity of coil triangles
  real,    allocatable :: coil_resist(:)             !< Total resistivity of each coil
  integer              :: n_pol_coils                !< Number of pol coils
  integer              :: n_rmp_coils                !< Number of rmp coils
  integer              :: n_voltage_coils            !< Number of voltage coils
  integer              :: n_diag_coils               !< Number of diag coils
  integer              :: ind_start_pol_coils        !< Index of first pol_coil in list of coils
  integer              :: ind_start_rmp_coils        !< Index of first rmp_coil in list of coils
  integer              :: ind_start_voltage_coils    !< Index of first voltage_coil in list of coils
  integer              :: ind_start_diag_coils       !< Index of first diag_coil in list of coils
end module coil_tria
