subroutine a_pwe_s_computing
use solv 
use tri_p
use mpi_v
use sca
use mpi

!-----------------------------------------------------------------------
implicit none

integer :: i,j

integer   BLACS_PNUM
EXTERNAL  BLACS_PNUM

integer :: ipr_rec=0,ipc_rec=0,ip_proc_rec=0,ipr_send=0, ipc_send=0
integer :: ip_proc_send=0,i_loc=0,j_loc=0, i_loc2=0,j_loc2=0

real,dimension(:  ),allocatable :: a_pwe_arr , a_pwe_arr_tot

allocate(a_pwe_arr(npot_p), a_pwe_arr_tot(npot_p),stat=ier)
   IF (IER /= 0) THEN
             WRITE (*,*) "a_pwe_s computing : Can not allocate local matrix a_pwe_arr: MY_PROC_NUM=",&
                     MYPNUM, "MP_A,NQ_A=", MP_A,NQ_A
             STOP
   END IF

a_pwe_arr=0. ; a_pwe_arr_tot=0.

if(rank==0)  write(*,*) 'a_pwe_s_computing begins'

!Set up scalapack sub grid
   call SCA_GRID(npot_p,nd_bez)
   ! allocate local matrix
   allocate(a_pwe_loc_s(MP_A,NQ_A), stat=ier)
   IF (IER /= 0) THEN
             WRITE (*,*) "a_pwe_s computing : Can not allocate local matrix a_pwe_loc_s: MY_PROC_NUM=",&
                     MYPNUM, "MP_A,NQ_A=", MP_A,NQ_A
             STOP
   END IF
   LDA_pwe_s=LDA_A
   a_pwe_loc_s=0.

do j=1,nd_bez
         call ScaLAPACK_mapping_j_2(j+nd_w,j_loc,ipc_send)
        
    do i=1,npot_p

         call ScaLAPACK_mapping_i_2(i,i_loc,ipr_send)
         ip_proc_send   = BLACS_PNUM(CONTEXT, ipr_send, ipc_send)
         
         if( MYPNUM==ip_proc_send)  a_pwe_arr(i) = a_pwe_loc(i_loc,j_loc)
       
   enddo

    CALL MPI_ALLREDUCE(a_pwe_arr, a_pwe_arr_tot, npot_p, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)

    call ScaLAPACK_mapping_j_2(j,j_loc2,ipc_rec)
    do i=1,npot_p

         call ScaLAPACK_mapping_i_2(i,i_loc2,ipr_rec)
         ip_proc_rec   = BLACS_PNUM(CONTEXT, ipr_rec, ipc_rec)
   
         if( MYPNUM==ip_proc_rec)  a_pwe_loc_s(i_loc2,j_loc2)   = a_pwe_arr_tot(i)
   enddo
   a_pwe_arr=0.;  a_pwe_arr_tot=0.

enddo


 deallocate(a_pwe_arr , a_pwe_arr_tot)

if(rank==0) write(*,*) 'a_pwe_s_computing done'
if(rank==0) write(*,*) '==============================================================='


end subroutine a_pwe_s_computing

