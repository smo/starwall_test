subroutine surface_wall

use icontr
use tri_w
use mpi_v
use mpi

implicit none

real                :: rc_w(MAX_MN_W),rs_w(MAX_MN_W),zc_w(MAX_MN_W),zs_w(MAX_MN_W)
real,   allocatable :: x_w(:),y_w(:),z_w(:),r(:)  
integer             :: m_w(MAX_MN_W),n_w(MAX_MN_W)
integer,allocatable :: jpot(:)
integer             :: nwu,nwv,ntw
integer             :: nwuv,mn_w, ierr
integer             :: j,ku,kv,i,ip,k,i1,i2,i3,i4
real                :: pi2,fnu,alu,alv
real                :: cm,cn,cov,siv,cou,siu,cop,sip,co,si
character(len=80)   :: wall_filename

! --- Namelist with conducting wall input parameters.
namelist / params_wall / nwu ,nwv, mn_w, n_w, m_w, rc_w, rs_w, zc_w, zs_w, wall_filename, eta_thin_w

! --- Preset and read wall input parameters.
nwu = 32
nwv = 32
mn_w = 2
rc_w = 0.
rs_w = 0.
zc_w = 0.
zs_w = 0.
m_w  = 0
n_w  = 0
eta_thin_w = -999.
wall_filename = "__NO_FILENAME__"

if(rank==0)  read(*, params_wall)
call MPI_BCAST(nwu,         1, MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
call MPI_BCAST(nwv,         1, MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
call MPI_BCAST(mn_w,        1, MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
call MPI_BCAST(n_w,  MAX_MN_W, MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
call MPI_BCAST(m_w,  MAX_MN_W, MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
call MPI_BCAST(rc_w, MAX_MN_W, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
call MPI_BCAST(rs_w, MAX_MN_W, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
call MPI_BCAST(zc_w, MAX_MN_W, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
call MPI_BCAST(zs_w, MAX_MN_W, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
call MPI_BCAST(eta_thin_w,  1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)

if (eta_thin_w < 0.d0) then
  if (rank == 0) then 
    write(*,*) ' ' 
    write(*,*) ' ERROR: ETA_THIN_W NEEDS TO BE SET UP!'
    write(*,*) ' '   
  endif
  call MPI_FINALIZE(ier)
  stop
endif

! --- Log wall input parameters.
if (rank==0) write(*,*)
if (rank==0) write(*,*) 'Conducting wall input parameters:'

if (trim(wall_filename) .ne. "__NO_FILENAME__" ) then

  if (rank==0) write(*,'(A,A)') '  wall from file : ', wall_filename
  if (rank==0) open(42, file=trim(wall_filename), status='old', action='read', iostat=ierr)

  if ( ierr /= 0 ) then
    if (rank==0) write(*,*) 'ERROR: COULD NOT OPEN NAMELIST FILE "', trim(wall_filename), '".'
    stop
  else
    if (rank==0) read(42,*) nwu

    if (rank==0) write(*,'(A,I3,A)') '  nwu   = ', nwu, '(grid points in poloidal direction)'
    if (rank==0) write(*,'(A,I3,A)') '  nwv   = ', nwv, '(grid points in toroidal direction)'

    call MPI_BCAST(nwu,         1, MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
    call MPI_BCAST(nwv,         1, MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
    nwuv  = nwu*nwv ! total number of grid points
    allocate (x_w(nwuv),y_w(nwuv),z_w(nwuv),r(nwuv),stat=ier)

    do i=1, nwu 
      if (rank==0) read(42,*) r(i), z_w(i)
    enddo
    if (rank==0) close(42)

    do kv=2,nwv
      do ku=1,nwu
        i = ku+nwu*(kv-1)
        r(i)   = r(ku)
        z_w(i) = z_w(ku)
      enddo
    enddo

  endif

else
  
  if ( rank == 0 ) then
    write(*,'(A,ES15.5)') '  eta_thin_w = ', eta_thin_w
    write(*,'(A,I3)')     '  nwu   = ', nwu, '(grid points in poloidal direction)'
    write(*,'(A,I3)')     '  nwv   = ', nwv, '(grid points in toroidal direction)'
    write(*,'(A,I3)')     '  mn_w  = ', mn_w
    write(*,*) '  n_w  m_w     rc_w        rs_w        zc_w        zs_w'
    do i=1,mn_w
      write(*,'(1x,2i5,1p4e12.4,0p)') n_w(i),m_w(i),rc_w(i),rs_w(i),zc_w(i),zs_w(i)
    end do
  end if

  nwuv  = nwu*nwv ! total number of grid points
  allocate (x_w(nwuv),y_w(nwuv),z_w(nwuv),r(nwuv),stat=ier)

! ----------------------------------------------------------------------
  pi2  =4.*asin(1.)
  fnu  = 1./float(nwu) 
  alu  = pi2*fnu 
  alv  = pi2/float(nwv)
  z_w = 0.
  r   = 0.
  do  j =  1,mn_w
    cm = m_w(j)*pi2
    cn = n_w(j)*pi2
    do  kv=1,nwv
      cov = cos(alv*n_w(j)*(kv-1))
      siv = sin(alv*n_w(j)*(kv-1))
      do  ku=1,nwu
        i = ku+nwu*(kv-1)
        cou = cos(alu*m_w(j)*(ku-1))
        siu = sin(alu*m_w(j)*(ku-1))

        cop = cou*cov-siu*siv
        sip = siu*cov+cou*siv

        r(i)   = r(i)   + rs_w(j)*sip + rc_w(j)*cop
        z_w(i) = z_w(i) + zs_w(j)*sip + zc_w(j)*cop 
      end do
    end do
  end do

endif

! Remarks:
! large Phi: independent variables
! small phi: current potential at each triangle vertex
! -> mapping between both required
!
! poloidal direction: u; toroidal direction: v

ntri_w = 2*nwuv                          ! number of triangles
allocate(xw(ntri_w,3),yw(ntri_w,3),zw(ntri_w,3),jpot_w(ntri_w,3),eta_thin_wall(ntri_w))
allocate(phi0_w(ntri_w,3))              ! for (n=0,m=0)-mode; required as Phi is not single-valued

eta_thin_wall(:) = eta_thin_w !### We need to generalize later on for spatially depending wall resistivity

pi2  = 4.*asin(1.)
alv  = pi2/float(nwv)

do  kv = 1,nwv
  co   = cos(alv*(kv-1)) 
  si   = sin(alv*(kv-1)) 
  do  ku = 1,nwu
    i      = nwu*(kv-1)+ku
    x_w(i)   =   co * r(i)
    y_w(i)   =   si * r(i)
  end do
end do

! ----------------------------------------------------------------------
do  kv = 1,nwv

  ip   = nwu
  if (kv.eq.nwv) ip   = -nwuv +nwu

  do ku=1,nwu
    i3   = ku+nwu*(kv-1)
    i4   = mod(ku,nwu)+1+nwu*(kv-1)
    i1   = i3+ip
    i2   = i4+ip
    i    = 2*i3-1

    ! --- Index vector that maps all triangle variables phi to the independent variables Phi.
    jpot_w(i  ,1) = i1
    jpot_w(i  ,2) = i2
    jpot_w(i  ,3) = i3
    jpot_w(i+1,1) = i4
    jpot_w(i+1,2) = i3
    jpot_w(i+1,3) = i2

    phi0_w(i  ,1) = (float(ku)-1.)*fnu
    phi0_w(i  ,2) = (float(ku)   )*fnu
    phi0_w(i  ,3) = (float(ku)-1.)*fnu
    phi0_w(i+1,1) = (float(ku)   )*fnu
    phi0_w(i+1,2) = (float(ku)-1.)*fnu
    phi0_w(i+1,3) = (float(ku)   )*fnu

  end do
end do

do i=1,2*nwuv
  xw(i,1) = x_w(jpot_w(i,1))
  xw(i,2) = x_w(jpot_w(i,2))
  xw(i,3) = x_w(jpot_w(i,3))
  yw(i,1) = y_w(jpot_w(i,1))
  yw(i,2) = y_w(jpot_w(i,2))
  yw(i,3) = y_w(jpot_w(i,3))
  zw(i,1) = z_w(jpot_w(i,1))
  zw(i,2) = z_w(jpot_w(i,2))
  zw(i,3) = z_w(jpot_w(i,3))
end do

npot_w  = nwuv
npot_w1 = npot_w-1

if (rank==0) write(6,*)  
if (rank==0) write(6,*) ' number of wall triangles ntri_w = ',ntri_w
if (rank==0) write(6,*) 'current potential array npot_w = ',npot_w

if ( rank == 0 ) then
  open(60, iostat=ier, file='resistive_wall',&
    status='NEW',form='FORMATTED')
  write(60,3000)  npot_w/2, 4
  3000 format(2i7) 
  do kv=1,nwv 
    do ku=1,nwu 
      j = ku+nwu*(kv-1)
      i = 2*j-1
      write(60,3300) xw(i  ,1),yw(i  ,1),zw(i  ,1)
      write(60,3300) xw(i  ,2),yw(i  ,2),zw(i  ,2)
      write(60,3300) xw(i+1,1),yw(i+1,1),zw(i+1,1)
      write(60,3300) xw(i  ,3),yw(i  ,3),zw(i  ,3)
      3300 format(1p3e14.6)
    end do
  end do
  close(60)
end if

if (rank==0) write(32,*) 'Dreiecke'
if (rank==0) write(32,*) 1
if (rank==0) write(32,*) nwu*nwv*2
do i=1,2*nwu*nwv
  if (rank==0) write(32,*) i,1.0
  do k=1,3
    if (rank==0) write(32,3300) xw(i,k),yw(i,k),zw(i,k) 
  end do 
end do


if (rank==0) write(6,*) 'Surface wall done'
end subroutine surface_wall
