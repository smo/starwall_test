subroutine cholesky_solver
use solv 
use tri_p
use mpi_v
use sca
!-----------------------------------------------------------------------
implicit none


if(rank==0) write(*,*) 'cholesky_solver begins'
!================================================================================= 
! DPOTRF computes the Cholesky factorization of a real symmetric
! positive definite matrix a_pp.

CALL DESCINIT( DESCA, npot_p, npot_p, NB, NB, 0, 0, CONTEXT, LDA_pp, INFO_A )
if(INFO_A .NE. 0) then
  write(6,*) "Something is wrong in Cholesky_solver.f90 CALL DESCINIT DESCA, INFO_A=",INFO_A
  stop
endif

CALL PDPOTRF('U',npot_p,a_pp_loc,1,1,DESCA,INFO)
if(INFO .NE. 0) then
  write(6,*) "Something is wrong in Cholesky_solver.f90 CALL PDPOTRF, INFO=",INFO
  stop
endif
!================================================================================= 



!=================================================================================
!PDPOTRS solves a system of linear equations
CALL DESCINIT( DESCB, npot_p, nd_we, NB, NB, 0, 0, CONTEXT, LDA_pwe, INFO_B )
if(INFO_A .NE. 0) then
  write(6,*) "Something is wrong in Cholesky_solver.f90 CALL DESCINIT DESCB, INFO_B=",INFO_B
  stop
endif

CALL PDPOTRS('U', npot_p , nd_we , a_pp_loc , 1 , 1 , DESCA , a_pwe_loc , 1 , 1 , DESCB , INFO )
if(INFO .NE. 0) then
  write(6,*) "Something is wrong in Cholesky_solver.f90 CALL PDPOTRS, INFO=",INFO, "rank=",rank
  stop
endif
!=================================================================================
if(rank==0) write(*,*) 'cholesky_solver done'
if(rank==0) write(*,*) '==============================================================='

end subroutine cholesky_solver

