subroutine a_ye_computing
use solv
use mpi_v
use sca
use resistive
!-----------------------------------------------------------------------
implicit none

integer :: i_loc,i
logical :: inside_i

if(rank==0) write(*,*) "a_ye_computing start"

call    SCA_GRID(n_w,nd_bez)
!allocate local matrix
allocate(a_ye_loc(MP_A,NQ_A), stat=ier)
  IF (IER /= 0) THEN
             WRITE (*,*) "a_ye_computing, can not allocate local matrix a_ye_loc MYPROC_NUM=",MYPNUM
             STOP
  END IF
  a_ye_loc=0.
  LDA_ye= LDA_A


CALL DESCINIT(DESCA,n_w,n_w, NB, NB, 0, 0, CONTEXT, LDA_sww, INFO_A )
if(INFO_A .NE. 0) then
  write(6,*) "Something is wrong in a_ye_computing.f90 CALL DESCINIT DESCA, INFO_A=",INFO_A
  stop
endif


!a_we
CALL DESCINIT(DESCB,n_w, nd_bez, NB, NB, 0, 0, CONTEXT, LDA_we, INFO_B )
if(INFO_B .NE. 0) then
  write(6,*) "Something is wrong in a_ye_computing.f90 CALL DESCINIT DESCB, INFO_B=",INFO_B
  stop
endif


CALL DESCINIT(DESC_ye, n_w,nd_bez, NB, NB, 0, 0, CONTEXT, LDA_ye, INFO_C )
if(INFO_C .NE. 0) then
  write(6,*) "Something is wrong in a_ye_computing  CALL DESCINIT DESCC, INFO_C=",INFO_C
  stop
endif


CALL PDGEMM('T','N', n_w ,nd_bez, n_w , 1.0D0  , s_ww_loc ,1,1,DESCA, a_we_loc,1,1,DESCB,&
             1.0D0, a_ye_loc,1 , 1 , DESC_ye)


do i=1,n_w
        call  ScaLAPACK_mapping_i(i,i_loc,inside_i)
        if (inside_i) then
            a_ye_loc(i_loc,:) = a_ye_loc(i_loc,:)/gamma(i)
        endif
enddo


if(rank==0) write(*,*) "a_ye_computing completed"
if(rank==0) write(*,*) '==============================================================='

end subroutine a_ye_computing

