!------------------------------------------------------------------------
subroutine generate_index_fu(tx,ty,tz,ntri,j_pot,nupot,nhole)
!------------------------------------------------------------------------
      implicit none
      real,   dimension(ntri,3) :: tx,ty,tz 
      integer,dimension(ntri,3) :: j_pot,nbou 
      integer                   :: nhole,nupot,j
      real                      :: delta,dis,disp,tx1,ty1,tz1,tx2,ty2,tz2
      integer                   :: ntri,k,i,kp,ip,k1,kp1
!------------------------------------------------------------------------
!find the boundaries of a multiply connected wall composed of triangles
!  nbou(i,k) = 0  : edge (i,k)-(i,k+1)  interior edge 
!  nbou(i,k) = -1 : edge (i,k)-(i,k+1)  boundary edge 
         delta =  1.e-10
         nbou  = -1
      do k=1,3
       do i=1,ntri
        if(nbou(i,k).lt.0) then
         do kp=1,3
          do ip=i+1,ntri
             dis  = abs(tx(ip,kp )-tx(i,k )) &
                  + abs(ty(ip,kp )-ty(i,k )) &
                  + abs(tz(ip,kp )-tz(i,k ))    
           if(dis.le.delta) then
             kp1  = mod(kp+1,3) + 1  
             k1   = mod(k   ,3) + 1  
             disp = abs(tx(ip,kp1)-tx(i,k1)) &
                  + abs(ty(ip,kp1)-ty(i,k1)) &
                  + abs(tz(ip,kp1)-tz(i,k1))    
            if(disp.le.delta) then
             nbou(i ,k  ) = 0
             nbou(ip,kp1) = 0
            endif
           endif
          enddo
         enddo
        endif
       enddo
      enddo
  ! find the boundary of the holes
         nhole = 0
      do i=1,ntri
       do k=1,3
        if(nbou(i,k).eq.-1) then
         nhole     = nhole+1
         nbou(i,k) = nhole
         tx1 = tx(i,k)
         ty1 = ty(i,k)
         tz1 = tz(i,k)
          k1 = mod(k,3)+1
         tx2 = tx(i,k1)
         ty2 = ty(i,k1)
         tz2 = tz(i,k1)
  98  continue
        do ip=i+1,ntri
         do kp=1,3
          if(nbou(ip,kp).eq.-1) then
            kp1 = mod(kp,3) +1
            dis = abs(tx2-tx(ip,kp))   &
                 +abs(ty2-ty(ip,kp))   &
                 +abs(tz2-tz(ip,kp))    
           if(dis.le.delta) then
             nbou(ip,kp) = nhole
            tx2 = tx(ip,kp1)
            ty2 = ty(ip,kp1)
            tz2 = tz(ip,kp1)
            disp  = abs(tx1-tx(ip,kp1))   &
                  + abs(ty1-ty(ip,kp1))   &
                  + abs(tz1-tz(ip,kp1))    
            if(disp.le.delta)  goto 99
        go to 98
            endif
           endif
          enddo  
         enddo  
         write(6,*) 'boundary nr.:',nhole+1,' is not closed:  stop'
        stop
   99   continue
        write(6,*) ' boundary nr.:',nhole,'is closed'
        endif
       enddo  
      enddo  
      write(6,*) 'number of boundaries: ',nhole
! compute the index function 
       j_pot = 0 
   if(nhole.gt.0) then
       do k=1,3
        do i=1,ntri
         if(nbou(i,k).gt.0.and.j_pot(i,k).eq.0) then
!        if(nbou(i,k).gt.0) then
           j_pot(i,k) = nbou(i,k)
          do kp=1,3
           do ip=1,ntri
            dis = abs(tx(i,k)-tx(ip,kp))   &
                 +abs(ty(i,k)-ty(ip,kp))   &
                 +abs(tz(i,k)-tz(ip,kp))
            if(dis.le.delta) &
             j_pot(ip,kp) = j_pot(i,k)
           enddo
          enddo
         endif
        enddo
       enddo
      endif
         j = nhole
      do i=1,ntri
       do k=1,3
         if(j_pot(i,k).eq.0) then
          j          = j+1
          j_pot(i,k) = j
          do ip=i+1,ntri
         do kp=1,3
           dis = abs(tx(i,k)-tx(ip,kp))   &
                +abs(ty(i,k)-ty(ip,kp))   &
                +abs(tz(i,k)-tz(ip,kp))
           if(dis.le.delta)  &
            j_pot(ip,kp) = j_pot(i,k)
          enddo
         enddo
        endif
       enddo
      enddo
         nupot  = j
!     do i=1,ntri
!     do k=1,3
!      write(32,3200)i,k,j_pot(i,k),tx(i,k),ty(i,k),tz(i,k)
!3200 format(3i6,1p3e12.4)
!     enddo
!     enddo
end subroutine generate_index_fu  
