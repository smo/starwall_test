!----------------------------------------------------------------------
 subroutine matrix_rc 
! ----------------------------------------------------------------------
!   purpose:                                                  13/12/05
!
!
! ----------------------------------------------------------------------
      use coil_tria
      use solv
      use mpi_v
      use icontr
! ----------------------------------------------------------------------
      implicit none
      integer                            :: i,i0,i1,j
      real                               :: dx1,dy1,dz1,dx2,dy2,dz2
      real                               :: dx3,dy3,dz3
      real   ,dimension(  :),allocatable :: jx,jy,jz,area1

      integer :: j_loc,i_loc
      logical :: inside_j,inside_i

! ----------------------------------------------------------------------

if (rank==0)   write(6,*) 'compute matrix_rc'
! ----------------------------------------------------------------------
      allocate(jx(ntri_c),jy(ntri_c),jz(ntri_c),area1(ntri_c))
!-----------------------------------------------------------------------

      do j=1,ntri_c
         dx1 = x_coil(j,2)-x_coil(j,3)
         dy1 = y_coil(j,2)-y_coil(j,3)
         dz1 = z_coil(j,2)-z_coil(j,3)
         dx2 = x_coil(j,3)-x_coil(j,1)
         dy2 = y_coil(j,3)-y_coil(j,1)
         dz2 = z_coil(j,3)-z_coil(j,1)
         dx3 = x_coil(j,1)-x_coil(j,2)
         dy3 = y_coil(j,1)-y_coil(j,2)
         dz3 = z_coil(j,1)-z_coil(j,2)
       jx(j) = dx1*phi_coil(j,1)+dx2*phi_coil(j,2)+dx3*phi_coil(j,3)
       jy(j) = dy1*phi_coil(j,1)+dy2*phi_coil(j,2)+dy3*phi_coil(j,3)
       jz(j) = dz1*phi_coil(j,1)+dz2*phi_coil(j,2)+dz3*phi_coil(j,3)

         area1(j) = .5/sqrt( (dy1*dz3-dz1*dy3)**2                      &
                            +(dz1*dx3-dx1*dz3)**2                      &
                            +(dx1*dy3-dy1*dx3)**2 )
       enddo


       i0 = 0
       do j = 1,ncoil
          call  ScaLAPACK_mapping_i(j,i_loc,inside_i)
          if (inside_i) then! If not inside
              call ScaLAPACK_mapping_j(j,j_loc,inside_j)
              if (inside_j) then! If not inside

                   do i = 1,jtri_c(j)
                       i1 = i0 +i
                       a_rw_loc(i_loc,j_loc) = a_rw_loc(i_loc,j_loc)  &
                                          +area1(i1)*( jx(i1)*jx(i1)  &
                                          +jy(i1)*jy(i1)              &
                                          +jz(i1)*jz(i1) )            &
                                          * eta_thin_coil(i1) / eta_thin_w
                   enddo
               endif
           endif
          i0 = i0+jtri_c(j)
       enddo


      deallocate(area1,jz,jy,jx)

if(rank==0) write(*,*) 'matrix_rc  done'
if(rank==0) write(*,*) '==============================================================='

end subroutine  matrix_rc
