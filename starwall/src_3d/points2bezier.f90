subroutine points_to_bezier(y,b,npoints)


real*8 :: A(4,4)

! Integrals from 0 to 1 of the Bezier test functions

!H11 H11         H11 H12           H11 H21           H11 H22
A(1,1) = 13/35; A(1,2) = 11/70;   A(1,3) = 9/70;    A(1,4) = -13/140

!H12 H11         H12 H12           H12 H21           H12 H22 
A(2,1) = 11/70; A(2,2) = 3/35;    A(2,3) = 13/140;  A(2,4) = -9/140

!H21 H11         H21 H12           H21 H21           H21 H22
A(3,1) = 9/70;  A(3,2) = 13/140;  A(3,3) = 13/35;   A(3,4) = -11/70

!H22 H11          H22 H12          H22 H21           H22 H22
A(4,1) = -13/140; A(4,2) = -9/140; A(4,3) = -11/70;  A(4,4) = 3/35

return
end

