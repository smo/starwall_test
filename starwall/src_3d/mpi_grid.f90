!----------------------------------------------------------------------
subroutine mpi_grid()
! ----------------------------------------------------------------------
!   purpose:                                                  12/01/2016
!   Subroutine creats the processes grid 

! ----------------------------------------------------------------------
     use mpi_v
     use sca
     
! ----------------------------------------------------------------------
     implicit none
   
     integer :: i
  
     !Set up process grid that is as close to square as possible.
     sqrtnp=int(sqrt(real(numtasks))+1)
     do i=1,sqrtnp
        if(mod(numtasks,i).eq.0) NPCOL=i
     enddo

     NPROW = numtasks / NPCOL

!Check that the number of process passed as the argument matches the number of
!processes in the processor grid

      CALL BLACS_PINFO(MYPNUM, NPROCS)
      CALL BLACS_GET(-1, 0, CONTEXT)

      IF (NPROCS /= NPROW * NPCOL) THEN
         WRITE(*,*) 'Error! Number of processors passed does not match with &
                     & processors in the grid. NPROW=',NPROW,"NPCOL=",NPCOL,"NPROCS=",NPROCS
         STOP
      END IF


     CALL BLACS_GRIDINIT (CONTEXT, 'R', NPROW, NPCOL )
     CALL BLACS_GRIDINFO (CONTEXT, NPROW, NPCOL, MYROW, MYCOL)


end subroutine mpi_grid

