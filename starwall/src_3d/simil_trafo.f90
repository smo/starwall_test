!-----------------------------------------------------------------------
      subroutine simil_trafo(a_ww_loc,a_rw_loc,nw,gamma,S_ww_loc)
!-----------------------------------------------------------------------
      use sca
      use mpi_v
      implicit none
               

      real,   dimension(MP_A,NQ_A)  :: a_ww_loc,a_rw_loc,S_ww_loc
      real,   dimension(   nw)  :: gamma        
      integer  :: nw
     
      integer :: IBTYPE

      integer :: INFO_SCA, LIWORK
      integer (KIND=8) LWORK
      integer :: M_out,NZ_out,IL,IU
      integer,dimension(:),allocatable   :: IFAIL
      integer,dimension(:),allocatable   :: ICLUSTR
      real,   dimension(:),allocatable   :: GAP
      real,   dimension(:),allocatable   :: WORK
      integer,dimension(:),allocatable   :: IWORK
      real :: ABSTOL, VL, VU
      
      REAL PDLAMCH
      EXTERNAL PDLAMCH

      integer i_loc
 !==================================================================     
     if(rank==0) write(*,*) '==============================================================='
     if(rank==0) write(*,*) 'simil_trafo starts'
    
     S_ww_loc=a_ww_loc

     CALL DESCINIT( DESCA, nw, nw, NB, NB, 0, 0, CONTEXT, LDA_sww, INFO_A )
     if(INFO_A .NE. 0) then
        write(6,*) "Something is wrong in simil_trafo.f90 CALL DESCINIT DESCA, INFO_A=",INFO_A
        stop
     endif

     CALL DESCINIT( DESCB, nw, nw, NB, NB, 0, 0, CONTEXT, LDA_rw,  INFO_B )
     if(INFO_B .NE. 0) then
        write(6,*) "Something is wrong in simil_trafo.f90 CALL DESCINIT DESCB, INFO_B=",INFO_B
        stop
     endif

     CALL DESCINIT( DESCZ, nw, nw, NB, NB, 0, 0, CONTEXT, LDA_sww,INFO_Z )
     if(INFO_Z .NE. 0) then
        write(6,*) "Something is wrong in simil_trafo.f90 CALL DESCINIT DESCB, INFO_Z=",INFO_Z
        stop
     endif

    allocate(WORK(1),IWORK(1),GAP(NPROW*NPCOL),ICLUSTR(2*NPROW*NPCOL),IFAIL(nw), stat=ier)
    IF (IER /= 0) THEN
             WRITE (*,*) "Simil Trafo: Can not allocate vector WORK, ...,IFAIL: MY_PROC_NUM=",MYPNUM
             STOP
    END IF

     
! Initial parameters for PDSYGVX functions
    LWORK=-1
    LIWORK=-1
    IBTYPE=1
    ABSTOL=-1 !Default value
    M_out=0
    NZ_out=0
    !ORFAC=-1 !Default value 1e-3
    WORK=0
    IWORK=0
    VU=0.0D0
    VL=0.0D0
    IL=0
    IU=0

    !this setting yields the most orthogonal eigenvectors.
    ABSTOL =PDLAMCH(CONTEXT, 'U')

    !Here we calculate just the workspace for th PDSYGVX (if variable LWORK and ILWORK=-1)
    CALL PDSYGVX(  IBTYPE, 'V', 'A', 'U', nw, a_ww_loc, 1, 1, DESCA, a_rw_loc, 1, 1,&
                   DESCB, VL, VU, IL, IU, ABSTOL, M_out, NZ_out, gamma,&
                   ORFAC, s_ww_loc, 1, 1, DESCZ, WORK, LWORK, IWORK, LIWORK,&
                   IFAIL, ICLUSTR, GAP, INFO_SCA )
    IF (INFO_SCA /= 0) THEN
             WRITE (*,*) "Something is wrong  in PDSYGVX with i=", INFO_SCA, "parameters. MYPNUM:",MYPNUM
             STOP
    END IF

    !Sometimes PDSYGVX could not ortogonalize all eigenvector 
    !due to the low workspace.It is indicated in INFO_SCA=2 and in ICLUSTR.
    !In this case make lwork large or ORFAC smaller and play with tolerance parameter in the variable ABSTOL. 


    ! lwork =2*INT(ABS(work(1)))
    lwork =lwork_cooficient*INT(ABS(work(1)))+1
    if(rank==0) write(6,*) "    Need to allocate = ", REAL(lwork*8.0/1024.0/1024.0/1024.0), "GB workspace array"

     DEALLOCATE(work)
     ALLOCATE(work(LWORK),stat=ier)
     IF (IER /= 0) THEN
             WRITE (*,*) "Simil Trafo: Can not allocate WORK: MY_PROC_NUM=",MYPNUM, "LWORK=",LWORK
             STOP
     END IF

     liwork =lwork_cooficient*INT (ABS(iwork(1)))
     DEALLOCATE(iwork)
     ALLOCATE(iwork(liwork),stat=ier)
     IF (IER /= 0) THEN
             WRITE (*,*) "Simil Trafo: Can not allocate IWORK: MY_PROC_NUM=",MYPNUM
             STOP
     END IF

      if(rank==0) write(6,*) "    Need to allocate 2 = ", REAL(liwork*8.0/1024.0/1024.0/1024.0), "GB workspace array"

!!!!======== Make the main calculations of eingenvalues and eigenvecros


    if(rank==0) write(6,*) "    Start PDSYGVX - Eigenvalue solver"
    CALL PDSYGVX(IBTYPE, 'V', 'A', 'U', nw, a_ww_loc, 1, 1, DESCA, a_rw_loc, 1, 1,&
                   DESCB, VL, VU, IL, IU, ABSTOL, M_out, NZ_out, gamma,&
                   ORFAC, s_ww_loc, 1, 1, DESCZ, WORK, LWORK, IWORK, LIWORK,&
                   IFAIL, ICLUSTR, GAP, INFO_SCA)
    if(rank==0) write(6,*) "    END PDSYGVX - Eigenvalue solver"
    
!If INFO_Sca is not 0 -reffer to the web page: http://www-01.ibm.com/support/knowledgecenter/SSNR5K_4.2.0/com.ibm.cluster.pessl.v4r2.pssl100.doc/am6gr_lsygvx.htm

    IF (INFO_SCA /= 0) THEN
           IF (INFO_SCA == 2) THEN
             if(rank==0) WRITE (*,*) "The eigenvectors corresponding to one or more clusters", & 
                         "of eigenvalues could not be reorthogonalized because of", & 
                         "insufficient workspace. The indices of the clusters are", & 
                         "stored in the array ICLUSTR file fort.3200.",&
                         " Possible solution- Decrease value of ORFAC or increase lwork and ilwork"
           ELSE 
             WRITE (*,*) "Something is wrong in PDSYGVX 2 with INFO_SCA=", INFO_SCA, "MYPNUM:",MYPNUM
             STOP
           ENDIF
    END IF

! Additional check if everething is fine.        
        do i_loc=1,nw
             if(IFAIL(i_loc)/=0 ) then
                   if(rank==0) write(6,*) "Some eigenvectors could not be orthogonalized.Check file fort.3100+PROCNUM"
                   if(rank==0) write(3100+MYPNUM,*)  i_loc, IFAIL(i_loc)
             endif
        enddo

        do i_loc=1,2*NPROW*NPCOL
             if(ICLUSTR(i_loc)/=0 ) then
                   if(rank==0) write(3200+MYPNUM,*) i_loc,ICLUSTR(i_loc)
             endif
        enddo

        do i_loc=1,NPROW*NPCOL
             if(GAP(i_loc)/= -1.0 ) then
                  if(rank==0)  write(3300+MYPNUM,*) i_loc,GAP(i_loc)
             endif
        enddo

        if(M_out .NE. NZ_out) then
                    if(rank==0) write(6,*) "Number of computed eigenvector does not correcpond &
                                            & to number of eigenvalues.Check file fort.3400+PROCNUM"
                    if(rank==0) write(3400+MYPNUM,*) i_loc,GAP(i_loc)
        endif

        DEALLOCATE(IWORK,WORK,ICLUSTR,GAP,IFAIL)

        if(rank==0) write(*,*) 'simil_trafo done'
        if(rank==0) write(*,*) '==============================================================='
end subroutine simil_trafo



 
