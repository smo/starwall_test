! ----------------------------------------------------------------------
subroutine matrix_pot_ew
! ----------------------------------------------------------------------
!     purpose:                                              17/08/09
!
! ----------------------------------------------------------------------
      use icontr
      use contr_su
      use tri_p
      use tri_w
      use solv
      use coil_tria
! ----------------------------------------------------------------------
      implicit none
! ----------------------------------------------------------------------
      real,dimension(:,:),allocatable :: phi,b_par0
      real,dimension(  :),allocatable :: bx,by,bz
      real,dimension(:,:),allocatable :: a_c,a_s,b_c,b_s
      real                            :: pi2,alv,fnv
      integer                         :: i,i0,i1,ier,j,k,kp,ku,kv,nuv,nd_wc
! ----------------------------------------------------------------------
     write(6,*) 'compute matrix_ew'
        pi2 = 4.*asin(1.)
        nuv = nu*nv
        fnv = 1./float(nv)
        alv = pi2*fnv
        nd_wc = npot_w+ncoil
    allocate(phi(nuv,npot_w),stat=ier) ; phi = 0. 
    allocate(a_c(nu,npot_w),  a_s(nu,npot_w)) ; a_c=0.; a_s=0.
    allocate(b_c(2*N_bnd,npot_w),b_s(2*N_bnd,npot_w)); b_c =0.; b_s=0.
 
  call potential(phi,x,y,z,nuv,xw,yw,zw,ntri_w,jpot_w,npot_w)

      if(n_tor(1).eq.0) then

    allocate(b_par0(nuv,npot_w),stat=ier) ; b_par0 = 0. 
    allocate(a_ew(2*N_bnd,nd_wc),stat=ier) ; a_ew = 0.
    allocate(bx(nuv),by(nuv),bz(nuv),stat=ier); bx=0.; by=0.; bz=0.

      call bfield_c(x,y,z,bx,by,bz,nuv,xw,yw,zw,phi0_w,ntri_w)
      do i=1,nuv
       b_par0(i,1) = -(xu(i)*bx(i)+yu(i)*by(i)+zu(i)*bz(i)) 
      enddo
      do k=1,npot_w1
       do i=1,nuv
!        b_par0(i,k+1) = -b_par(i,k)
       enddo
      enddo
     do  ku=1,nu
      do  kv =1,nv
                j = ku+nu*(kv-1)
       do k=1,npot_w
        a_c(ku,k) = a_c(ku,k) + b_par0(j,k)*fnv
       enddo
      enddo
     enddo
!--- -----------------------------------------------------------------------
     call real_space2bezier(b_c,a_c,N_bnd,n_points,npot_w)
     do i =1,N_bnd
         j = 1 + nd_harm0*(i-1)
      do k=1,npot_w
        a_ew(j  ,k)  = b_c(2*i-1,k) 
        a_ew(j+1,k)  = b_c(2*i  ,k) 
      enddo
     enddo
   endif
!--------------------------------------------------------------------------
   if(n_tor(1).ne.0.or.n_harm.gt.1) then
     allocate(a_ew(N_bnd*nd_harm,npot_w)) ; a_ew = 0.
!--------------------------------------------------------------------------
       i0 = 1
     if(n_tor(1).eq.0) &
       i0 = 2

    do i=i0,n_harm           !   toroidal harmonics

        a_c = 0.
        a_s = 0.
        b_c = 0.
        b_s = 0. 

     do  ku=1,nu
      do  kv =1,nv
           j = ku+nu*(kv-1)
       do k=1,npot_w
        a_c(ku,k) = a_c(ku,k) + phi(j,k)*cos(alv*n_tor(i)*(kv-1))*fnv*2
        a_s(ku,k) = a_s(ku,k) + phi(j,k)*sin(alv*n_tor(i)*(kv-1))*fnv*2
       enddo
      enddo
     enddo

      call real_space2bezier(b_c,a_c,N_bnd,n_points,npot_w)
      call real_space2bezier(b_s,a_s,N_bnd,n_points,npot_w)

     do k =1,N_bnd
         j = 1+4*(i-i0) + nd_harm*(k-1)
      do kp=1,npot_w
        a_ew(j  ,kp) = b_c(2*k-1,kp) 
        a_ew(j+1,kp) = b_c(2*k,  kp) 
        a_ew(j+2,kp) = b_s(2*k-1,kp) 
        a_ew(j+3,kp) = b_s(2*k,  kp) 
      enddo
     enddo

    enddo
  endif
     deallocate(b_s,b_c,a_s,a_c)
     write(6,*) ' matrix_ew done'
   end subroutine matrix_pot_ew 
