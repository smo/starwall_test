subroutine a_ew_computing
use solv
use tri_p
use mpi_v
use sca

!-----------------------------------------------------------------------
implicit none


if(rank==0) write(*,*) "a_ew_computing start"


!a_ep
CALL DESCINIT(DESCA,nd_bez, npot_p, NB, NB, 0, 0, CONTEXT, LDA_ep, INFO_A )
if(INFO_A .NE. 0) then
  write(6,*) "Something is wrong in a_ew_computing.f90 CALL DESCINIT DESCA, INFO_A=",INFO_A
  stop
endif

!a_pwe
CALL DESCINIT(DESCB,npot_p, nd_w, NB, NB, 0, 0, CONTEXT, LDA_pwe, INFO_B )
if(INFO_B .NE. 0) then
  write(6,*) "Something is wrong in a_ew_computing.f90 CALL DESCINIT DESCB, INFO_B=",INFO_B
  stop
endif

!a_ew
!Try with real size a_ew(nd_e,npot_w+ncoils) and wih a_ew(nd_e,npot_w)
CALL DESCINIT(DESCC,nd_e, nd_w, NB, NB, 0, 0, CONTEXT, LDA_ew, INFO_C )
if(INFO_B .NE. 0) then
  write(6,*) "Something is wrong in a_ew_computing  CALL DESCINIT DESCC, INFO_C=",INFO_C
  stop
endif


CALL PDGEMM('N','N', nd_e ,nd_w, npot_p , -1.0D0  , a_ep_loc_sca ,1,1,DESCA, a_pwe_loc,1,1,DESCB,&
             1.0D0, a_ew_loc_sca,1 , 1 , DESCC) 


if(rank==0) write(*,*) "a_ew_computing completed"
if(rank==0) write(*,*) '==============================================================='

end subroutine a_ew_computing

