subroutine resistive_wall_response

use icontr
use contr_su
use solv 
use tri_w
use coil_tria

use sca
use mpi_v
use time
use resistive
use mpi
!-----------------------------------------------------------------------
implicit none

integer :: i

!-----------------------------------------------------------------------
if(rank==0) write(*,*) 'resistive_wall_response starts'

nd_bez = (nd_harm + nd_harm0)/2*n_dof_bnd
n_w    = npot_w+ncoil

if(rank==0) write(*,*) '     resistive_wall_response nd_harm  :',nd_harm
if(rank==0) write(*,*) '     resistive_wall_response nd_harm0 :',nd_harm0
if(rank==0) write(*,*) '     resistive_wall_response n_bnd    :',n_bnd
if(rank==0) write(*,*) '     resistive_wall_response nd_bez   :',nd_bez


    allocate(gamma(n_w),stat=ier)
    gamma =0.

    call SCA_GRID(n_w,n_w)
    !allocate local matrix
    allocate(S_ww_loc(MP_A,NQ_A), stat=ier)
    IF (IER /= 0) THEN
             WRITE (*,*) "resistive_wall_response, can not allocate local matrix s_ww_loc MYPROC_NUM=",MYPNUM
             STOP
    END IF
    S_ww_loc=0.
    LDA_sww= LDA_A
!======================================================
call simil_trafo(a_ww_loc,a_rw_loc,n_w,gamma,S_ww_loc)
!======================================================
deallocate(a_ww_loc,a_rw_loc)

    atime(21)=MPI_WTIME()
    if(rank==0) write(*,*) 'simil_trafo=',(atime(21)-atime(20)),'seconds'


!=================================================
call a_ye_computing
!=================================================

    atime(22)=MPI_WTIME()
    if(rank==0) write(*,*) 'a_ye_computing=',(atime(22)-atime(21)),'seconds'


!=================================================
call a_ey_computing
!=================================================


    atime(23)=MPI_WTIME()
    if(rank==0) write(*,*) 'a_ey_computing=',(atime(23)-atime(22)),'seconds'


!=================================================
call d_ee_computing
!=================================================

n_tor_jorek = 0
do i = 1, n_harm
  if ( n_tor(i) == 0 ) then
    n_tor_jorek = n_tor_jorek + 1
    i_tor_jorek(n_tor_jorek) = 1
  else
    n_tor_jorek = n_tor_jorek + 2
    i_tor_jorek(n_tor_jorek-1:n_tor_jorek) = n_tor(i)*2 + (/ 0, 1 /)
  end if
end do


    atime(24)=MPI_WTIME()
    if(rank==0) write(*,*) 'd_ee_computing=',(atime(24)-atime(23)),'seconds'

!=================================================
call computing_s_ww_inverse
!=================================================


    atime(25)=MPI_WTIME()
    if(rank==0) write(*,*) 'computing_s_ww_inverse=',(atime(25)-atime(24)),'seconds'


call MPI_BARRIER(MPI_COMM_WORLD,ier)
atime(26)=MPI_WTIME()


    if(rank==0) write(*,*) 'All Input=',(atime(3)-atime(2)), '  seconds'
    if(rank==0) write(*,*) 'All surface_wall=',(atime(4)-atime(3)), '  seconds'
    if(rank==0) write(*,*) 'matrix_pp=',(atime(5)-atime(4)), '  seconds'
    if(rank==0) write(*,*) 'matrix_wp=',(atime(6)-atime(5)), '  seconds'
    if(rank==0) write(*,*) 'matrix_ww=',(atime(7)-atime(6)), '  seconds'
    if(rank==0) write(*,*) 'matrix_rw=',(atime(8)-atime(7)), '  seconds'
    if(rank==0) write(*,*) 'matrix_cc=',(atime(9)-atime(8)), '  seconds'
    if(rank==0) write(*,*) 'matrix_cc=',(atime(10)-atime(9)), '  seconds'
    if(rank==0) write(*,*) 'matrix_cc=',(atime(11)-atime(10)), '  seconds'
    if(rank==0) write(*,*) 'matrix_cc=',(atime(12)-atime(11)), '  seconds'
    if(rank==0) write(*,*) 'matrix_ec=',(atime(13)-atime(12)), '  seconds'
    if(rank==0) write(*,*) 'a_pe_transpose_sca=',(atime(14)-atime(13)), 'seconds'
    if(rank==0) write(*,*) 'cholesky_solver=',(atime(15)-atime(14)), 'seconds'
    if(rank==0) write(*,*) 'a_pwe_s_computing=',(atime(16)-atime(15)), 'seconds'
    if(rank==0) write(*,*) 'a_ee_computing=',(atime(17)-atime(16)), 'seconds'
    if(rank==0) write(*,*) 'a_ew_computing=',(atime(18)-atime(17)), 'seconds'
    if(rank==0) write(*,*) 'a_we_computing=',(atime(19)-atime(18)), 'seconds'
    if(rank==0) write(*,*) 'matrix_multiplication=',(atime(20)-atime(19)),'seconds'
    if(rank==0) write(*,*) 'simil_trafo=',(atime(21)-atime(20)),'seconds'
    if(rank==0) write(*,*) 'a_ye_computing=',(atime(22)-atime(21)),'seconds'
    if(rank==0) write(*,*) 'a_ey_computing=',(atime(23)-atime(22)),'seconds'
    if(rank==0) write(*,*) 'd_ee_computing=',(atime(24)-atime(23)),'seconds'
    if(rank==0) write(*,*)  'computing_s_ww_inverse=',(atime(25)-atime(24)),'seconds'


if(rank==0) write(*,*) 'Total wall clock time before output =', (atime(26)-atime(1))/60.0, '  minutes'
if(rank==0) write(*,*) '======================================================================='


!=================================================
call write_response
!=================================================


if(rank==0) write(*,*) 'Program FINISH'
if(rank==0) write(*,*) '======================================================================='

call MPI_BARRIER(MPI_COMM_WORLD,ier)
atime(27)=MPI_WTIME()

if(rank==0) write(*,*) 'Total wall clock time including output=', (atime(27)-atime(1))/60.0, '  minutes'
if(rank==0) write(*,*) '======================================================================='

end subroutine resistive_wall_response
