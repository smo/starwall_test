module resistive

   implicit none
   real,dimension(:,:),allocatable ::  S_ww_loc, a_ey_loc, a_ye_loc, d_ee_loc, s_ww_inv_loc
   real,dimension(  :),allocatable ::  gamma

   integer :: n_w
   integer                         :: n_tor_jorek, i_tor_jorek(999)
   character(len=64)               :: format_type
   character(len=512)              :: char512
   character(len=36)               :: char36
   character(len=24)               :: char24

end module resistive
