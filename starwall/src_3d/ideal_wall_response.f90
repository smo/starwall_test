      subroutine ideal_wall_response
       use icontr
       use contr_su
       use solv 
       use tri_w
       use coil_tria
       use sca
!-----------------------------------------------------------------------
      implicit none
      integer                            :: i,ier,j,k,nd
!-----------------------------------------------------------------------
       nd     = npot_w+ncoil
       nd_bez = (nd_harm+nd_harm0)/2*n_dof_bnd

     allocate(b_ee(nd_bez,nd_bez))

!       solve a_ww * Phi_w = a_we

    call  dpotrf('U',nd,a_ww,nd,info)
    call dpotrs('U',nd,nd_bez,a_ww,nd,a_we,nd,info)

         b_ee= a_ee

     do i=1,nd_bez
     do k=1,nd_bez
      do j=1,npot_w
         b_ee(i,k) = b_ee(i,k) - a_ew(i,j)*a_we(j+ncoil,k)
      enddo
     enddo
    enddo


     do i=1,nd_bez
     do k=1,nd_bez
      do j=1,ncoil
         b_ee(i,k) = b_ee(i,k) - a_ec(i,j)*a_we(j,k)
      enddo
     enddo
    enddo

   
  open(60,file='starwall_m_nw',form="formatted",iostat=ier)
            write(60,'(i6)')  nd_bez
      do i=1,nd_bez
       do k=1,nd_bez
        write(60,'(2i6,1pe14.6)') i,k,a_ee(i,k)
       enddo
      enddo
  close(60)
  open(60,file='starwall_m_id',form="formatted",iostat=ier)
            write(60,'(i6)') nd_bez
      do i=1,nd_bez
       do k=1,nd_bez
        write(60,'(2i6,1pe14.6)') i,k,b_ee(i,k)
       enddo
      enddo
  close(60)
 end subroutine ideal_wall_response
