!----------------------------------------------------------------------
 subroutine matrix_wp 
! ----------------------------------------------------------------------
!   purpose:                                                  16/11/05
!
!
! ----------------------------------------------------------------------
use solv
use tri_p
use tri_w
use coil_tria

use tri ! new module with arrays and variables for tri_induct subroutine
use tri_b !new module with arrays and variables for tri_induct subroutine for dimb maxtrix
use mpi_v ! new module with mpi variables
use sca ! new module for ScaLAPACK variables
use mpi
 
! ----------------------------------------------------------------------
implicit none

real,   dimension(:,:),allocatable :: dxp,dyp,dzp,dxw,dyw,dzw
real,   dimension(  :),allocatable :: jwx,jwy,jwz,jpx,jpy,jpz
real,   dimension(  :),allocatable :: jx,jy,jz,jx_p,jy_p,jz_p
integer,dimension(:,:),allocatable :: ipot_p,ipot_w

real                               :: temp,aa
integer                            :: i,ip,iw,j,jp,jw
integer                            :: k,kp,kw
! ----------------------------------------------------------------------
real :: dima_sca=0, dima_sca_b=0
real,   dimension(  :),allocatable :: jwx2,jwy2,jwz2,jpx2,jpy2,jpz2
integer :: j_loc,i_loc
logical :: inside_j,inside_i


if(rank==0) write(*,*) 'compute matrix_wp, npot_p=', npot_p, 'npot_w=', npot_w 


allocate(dxp(ntri_p,3), dyp(ntri_p,3), dzp(ntri_p,3),              &
         dxw(ntri_w,3), dyw(ntri_w,3), dzw(ntri_w,3),              &
         jpx(ntri_w), jpy(ntri_w), jpz(ntri_w),                    &
         jwx(ntri_p), jwy(ntri_p), jwz(ntri_p),                    &
         jx( ntri_w), jy( ntri_w), jz( ntri_w),                    &
         jx_p(ntri_p),jy_p(ntri_p),jz_p(ntri_p),                   &
         ipot_p(ntri_p,3),ipot_w(ntri_w,3),stat=ier) 
         if (ier /= 0) then
             print *,'matrix_wp.f90: Can not allocate local arrays a_wp, ..., ipot_w'
             stop
         endif

dxp=0.; dyp=0.; dzp=0.; dxw=0.; dyw=0.; dzw=0.
jpx=0.; jpy=0.; jpz=0.; jwx=0.; jwy=0.; jwz=0.
jx=0.; jy=0.; jz=0.; jx_p=0.; jy_p=0.; jz_p=0.; ipot_p=0; ipot_w=0 

! ----------------------------------------------------------------------

! Additional arrays that will be used for tri_induct subroutine
! For the production run with n_tri_w=5*10^5 the size of all array will be around 300Mb
! if they will not be distributed among MPI tasks

 allocate(nx(ntri_w),ny(ntri_w),nz(ntri_w),tx1(ntri_w),         &
          ty1(ntri_w),tz1(ntri_w),tx2(ntri_w),ty2(ntri_w),      &
          tz2(ntri_w),tx3(ntri_w),ty3(ntri_w),tz3(ntri_w),      &
          d221(ntri_w),d232(ntri_w),d213(ntri_w),area(ntri_w),  &
          xm(7*ntri_p), ym(7*ntri_p), zm(7*ntri_p),             &
          jwx2(ntri_p),  jwy2(ntri_p),  jwz2(ntri_p),           &
          jpx2(ntri_w),  jpy2(ntri_w),  jpz2(ntri_w), stat=ier)
          if (ier /= 0) then
             print *,'Matrix_wp.f90: Can not allocate local arrays nx, ..., zm'
             stop
          endif

nx=0.; ny=0.; nz=0.; tx1=0.; ty1=0.;tz1=0.; tx2=0.
ty2=0.; tz2=0.; tx3=0.; ty3=0.; tz3=0.; d221=0.
d232=0.; d213=0.;area=0.; xm=0.; ym=0.; zm=0.
jpx2=0.; jpy2=0.; jpz2=0.; jwx2=0.; jwy2=0.; jwz2=0.


 allocate(nx_b(ntri_p),ny_b(ntri_p),nz_b(ntri_p),tx1_b(ntri_p),         &
          ty1_b(ntri_p),tz1_b(ntri_p),tx2_b(ntri_p),ty2_b(ntri_p),      &
          tz2_b(ntri_p),tx3_b(ntri_p),ty3_b(ntri_p),tz3_b(ntri_p),      &
          d221_b(ntri_p),d232_b(ntri_p),d213_b(ntri_p),area_b(ntri_p),  &
          xm_b(7*ntri_w), ym_b(7*ntri_w), zm_b(7*ntri_w),stat=ier)
          if (ier /= 0) then
             print *,'Matrix_wp.f90: Can not allocate local arrays nx_b, ..., zm_b'
             stop
         endif

nx_b=0.; ny_b=0.; nz_b=0.; tx1_b=0.; ty1_b=0.;tz1_b=0.; tx2_b=0.
ty2_b=0.; tz2_b=0.; tx3_b=0.; ty3_b=0.; tz3_b=0.; d221_b=0.
d232_b=0.; d213_b=0.;area_b=0.; xm_b=0.; ym_b=0.; zm_b=0.
! ----------------------------------------------------------------------

ipot_p = jpot_p
ipot_w = jpot_w


do i = 1,ntri_p
  dxp(i,1) = xp(i,2) - xp(i,3)
  dyp(i,1) = yp(i,2) - yp(i,3)
  dzp(i,1) = zp(i,2) - zp(i,3)
  dxp(i,2) = xp(i,3) - xp(i,1)
  dyp(i,2) = yp(i,3) - yp(i,1)
  dzp(i,2) = zp(i,3) - zp(i,1)
  dxp(i,3) = xp(i,1) - xp(i,2)
  dyp(i,3) = yp(i,1) - yp(i,2)
  dzp(i,3) = zp(i,1) - zp(i,2)

  call tri_induct_1(i,xp,yp,zp,ntri_p)
  call tri_induct_2_b(ntri_p,i,xp,yp,zp)

enddo

do i = 1,ntri_w
  dxw(i,1) = xw(i,2) - xw(i,3)
  dyw(i,1) = yw(i,2) - yw(i,3)
  dzw(i,1) = zw(i,2) - zw(i,3)
  dxw(i,2) = xw(i,3) - xw(i,1)
  dyw(i,2) = yw(i,3) - yw(i,1)
  dzw(i,2) = zw(i,3) - zw(i,1)
  dxw(i,3) = xw(i,1) - xw(i,2)
  dyw(i,3) = yw(i,1) - yw(i,2)
  dzw(i,3) = zw(i,1) - zw(i,2)
  
  call tri_induct_1_b(i,xw,yw,zw,ntri_w)
  call tri_induct_2(ntri_w,i,xw,yw,zw)

enddo

do i = 1,ntri_w
  jx(i) = phi0_w(i,1)*dxw(i,1)+phi0_w(i,2)*dxw(i,2)+phi0_w(i,3)*dxw(i,3)
  jy(i) = phi0_w(i,1)*dyw(i,1)+phi0_w(i,2)*dyw(i,2)+phi0_w(i,3)*dyw(i,3)
  jz(i) = phi0_w(i,1)*dzw(i,1)+phi0_w(i,2)*dzw(i,2)+phi0_w(i,3)*dzw(i,3)
enddo


do i = 1,ntri_p
  jx_p(i) = phi0_p(i,1)*dxp(i,1)+phi0_p(i,2)*dxp(i,2)+phi0_p(i,3)*dxp(i,3)
  jy_p(i) = phi0_p(i,1)*dyp(i,1)+phi0_p(i,2)*dyp(i,2)+phi0_p(i,3)*dyp(i,3)
  jz_p(i) = phi0_p(i,1)*dzp(i,1)+phi0_p(i,2)*dzp(i,2)+phi0_p(i,3)*dzp(i,3)
enddo

   ! Set up scalapack sub grid
   call SCA_GRID(npot_w+ncoil,npot_p)
   ! allocate local matrix
   allocate(a_wp_loc(MP_A,NQ_A), stat=ier)
   IF (IER /= 0) THEN
             WRITE (*,*) "Matrrix wp: Can not allocate local matrix A_loc_wp : MY_PROC_NUM=",MYPNUM
             STOP
   END IF
   LDA_wp=LDA_A   

    a_wp_loc=0.

    step=ntri_w/NPROCS
    ntri_w_loc_b=(rank)*step+1
    ntri_w_loc_e=(rank+1)*step
    if(rank==NPROCS-1) ntri_w_loc_e=ntri_w

do i=ntri_w_loc_b,ntri_w_loc_e
  do k=1,ntri_p

    dima_sca=0
    call tri_induct_3_2(ntri_w,i,k,xw,yw,zw,dima_sca)  !dima
   
    dima_sca_b=0
    call tri_induct_3_2_b(ntri_p,k,i,xp,yp,zp,dima_sca_b) !dimb

    jpx(i)= jpx(i) + jx_p(k)*.5*(dima_sca+dima_sca_b)
    jpy(i)= jpy(i) + jy_p(k)*.5*(dima_sca+dima_sca_b)
    jpz(i)= jpz(i) + jz_p(k)*.5*(dima_sca+dima_sca_b)

  enddo
enddo


do i=1,ntri_p
  do k=ntri_w_loc_b,ntri_w_loc_e

    dima_sca=0
    call tri_induct_3_2(ntri_w,k,i,xw,yw,zw,dima_sca)  !dima

    dima_sca_b=0
   call tri_induct_3_2_b(ntri_p,i,k,xp,yp,zp,dima_sca_b) !dimb

    jwx(i)= jwx(i) + jx(k)*.5*(dima_sca+dima_sca_b)
    jwy(i)= jwy(i) + jy(k)*.5*(dima_sca+dima_sca_b)
    jwz(i)= jwz(i) + jz(k)*.5*(dima_sca+dima_sca_b)

  enddo
enddo


    CALL MPI_ALLREDUCE(jpx, jpx2, ntri_w, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
    CALL MPI_ALLREDUCE(jpy, jpy2, ntri_w, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
    CALL MPI_ALLREDUCE(jpz, jpz2, ntri_w, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
 
    jpx=jpx2
    jpy=jpy2
    jpz=jpz2

    deallocate(jpx2,jpy2,jpz2)

    CALL MPI_ALLREDUCE(jwx, jwx2, ntri_p, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
    CALL MPI_ALLREDUCE(jwy, jwy2, ntri_p, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
    CALL MPI_ALLREDUCE(jwz, jwz2, ntri_p, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)

    jwx=jwx2
    jwy=jwy2
    jwz=jwz2

    deallocate(jwx2,jwy2,jwz2)



do i=1,ntri_p
  do k=1,3
    if (jpot_p(i,k) .eq. npot_p) then
      dxp(i,k) = 0.
      dyp(i,k) = 0.
      dzp(i,k) = 0.
      ipot_p(i,k) = 1
    endif
  enddo
enddo


do i=1,ntri_w
  do k=1,3
    if (jpot_w(i,k) .eq. npot_w) then
      dxw(i,k) = 0.
      dyw(i,k) = 0.
      dzw(i,k) = 0.
      ipot_w(i,k) = 1
    endif
  enddo
enddo



do iw =1,ntri_w
  do ip=1,ntri_p
     do kw =1,3
        jw = ipot_w(iw,kw) + 1
        call  ScaLAPACK_mapping_i(jw+ncoil,i_loc,inside_i)
        if (inside_i) then! If not inside       
            do kp=1,3
            jp = ipot_p(ip,kp) + 1
                 call ScaLAPACK_mapping_j(jp,j_loc,inside_j)
                 if (inside_j) then! If not inside
                   dima_sca=0
                   dima_sca_b=0
                   call tri_induct_3_2(ntri_w,iw,ip,xw,yw,zw,dima_sca)  !dima
                   call tri_induct_3_2_b(ntri_p,ip,iw,xp,yp,zp,dima_sca_b) !dimb

                   temp =.5*(dxp(ip,kp)*dxw(iw,kw) + dyp(ip,kp)*dyw(iw,kw) + &
                             dzp(ip,kp)*dzw(iw,kw)) * (dima_sca+dima_sca_b)

                   a_wp_loc(i_loc,j_loc) = a_wp_loc(i_loc,j_loc) + temp

                endif
             enddo
         endif
    enddo
  enddo
enddo
!===================================================================

call ScaLAPACK_mapping_i(1+ncoil,i_loc,inside_i)
    if (inside_i) then

        call ScaLAPACK_mapping_j(1,j_loc,inside_j)
        if (inside_j) then

            aa = 0.
            do i=1,ntri_w
                  aa= aa +jx(i)*jpx(i)+jy(i)*jpy(i)+jz(i)*jpz(i)
            enddo

            a_wp_loc(i_loc,j_loc) = aa

        endif
    endif

do i=1,ntri_p
  do k=1,3
    j = ipot_p(i,k)
    call ScaLAPACK_mapping_i(1+ncoil,i_loc,inside_i)
         if (inside_i) then
              call ScaLAPACK_mapping_j(1+j,j_loc,inside_j)
                  if (inside_j) then

                      temp  = jwx(i)*dxp(i,k) +jwy(i)*dyp(i,k) +jwz(i)*dzp(i,k)
                      a_wp_loc(i_loc,j_loc) = a_wp_loc(i_loc,j_loc) +temp
                  endif
         endif
  enddo
enddo


do i=1,ntri_w
  do k=1,3
    j = ipot_w(i,k)
    call ScaLAPACK_mapping_i(1+ncoil+j,i_loc,inside_i)
         if (inside_i) then
              call ScaLAPACK_mapping_j(1,j_loc,inside_j)
                  if (inside_j) then
                      temp  = jpx(i)*dxw(i,k) + jpy(i)*dyw(i,k) +jpz(i)*dzw(i,k)
                      a_wp_loc(i_loc,j_loc) = a_wp_loc(i_loc,j_loc) +temp
                  endif
         endif
  enddo
enddo



deallocate(ipot_w,ipot_p,jz_p,jy_p,jx_p,jz,jy,jx,jwz,jwy,jwx,       & 
           jpz,jpy,jpx,dzw,dyw,dxw,dzp,dyp,dxp           )

deallocate(nx,ny,nz,tx1,ty1,tz1,tx2,ty2, &
          tz2,tx3,ty3,tz3,d221,d232,d213,area,xm,ym,zm)

deallocate(nx_b,ny_b,nz_b,tx1_b,ty1_b,tz1_b,tx2_b,ty2_b, &
          tz2_b,tx3_b,ty3_b,tz3_b,d221_b,d232_b,d213_b,area_b,xm_b,ym_b,zm_b)

if(rank==0) write(*,*) 'matrix_wp done'
if(rank==0) write(*,*) '==============================================================='

end subroutine matrix_wp
