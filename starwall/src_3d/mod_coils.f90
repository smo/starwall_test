!> Module coils: Contains data structures for coils, coil sets, and related routines.
!! Fills module coil_tria with the information needed for STARWALL matrix construction.
!! 
!! * Public routine:
!!   * get_coil_data:         Get all coil data from input files and create triangles
!!   * export_coil_triangles: Write out ascii and vtk files for all coil sets
!!   * cleanup_coil_module:   Clean up data structures
!!
!! ===================================================================================
!! Derivation for eta_thin for the various coil types:
!! -----------------------------------------------------------------------------------
!! 
!!  Res                       Resistance in Ohm
!!  eta      = Res * A / l    Resistivity on Ohm m
!!  eta_thin = eta / d        Thin wall resistivity
!!
!! 1) AXISYM_THICK coil type
!!
!!   nR                       Number of bands in R direction
!!   nZ                       Number of bands in Z direction
!!   i                        Band index in R direction
!!   j                        Band index in Z direction
!!   A_i,j = A / (nR * nZ)    Surface of coil cross-section divided by bands
!!   
!!   Res_i,j = Res * nR * nZ                ! Distribute resistance across bands
!!   eta_i,j = Res_i,j * A_i,j / l_i
!!           = Res_i,j * [ dR * dZ / (nR * nZ) ] / ( 2pi * R_i )
!!   eta_thin,i,j = eta / d
!!                = eta * n_R / dR
!!                = Res_i,j * dZ / ( 2pi * R_i * nZ )
!!                = Res * nR * dZ / ( 2pi * R_i )
!!   (That means eta_thin is different for each band in R direction)
!!
!! 2) AXISYM_FILA coil type
!!
!!   n                        Number of filaments (and thus also of bands)
!!   w                        Width of band for each filament
!!   A_i = d * w              "Surface" in cross section for each filament
!!   L_i = 2pi * R_i          Length of each filament
!!   nturns_i                 Number of windings repesented by the filament
!!   nturns = sum_i nturns_i  Total number of windings in the coil
!!
!!   Res_i = Res * nturns / nturns_i
!!   eta_i = Res_i * d * w / (2pi * R_i)
!!   eta_thin,i = eta / d = Res_i * w / (2pi * R_i)
!!              = Res * nturns * w / (2pi * R_i * nturns_i)
!!
!! 3) GENERAL_THIN coil type
!!
!!   L                        "Length" of the coil
!!   w                        Width of the triangle band representing the coil
!!
!!   eta      = Res * d * w / L
!!   eta_thin = eta / d = Res * w / L
!!
!! ===================================================================================
module coils
  
  
  
  implicit none
  
  
  
  private
  
  ! --- Public derived data types
  public t_coil_set
  ! --- Public routines
  public get_coil_data, export_coil_triangles, cleanup_coil_module
  
  
  
  ! --- Some module constants
  integer, parameter              :: N_MAX_FILA  = 2001       !< Maximum number of filaments per coil
  integer, parameter              :: N_MAX_PTS   = 2001       !< Maximum number of points per coil
  logical, parameter              :: COIL_DEBUG  = .false.     !< Switch on to get debugging output
  real,    parameter              :: PI2         = 4.d0*asin(1.d0) !< 2*PI, should be moved somewhere else...
  integer, parameter              :: IOCH        = 167        !< Channel for file I/O
  real,    parameter              :: FILA_DZ     = 0.003      !< Width of triangle bands for AXISYM_FILA coils
  
  ! --- Constants for coil (geometrical) types
  character(len=12), parameter          :: AXISYM_THICK = 'axisym_thick' !< Axisymmetric coils with dR,dZ extent
  character(len=12), parameter          :: AXISYM_FILA  = 'axisym_fila ' !< Axisymmetric coils specified by filaments
  character(len=12), parameter          :: GENERAL_THIN = 'general_thin' !< 3D coils specified by a list of points
  
  logical :: was_initialized = .false.
  
  ! --- Data structure for a single coil of arbitrary type
  type :: t_coil
    ! --- For all coil types
    character(len=128)   :: name     = 'NONE'        !< Name of the coil
    character(len=32)    :: coil_type                !< Coil type according to constants specified above
    real                 :: resist   = -999.         !< Total coil resistivity in Ohm
    real                 :: nturns   = 0.            !< Total number of turns in the coil
    integer              :: ntorpts  = 0             !< Number of toroidal points (axisym coils)
    ! --- For axisymmetric coils with dR and dZ extent
    real                 :: R        = -999.         !< Position in R (center of coil)
    real                 :: Z        = -999.         !< Position in Z (center of coil)
    real                 :: dR       = -999.         !< Full width in R
    real                 :: dZ       = -999.         !< Full width in Z
    integer              :: nbands_R = 0             !< Discretize coil by triangle bands in R direction
    integer              :: nbands_Z = 0             !< Discretize coil by triangle bands in Z direction
    ! --- For coils specified by filaments
    integer              :: n_fila   = 0             !< Number of filaments for the coil
    real                 :: n_fila_turns(N_MAX_FILA) = 1. !< Number of turns in each filament
    real                 :: R_fila(N_MAX_FILA) = -999. !< R-position of filaments
    real                 :: Z_fila(N_MAX_FILA) = -999. !< Z-position of filaments
    ! --- For 3D coil specified by a list of points
    integer              :: n_pts    = 0             !< Number of points along the coil
    real                 :: width    = 0.            !< Width of coil
    real                 :: xpts(N_MAX_PTS) = -999.  !< x-Position of points
    real                 :: ypts(N_MAX_PTS) = -999.  !< y-Position of points
    real                 :: zpts(N_MAX_PTS) = -999.  !< z-Position of points
  end type t_coil
  
  ! --- Data structure representing coil set triangles
  type :: t_triangles
    integer              :: ntri_c       = 0         !< Number of coil triangles
    integer, allocatable :: jtri_c(:)                !< Number of triangles of each coil
    real,    allocatable :: x_coil(:,:)              !< x-position of coil triangle nodes
    real,    allocatable :: y_coil(:,:)              !< y-position of coil triangle nodes
    real,    allocatable :: z_coil(:,:)              !< z-position of coil triangle nodes
    real,    allocatable :: phi_coil(:,:)            !< "Current factor" (for bands and turns)
    real,    allocatable :: eta_thin_coil(:)         !< Thin wall resistivity of each coil triangle
  end type t_triangles
  
  ! --- Data structure for a coil set
  type :: t_coil_set
    character(len=32)    :: name         = 'NONE'    !< Name of coil set (should not be changed by user)
    character(len=256)   :: description  = 'NONE'    !< Human readable description
    integer              :: ncoil        = 0         !< Number of coils in the set
    integer              :: index_start  = -999      !< Index of first coil of this set in all_coil_set
    type(t_coil), allocatable :: coil(:)             !< Coils in this coil set
    type(t_triangles)    :: tri                      !< Triangle representation of the coil set
  end type t_coil_set
  
  ! --- Triangle band structure (auxilliary structure for constructing triangles)
  type :: t_triangle_band
    integer              :: len                      !< Length of band in points
    real,    allocatable :: x(:,:), y(:,:), z(:,:)   !< Coordinates of points along band
    real,    allocatable :: phi(:,:)                 !< "Potential" to account for several turns and bands
  end type t_triangle_band
  
  ! --- Global information about all coils and coil triangles
  type(t_coil_set)       :: all_coil_set             !< All coils as contained also in following structures
  type(t_coil_set)       :: pol_coil_set             !< Poloidal field coils
  type(t_coil_set)       :: rmp_coil_set             !< External perturbation coils
  type(t_coil_set)       :: voltage_coil_set         !< Coils with prescribed voltage in JOREK
  type(t_coil_set)       :: diag_coil_set            !< Coils for virtual diagnostics
  
  
  
  contains
  
  
  
  !> Initialize the coil module, should be called before any other routine.
  subroutine init_coil_module(rank)
    
    ! --- Routine parameters
    integer, intent(in) :: rank
    
    call dealloc_coil_set(all_coil_set)
    call dealloc_coil_set(pol_coil_set)
    call dealloc_coil_set(rmp_coil_set)
    call dealloc_coil_set(voltage_coil_set)
    call dealloc_coil_set(diag_coil_set)
    all_coil_set%name     = 'all_coil_set'
    pol_coil_set%name     = 'pol_coil_set'
    rmp_coil_set%name     = 'rmp_coil_set'
    voltage_coil_set%name = 'voltage_coil_set'
    diag_coil_set%name    = 'diag_coil_set'
    
    was_initialized = .true.
    
  end subroutine init_coil_module
  
  
  
  !> Initialize the coil module, should be called before any other routine.
  subroutine cleanup_coil_module(rank)
    
    ! --- Routine parameters
    integer, intent(in) :: rank
    
    call init_coil_module(rank)
    
    was_initialized = .false.
    
  end subroutine cleanup_coil_module
  
  
  
  !> Deallocate information stored in a coil set
  subroutine dealloc_coil_set(coil_set)
    
    ! --- Routine parameters
    type(t_coil_set), intent(inout)  :: coil_set
    
    !coil_set%name is not touched intentionally
    coil_set%description = 'NONE'
    coil_set%ncoil       = 0
    coil_set%index_start = -999
    if ( allocated(coil_set%coil) ) deallocate( coil_set%coil )
    call dealloc_triangles(coil_set%tri)
    
  end subroutine dealloc_coil_set
  
  
  
  subroutine get_coil_data(pol_coil_file, rmp_coil_file, voltage_coil_file, diag_coil_file, rank)
    
    ! --- Routine parameters
    character(len=*), intent(in) :: pol_coil_file, rmp_coil_file, voltage_coil_file, diag_coil_file
    integer,          intent(in) :: rank
    
    if ( pol_coil_file     /= 'none' ) call read_coil_set(trim(pol_coil_file),     pol_coil_set,     rank)
    if ( rmp_coil_file     /= 'none' ) call read_coil_set(trim(rmp_coil_file),     rmp_coil_set,     rank)
    if ( voltage_coil_file /= 'none' ) call read_coil_set(trim(voltage_coil_file), voltage_coil_set, rank)
    if ( diag_coil_file    /= 'none' ) call read_coil_set(trim(diag_coil_file),    diag_coil_set,    rank)
    call collect_all_coil_info(rank)
    
  end subroutine get_coil_data



  !> Print information contained in a coil set
  subroutine print_triangles(tri)
    
    ! --- Routine parameters
    type(t_triangles), intent(in)  :: tri
    
    201 format(1x,a,999i13)
    203 format(1x,a,es13.6)
    
    write(*,201)
    write(*,201) '    print_triangles:'
    write(*,201) '      ntri_c      = ', tri%ntri_c
    if ( allocated(tri%jtri_c) ) write(*,201) '      jtri_c      = ', tri%jtri_c(:)
    if ( allocated(tri%x_coil) ) then
      write(*,201) '      x_coil:'
      write(*,203) '        min:      : ', minval(tri%x_coil(:,:))
      write(*,203) '        max:      : ', maxval(tri%x_coil(:,:))
    end if
    if ( allocated(tri%y_coil) ) then
      write(*,201) '      y_coil:'
      write(*,203) '        min:      : ', minval(tri%y_coil(:,:))
      write(*,203) '        max:      : ', maxval(tri%y_coil(:,:))
    end if
    if ( allocated(tri%z_coil) ) then
      write(*,201) '      z_coil:'
      write(*,203) '        min:      : ', minval(tri%z_coil(:,:))
      write(*,203) '        max:      : ', maxval(tri%z_coil(:,:))
    end if
    if ( allocated(tri%phi_coil) ) then
      write(*,201) '      phi_coil:'
      write(*,203) '        min:      : ', minval(tri%phi_coil(:,:))
      write(*,203) '        max:      : ', maxval(tri%phi_coil(:,:))
    end if
    if ( allocated(tri%eta_thin_coil) ) then
      write(*,201) '      eta_thin_coil:'
      write(*,203) '        min:      : ', minval(tri%eta_thin_coil(:))
      write(*,203) '        max:      : ', maxval(tri%eta_thin_coil(:))
    end if
    write(*,201) '    Finished print_triangles'
    
  end subroutine print_triangles
  
  
  
  !> Print information contained in a coil set
  subroutine print_coil_set(coil_set, verbose)
    
    ! --- Routine parameters
    type(t_coil_set), intent(in)  :: coil_set
    logical,          intent(in)  :: verbose
    
    ! --- Local variables
    integer :: i
    
    100 format(1x,99a)
    101 format(1x,a,99i13)
    102 format(1x,a,i3.3,99a)
    103 format(1x,a,es13.6)
    104 format(1x,a,f13.6)
    
    if (COIL_DEBUG) write(*,*)
    if (COIL_DEBUG) write(*,100) '  print_coil_set ', trim(coil_set%name), ':'
    write(*,100) '    description   = ', trim(coil_set%description)
    write(*,101) '    ncoil         = ', coil_set%ncoil
    if ( COIL_DEBUG .or. verbose ) write(*,101) '    index_start   = ', coil_set%index_start
    write(*,101) '    ntri_c        = ', coil_set%tri%ntri_c
    
    do i = 1, coil_set%ncoil
      
      if ( COIL_DEBUG ) then
        write(*,*)
        write(*,102) '    Coil ', i, ': ', trim(coil_set%coil(i)%name)
      else
        write(*,102) '    Coil ', i, ': ', trim(coil_set%coil(i)%name), ' (', trim(coil_set%coil(i)%coil_type), ')'
      end if
      
      if ( COIL_DEBUG .or. verbose ) then
        write(*,100) '      coil_type   = ', trim(coil_set%coil(i)%coil_type)
        write(*,103) '      resist      = ', coil_set%coil(i)%resist
        write(*,103) '      nturns      = ', coil_set%coil(i)%nturns
        write(*,101) '      ntorpts     = ', coil_set%coil(i)%ntorpts
        
        if ( coil_set%coil(i)%coil_type == AXISYM_THICK ) then
          write(*,104) '      R           = ', coil_set%coil(i)%R
          write(*,104) '      Z           = ', coil_set%coil(i)%Z
          write(*,104) '      dR          = ', coil_set%coil(i)%dR
          write(*,104) '      dZ          = ', coil_set%coil(i)%dZ
          write(*,101) '      nbands_R    = ', coil_set%coil(i)%nbands_R
          write(*,101) '      nbands_Z    = ', coil_set%coil(i)%nbands_Z
        else if ( coil_set%coil(i)%coil_type == AXISYM_FILA ) then
          write(*,101) '      n_fila      = ', coil_set%coil(i)%n_fila
          write(*,100) '      n_fila_turns:'
          write(*,103) '        min:        ', minval(coil_set%coil(i)%n_fila_turns(1:coil_set%coil(i)%n_fila))
          write(*,103) '        max:        ', maxval(coil_set%coil(i)%n_fila_turns(1:coil_set%coil(i)%n_fila))
          write(*,100) '      R_fila:'
          write(*,103) '        min:        ', minval(coil_set%coil(i)%R_fila(1:coil_set%coil(i)%n_fila))
          write(*,103) '        max:        ', maxval(coil_set%coil(i)%R_fila(1:coil_set%coil(i)%n_fila))
          write(*,100) '      Z_fila:'
          write(*,103) '        min:        ', minval(coil_set%coil(i)%Z_fila(1:coil_set%coil(i)%n_fila))
          write(*,103) '        max:        ', maxval(coil_set%coil(i)%Z_fila(1:coil_set%coil(i)%n_fila))
        else if ( coil_set%coil(i)%coil_type == GENERAL_THIN ) then
          write(*,101) '      n_pts       = ', coil_set%coil(i)%n_pts
          write(*,103) '      width       = ', coil_set%coil(i)%width
          write(*,100) '      xpts:'
          write(*,103) '        min:        ', minval(coil_set%coil(i)%xpts(1:coil_set%coil(i)%n_pts))
          write(*,103) '        max:        ', maxval(coil_set%coil(i)%xpts(1:coil_set%coil(i)%n_pts))
          write(*,100) '      ypts:'
          write(*,103) '        min:        ', minval(coil_set%coil(i)%ypts(1:coil_set%coil(i)%n_pts))
          write(*,103) '        max:        ', maxval(coil_set%coil(i)%ypts(1:coil_set%coil(i)%n_pts))
          write(*,100) '      zpts:'
          write(*,103) '        min:        ', minval(coil_set%coil(i)%zpts(1:coil_set%coil(i)%n_pts))
          write(*,103) '        max:        ', maxval(coil_set%coil(i)%zpts(1:coil_set%coil(i)%n_pts))
        end if
        
      end if
      
    end do
    
    if (COIL_DEBUG) call print_triangles(coil_set%tri)
    
    if (COIL_DEBUG) write(*,*) '  Finished print_coil_set'
    
  end subroutine print_coil_set
  
  
  
  ! --- Check coil_set for consistency
  subroutine check_coil_set(coil_set, accept_empty, warn_only, check_index_start, check_tria)
  
    ! --- Routine parameters
    type(t_coil_set), intent(in) :: coil_set
    logical,          intent(in) :: accept_empty         !< Silently accept empty coil set
    logical,          intent(in) :: warn_only            !< Warn instead of stop
    logical,          intent(in) :: check_index_start    !< Check also index_start
    logical,          intent(in) :: check_tria           !< Check also triangles
    
    ! --- Local variables
    integer :: i
    
    if (COIL_DEBUG) write(*,*)
    if (COIL_DEBUG) write(*,'(2a)') '    check_coil_set: ', trim(coil_set%name)
    
    if ( accept_empty .and. (coil_set%ncoil == 0) ) then
      if ( COIL_DEBUG ) write(*,*) '      skipping this coil set since ncoil==0.'
      return
    end if
    
    if (COIL_DEBUG) write(*,*) '      check set itself'
    if ( coil_set%ncoil < 1 ) then
      write(*,*) 'ERROR: coil_set%ncoil < 1'
      if ( .not. warn_only ) stop
    else if ( .not. allocated(coil_set%coil) ) then
      write(*,*) 'ERROR: coil_set%coil(:) not allocated'
      if ( .not. warn_only ) stop
    else if ( coil_set%ncoil /= size(coil_set%coil,1) ) then
      write(*,*) 'ERROR: coil_set%ncoil /= size(coil_set%coil,1)'
      if ( .not. warn_only ) stop
    else if ( check_index_start .and. (coil_set%name /= 'all_coil_set') .and. (coil_set%index_start<1) ) then
      write(*,*) 'ERROR: coil_set%index_start is not initialized'
      if ( .not. warn_only ) stop
    end if
    if (COIL_DEBUG) write(*,*) '      finished check set itself'
    
    do i = 1, coil_set%ncoil
      if (COIL_DEBUG) write(*,*) '      check coil ', i
      call check_coil(coil_set%coil(i), warn_only)
      if (COIL_DEBUG) write(*,*) '      finished check coil'
    end do
    
    if ( check_tria ) then
      if (COIL_DEBUG) write(*,*) '      check triangles'
      call check_triangles(coil_set%tri, warn_only)
      if (COIL_DEBUG) write(*,*) '      finished check triangles'
    end if
    
    if (COIL_DEBUG) write(*,*) '    Finished check_coil_set'
    
  end subroutine check_coil_set
  
  
  
  
  
  
  ! --- Check coil_set for consistency
  subroutine check_coil(coil, warn_only)
  
    ! --- Routine parameters
    type(t_coil), intent(in) :: coil
    logical,      intent(in) :: warn_only
    
    ! --- Local variables
    real    :: dist
    integer :: i
    
    if ( coil%name == 'NONE' ) then
      write(*,*) 'ERROR: coil%name not initialized'
      if ( .not. warn_only ) stop
    else if ( coil%resist < 0. ) then
      write(*,*) 'ERROR: coil%resist < 0.'
      if ( .not. warn_only ) stop
    else if ( coil%nturns <= 0.  ) then
      write(*,*) 'ERROR: coil%nturns <= 0'
      if ( .not. warn_only ) stop
    else if ( (coil%ntorpts < 2) .and. ( (coil%coil_type==AXISYM_THICK) .or. (coil%coil_type==AXISYM_FILA) ) ) then
      write(*,*) 'ERROR: coil%ntorpts < 2'
      if ( .not. warn_only ) stop
    end if
    
    if ( coil%coil_type == AXISYM_THICK ) then
      if ( coil%R < 0. ) then
        write(*,*) 'ERROR: coil%R < 0'
        if ( .not. warn_only ) stop
      else if ( coil%Z < -100. ) then
        write(*,*) 'ERROR: coil%Z < -100'
        if ( .not. warn_only ) stop
      else if ( coil%dR < 0. ) then
        write(*,*) 'ERROR: coil%dR < 0'
        if ( .not. warn_only ) stop
      else if ( coil%dZ < 0. ) then
        write(*,*) 'ERROR: coil%dZ < 0'
        if ( .not. warn_only ) stop
      else if ( coil%nbands_R < 1 ) then
        write(*,*) 'ERROR: coil%nbands_R < 1'
        if ( .not. warn_only ) stop
      else if ( coil%nbands_Z < 1) then
        write(*,*) 'ERROR: coil%nbands_Z < 1'
        if ( .not. warn_only ) stop
      end if
    else if ( coil%coil_type == AXISYM_FILA ) then
      if ( coil%n_fila < 1 ) then
        write(*,*) 'ERROR: coil%n_fila < 1'
        if ( .not. warn_only ) stop
      else if ( minval(coil%n_fila_turns(1:coil%n_fila)) <= 0. ) then
        write(*,*) 'ERROR: at least some coil%n_fila_turns values are < 0'
        if ( .not. warn_only ) stop
      else if ( minval(coil%R_fila(1:coil%n_fila)) < 0. ) then
        write(*,*) 'ERROR: at least some coil%R_fila values are < 0'
        if ( .not. warn_only ) stop
      else if ( minval(coil%R_fila(1:coil%n_fila)) < -100. ) then
        write(*,*) 'ERROR: at least some coil%R_fila values are < -100'
        if ( .not. warn_only ) stop
      end if
    else if ( coil%coil_type == GENERAL_THIN ) then
      if ( coil%n_pts < 1 ) then
        write(*,*) 'ERROR: coil%n_pts < 1'
        if ( .not. warn_only ) stop
      else if ( coil%width <= 0. )   then 
        write(*,*) 'ERROR: width of coil <= 0.'
        if ( .not. warn_only) stop
      else if ( minval(coil%xpts(1:coil%n_pts)) < -100. ) then
        write(*,*) 'ERROR: at least some coil%xpts values are < -100.'
        if ( .not. warn_only ) stop
      else if ( minval(coil%ypts(1:coil%n_pts)) < -100. ) then
        write(*,*) 'ERROR: at least some coil%ypts values are < -100.'
        if ( .not. warn_only ) stop
      else if ( minval(coil%zpts(1:coil%n_pts)) < -100. ) then
        write(*,*) 'ERROR: at least some coil%zpts values are < -100.'
        if ( .not. warn_only ) stop
      end if
      dist = sqrt( (coil%xpts(1)-coil%xpts(coil%n_pts))**2 &
                 + (coil%ypts(1)-coil%ypts(coil%n_pts))**2 &
                 + (coil%zpts(1)-coil%zpts(coil%n_pts))**2 )
      if ( dist > 1.e-8 ) then
        write(*,*) 'ERROR: coil represented by points is not closed in itself.'
        if ( .not. warn_only ) stop
      end if
      ! --- check for duplicate points
      do i = 1, coil%n_pts-1
        dist = sqrt( (coil%xpts(i)-coil%xpts(i+1))**2 &
                   + (coil%ypts(i)-coil%ypts(i+1))**2 &
                   + (coil%zpts(i)-coil%zpts(i+1))**2 )
        if ( dist < 1.e-8 ) then
          write(*,*) 'ERROR: coil contains duplicate points.'
          write(*,*) '  coil%name=', trim(coil%name)
          write(*,*) '  points number ', i, i+1
          if ( .not. warn_only ) stop
        end if
      end do
    else
      write(*,*) 'ERROR: coil%coil_type illegal: ', trim(coil%coil_type)
      if ( .not. warn_only ) stop
    end if
    
  end subroutine check_coil
  
  
  
  ! --- Check coil_set for consistency
  subroutine check_triangles(tri, warn_only)
  
    ! --- Routine parameters
    type(t_triangles), intent(in) :: tri
    logical,           intent(in) :: warn_only
    
    if ( tri%ntri_c <= 0 ) then
      write(*,*) 'ERROR: tri%ntri_c <=0'
      if ( .not. warn_only ) stop
    else if ( .not. allocated(tri%jtri_c) ) then
      write(*,*) 'ERROR: tri%jtri_c is not allocated'
      if ( .not. warn_only ) stop
    else if ( minval(tri%jtri_c) < 1 ) then
      write(*,*) 'ERROR: at least one tri%jtri_c value < 1'
      if ( .not. warn_only ) stop
    else if ( .not. allocated(tri%x_coil) ) then
      write(*,*) 'ERROR: tri%x_coil is not allocated'
      if ( .not. warn_only ) stop
    else if ( .not. allocated(tri%y_coil) ) then
      write(*,*) 'ERROR: tri%y_coil is not allocated'
      if ( .not. warn_only ) stop
    else if ( .not. allocated(tri%z_coil) ) then
      write(*,*) 'ERROR: tri%z_coil is not allocated'
      if ( .not. warn_only ) stop
    else if ( (size(tri%x_coil,1) /= tri%ntri_c) .or. (size(tri%x_coil,2) /= 3) ) then
      write(*,*) 'ERROR: tri%x_coil has wrong size'
      if ( .not. warn_only ) stop
    else if ( (size(tri%y_coil,1) /= tri%ntri_c) .or. (size(tri%y_coil,2) /= 3) ) then
      write(*,*) 'ERROR: tri%y_coil has wrong size'
      if ( .not. warn_only ) stop
    else if ( (size(tri%z_coil,1) /= tri%ntri_c) .or. (size(tri%z_coil,2) /= 3) ) then
      write(*,*) 'ERROR: tri%z_coil has wrong size'
      if ( .not. warn_only ) stop
    else if ( minval(tri%x_coil) < -100. ) then
      write(*,*) 'ERROR: at least one tri%x_coil value < -100'
      if ( .not. warn_only ) stop
    else if ( minval(tri%y_coil) < -100. ) then
      write(*,*) 'ERROR: at least one tri%y_coil value < -100'
      if ( .not. warn_only ) stop
    else if ( minval(tri%z_coil) < -100. ) then
      write(*,*) 'ERROR: at least one tri%z_coil value < -100'
      if ( .not. warn_only ) stop
    else if ( (size(tri%phi_coil,1) /= tri%ntri_c) .or. (size(tri%phi_coil,2) /= 3) ) then
      write(*,*) 'ERROR: tri%phi_coil has wrong size'
      if ( .not. warn_only ) stop
    else if ( minval(tri%phi_coil) < 0. ) then
      write(*,*) 'ERROR: at least one tri%phi_coil value < 0'
      if ( .not. warn_only ) stop
    else if ( (size(tri%eta_thin_coil,1) /= tri%ntri_c) ) then
      write(*,*) 'ERROR: tri%eta_thin_coil has wrong size'
      if ( .not. warn_only ) stop
    else if ( minval(tri%eta_thin_coil) < 0. ) then
      write(*,*) 'ERROR: at least one tri%eta_thin_coil value < 0'
      if ( .not. warn_only ) stop
    end if
    
  end subroutine check_triangles
  
  
  
  !> Read one coils set from a file and create the coil triangles for it
  subroutine read_coil_set(filename, coil_set, rank)
    
    ! --- Routine parameters
    character(len=*), intent(in)     :: filename
    type(t_coil_set), intent(inout)  :: coil_set
    integer,          intent(in)     :: rank
    
    ! --- Local variables
    character(len=256)        :: description
    integer                   :: i, ncoil, err
    type(t_coil), allocatable :: coil(:)
    
    ! --- Namelists
    namelist /coil_set_nml/ description, ncoil
    namelist /coils_nml   / coil
    
    if ( .not. was_initialized ) call init_coil_module(rank)
    
    call dealloc_coil_set(coil_set)
    
    if ( rank == 0 ) then ! Read only on MPI rank 0
      
      if ( COIL_DEBUG ) write(*,*)
      if ( COIL_DEBUG ) write(*,*) 'read_coil_set'
      write(*,*) '  filename        = ', trim(filename)
      write(*,*) '  name            = ', trim(coil_set%name)
      
      open(IOCH, file=trim(filename), status='old', action='read', iostat=err)
      if ( err /= 0 ) then
        write(*,*) '  ERROR opening file', trim(filename), '!'
        stop
      end if
      
      ! --- First namelist
      if ( COIL_DEBUG ) write(*,*) '  Read namelist coil_set_nml'
      read(IOCH, coil_set_nml)
      coil_set%description = description
      coil_set%ncoil       = ncoil
      
      ! --- Second namelist
      if ( COIL_DEBUG ) write(*,*) '  Read namelist coils_nml'
      allocate( coil_set%coil(coil_set%ncoil), coil(coil_set%ncoil) )
      read(IOCH, coils_nml)
      ! --- Calculate turns for AXISYM_FILA coils from n_fila_turns
      do i = 1, coil_set%ncoil
        if ( coil(i)%coil_type == AXISYM_FILA ) then
          coil(i)%nturns = sum( coil(i)%n_fila_turns(1:coil(i)%n_fila) )
        end if
      end do
      coil_set%coil(:) = coil(:)
      deallocate(coil)
      
      close(IOCH)
      
      call check_coil_set(coil_set, accept_empty=.false., warn_only=.false., check_index_start=.false., check_tria=.false.)
      
      call create_coil_triangles(coil_set)
      
      call check_coil_set(coil_set, accept_empty=.false., warn_only=.false., check_index_start=.false., check_tria=.true.)
      
      call print_coil_set(coil_set, COIL_DEBUG)
      
    end if
    
    if ( COIL_DEBUG ) write(*,*) 'Finished read_coil_set'
    
  end subroutine read_coil_set
  
  
  
  ! --- Create triangles for one coil set
  subroutine create_coil_triangles(coil_set)
    
    ! --- Routine parameters
    type(t_coil_set), intent(inout)  :: coil_set
    
    ! --- Local variables
    integer :: i, n_max_tria, n_tria, n_tria_i
    
    if ( COIL_DEBUG ) write(*,*)
    if ( COIL_DEBUG ) write(*,'(2a)') '  create_coil_triangles for ',trim(coil_set%name)
    
    ! --- Determine maximum number of triangles per coil and total number of triangles
    n_max_tria = 0
    n_tria     = 0
    do i = 1, coil_set%ncoil
      
      if ( coil_set%coil(i)%coil_type == AXISYM_THICK ) then
        n_tria_i = coil_set%coil(i)%nbands_R * coil_set%coil(i)%nbands_Z * coil_set%coil(i)%ntorpts * 2
      else if ( coil_set%coil(i)%coil_type == AXISYM_FILA ) then
        n_tria_i = coil_set%coil(i)%n_fila * coil_set%coil(i)%ntorpts * 2
      else if ( coil_set%coil(i)%coil_type == GENERAL_THIN ) then
        n_tria_i = coil_set%coil(i)%n_pts * 2
      end if
      n_tria     = n_tria + n_tria_i
      n_max_tria = max(n_max_tria, n_tria_i)
      
    end do
    
    ! --- Create triangles coil by coil
    do i = 1, coil_set%ncoil
      if (COIL_DEBUG) write(*,*)
      if (COIL_DEBUG) write(*,'(1x,a,i3.3,2a)') '    COIL ', i, ': ', trim(coil_set%coil(i)%name)
      
      if ( coil_set%coil(i)%coil_type == AXISYM_THICK ) then
        call create_axisym_thick_triangles(coil_set, i)
      else if ( coil_set%coil(i)%coil_type == AXISYM_FILA ) then
        call create_axisym_fila_triangles(coil_set, i)
      else if ( coil_set%coil(i)%coil_type == GENERAL_THIN ) then
        call create_general_thin_triangles(coil_set, i)
      else
        write(*,*) 'ERROR: Illegal coil_type for coil ', i, '!'
        stop
      end if
      
    end do
    
    if ( COIL_DEBUG ) write(*,*) '  Finished create_coil_triangles'
    
  end subroutine create_coil_triangles
  
  
  
  ! --- Deallocate triangles data structure
  subroutine dealloc_triangles(tri)
    
    ! --- Routine parameters
    type(t_triangles),     intent(inout)  :: tri
    
    tri%ntri_c  = 0
    if ( allocated(tri%jtri_c       ) ) deallocate( tri%jtri_c        )
    if ( allocated(tri%x_coil       ) ) deallocate( tri%x_coil        )
    if ( allocated(tri%y_coil       ) ) deallocate( tri%y_coil        )
    if ( allocated(tri%z_coil       ) ) deallocate( tri%z_coil        )
    if ( allocated(tri%phi_coil     ) ) deallocate( tri%phi_coil      )
    if ( allocated(tri%eta_thin_coil) ) deallocate( tri%eta_thin_coil )
    
  end subroutine dealloc_triangles
  
  
  
  ! --- Allocate triangles data structure
  subroutine alloc_triangles(tri, ntri_c, ncoil)
    
    ! --- Routine parameters
    type(t_triangles),     intent(inout)  :: tri
    integer,               intent(in)     :: ntri_c
    integer,               intent(in)     :: ncoil
    
    call dealloc_triangles(tri)
    tri%ntri_c = ntri_c
    allocate( tri%jtri_c       (ncoil   ) )
    allocate( tri%x_coil       (ntri_c,3) )
    allocate( tri%y_coil       (ntri_c,3) )
    allocate( tri%z_coil       (ntri_c,3) )
    allocate( tri%phi_coil     (ntri_c,3) )
    allocate( tri%eta_thin_coil(ntri_c  ) )
    
  end subroutine alloc_triangles
  
  
  
  ! --- Create triangles for one axisym_thick coil
  subroutine create_triangles_for_bands(coil_set, i_coil, bands, nbands, tri, eta_thin_band)
    
    ! --- Routine parameters
    type(t_coil_set),      intent(inout)  :: coil_set
    integer,               intent(in)     :: i_coil
    type(t_triangle_band), intent(in)     :: bands(nbands)
    integer,               intent(in)     :: nbands
    type(t_triangles),     intent(inout)  :: tri
    real,                  intent(in)     :: eta_thin_band(nbands)
    
    ! --- Local variables
    integer :: i, j, l_tria
    
    call dealloc_triangles(tri)
    do i = 1, nbands
      tri%ntri_c = tri%ntri_c + 2 * (bands(i)%len-1)
    end do
    allocate( tri%jtri_c(1), tri%x_coil(tri%ntri_c,3), tri%y_coil(tri%ntri_c,3), tri%z_coil(tri%ntri_c,3), tri%phi_coil(tri%ntri_c,3), tri%eta_thin_coil(tri%ntri_c) )
    tri%jtri_c(1) = tri%ntri_c
    
    l_tria = 0
    do i = 1, nbands
      do j = 1, bands(i)%len-1
        
        ! --- First triangle for present position in band
        l_tria = l_tria + 1
        tri%x_coil(l_tria, 1) = bands(i)%x(j,  2)
        tri%y_coil(l_tria, 1) = bands(i)%y(j,  2)
        tri%z_coil(l_tria, 1) = bands(i)%z(j,  2)
        tri%x_coil(l_tria, 2) = bands(i)%x(j+1,2)
        tri%y_coil(l_tria, 2) = bands(i)%y(j+1,2)
        tri%z_coil(l_tria, 2) = bands(i)%z(j+1,2)
        tri%x_coil(l_tria, 3) = bands(i)%x(j,  1)
        tri%y_coil(l_tria, 3) = bands(i)%y(j,  1)
        tri%z_coil(l_tria, 3) = bands(i)%z(j,  1)
        tri%phi_coil(l_tria, 1) = bands(i)%phi(j,  2)
        tri%phi_coil(l_tria, 2) = bands(i)%phi(j+1,2)
        tri%phi_coil(l_tria, 3) = bands(i)%phi(j,  1)
        tri%eta_thin_coil(l_tria) = eta_thin_band(i)
        
        ! --- Second triangle for present position in band
        l_tria = l_tria + 1
        tri%x_coil(l_tria, 1) = bands(i)%x(j+1,1)
        tri%y_coil(l_tria, 1) = bands(i)%y(j+1,1)
        tri%z_coil(l_tria, 1) = bands(i)%z(j+1,1)
        tri%x_coil(l_tria, 2) = bands(i)%x(j,  1)
        tri%y_coil(l_tria, 2) = bands(i)%y(j,  1)
        tri%z_coil(l_tria, 2) = bands(i)%z(j,  1)
        tri%x_coil(l_tria, 3) = bands(i)%x(j+1,2)
        tri%y_coil(l_tria, 3) = bands(i)%y(j+1,2)
        tri%z_coil(l_tria, 3) = bands(i)%z(j+1,2)
        tri%phi_coil(l_tria, 1) = bands(i)%phi(j+1,1)
        tri%phi_coil(l_tria, 2) = bands(i)%phi(j,  1)
        tri%phi_coil(l_tria, 3) = bands(i)%phi(j+1,2)
        tri%eta_thin_coil(l_tria) = eta_thin_band(i)
      end do
    end do
    
  end subroutine create_triangles_for_bands
  
  
  
  ! --- Create triangles for one axisym_thick coil
  subroutine create_axisym_thick_triangles(coil_set, i_coil)
    
    ! --- Routine parameters
    type(t_coil_set),  intent(inout)  :: coil_set
    integer,           intent(in)     :: i_coil
    
    ! --- Local variables
    type(t_coil)                       :: coil
    type(t_triangle_band), allocatable, target :: bands(:)
    type(t_triangle_band), pointer     :: band
    type(t_triangles)                  :: tri
    integer                            :: j, jR, jZ, k, nbands, l
    real, allocatable                  :: eta_thin_band(:)
    real                               :: phi, R, Z1, Z2
    
    if ( COIL_DEBUG ) write(*,*) '      create_axisym_thick_triangles'
    
    ! --- Construct geometry of the triangle bands
    coil     = coil_set%coil(i_coil)
    nbands   = coil%nbands_R * coil%nbands_Z
    allocate( bands(nbands), eta_thin_band(nbands) )
    do jR = 1, coil%nbands_R
      R   = coil%R-coil%dR/2.*(1.-1./real(coil%nbands_R))+real(jR-1)*coil%dR/real(coil%nbands_R)
      do jZ = 1, coil%nbands_Z
        j    = (jR-1)*coil%nbands_Z + jZ
        band => bands(j)
        eta_thin_band(j) = coil%resist * coil%nbands_R * coil%dZ / ( pi2 * R )
        band%len = coil%ntorpts
        Z1  = coil%Z+coil%dZ/2.-real(jZ-1)*coil%dZ/real(coil%nbands_Z)
        Z2  = coil%Z+coil%dZ/2.-real(jZ  )*coil%dZ/real(coil%nbands_Z)
        allocate( band%x(band%len,2), band%y(band%len,2), band%z(band%len,2), band%phi(band%len,2) )
        do l = 1, coil%ntorpts
          phi         = real(l-1) * PI2 / real(band%len-1)
          band%x(l,:) = R * cos(phi)
          band%y(l,:) = R * sin(phi)
          band%z(l,:) = (/Z1, Z2/)
          band%phi(l,1) = 0.
          band%phi(l,2) = 1. / real(nbands) * coil_set%coil(i_coil)%nturns
        end do
      end do
    end do
    
    ! --- Create triangles
    call create_triangles_for_bands(coil_set, i_coil, bands, nbands, tri, eta_thin_band)
    call merge_triangles(coil_set%tri, tri)
    
    coil_set%coil(i_coil) = coil
    
    deallocate(eta_thin_band)
    
    if ( COIL_DEBUG ) write(*,*) '      Finished create_axisym_thick_triangles'
    
  end subroutine create_axisym_thick_triangles
  
  
  
  ! --- Create triangles for one axisym_fila coil
  subroutine create_axisym_fila_triangles(coil_set, i_coil)
    
    ! --- Routine parameters
    type(t_coil_set), intent(inout)  :: coil_set
    integer,          intent(in)     :: i_coil
    
    ! --- Local variables
    type(t_coil)                       :: coil
    type(t_triangle_band), allocatable, target :: bands(:)
    type(t_triangle_band), pointer     :: band
    type(t_triangles)                  :: tri
    integer                            :: j, k, nbands, l
    real, allocatable                  :: eta_thin_band(:)
    real                               :: phi, R, Z1, Z2
    
    if ( COIL_DEBUG ) write(*,*) '      create_axisym_fila_triangles'
    
    ! --- Construct geometry of the triangle bands
    coil     = coil_set%coil(i_coil)
    nbands   = coil%n_fila
    allocate( bands(nbands), eta_thin_band(nbands) )
    do j = 1, nbands
      band => bands(j)
      band%len = coil%ntorpts
      R   = coil%R_fila(j)
      Z1  = coil%Z_fila(j)
      Z2  = coil%Z_fila(j) + FILA_DZ
      allocate( band%x(band%len,2), band%y(band%len,2), band%z(band%len,2), band%phi(band%len,2) )
      do l = 1, coil%ntorpts
        phi         = real(l-1) * PI2 / real(band%len-1)
        band%x(l,:) = R * cos(phi)
        band%y(l,:) = R * sin(phi)
        band%z(l,:) = (/Z1, Z2/)
        band%phi(l,1) = 0.
        band%phi(l,2) = coil%n_fila_turns(j)
      end do
      eta_thin_band(j) = coil%resist * sum(coil%n_fila_turns(:)) * coil%width / ( pi2 * R * coil%n_fila_turns(j) )
    end do
    
    ! --- Create triangles
    call create_triangles_for_bands(coil_set, i_coil, bands, nbands, tri, eta_thin_band)
    call merge_triangles(coil_set%tri, tri)
    
    coil_set%coil(i_coil) = coil
    
    deallocate(eta_thin_band)
    
    if ( COIL_DEBUG ) write(*,*) '      Finished create_axisym_fila_triangles'
    
  end subroutine create_axisym_fila_triangles
  
  
  
  ! --- Create triangles for one general_thin coil
  subroutine create_general_thin_triangles(coil_set, i_coil)
    
    ! --- Routine parameters
    type(t_coil_set), intent(inout)  :: coil_set
    integer,          intent(in)     :: i_coil
    
    ! --- Local variables
    type(t_triangles)                  :: tri
    type(t_coil)                       :: coil
    type(t_triangle_band), allocatable, target :: bands(:)
    type(t_triangle_band), pointer     :: band
    real                               :: center(3), pos(3), buf(3), r1(3), d1(3), len
    real, allocatable                  :: eta_thin_band(:)
    integer                            :: npts, i
    if ( COIL_DEBUG ) write(*,*) '      create_general_thin_triangles'
    
    ! --- Construct geometry of the triangle bands
    coil     = coil_set%coil(i_coil)
    npts     = coil%n_pts
    allocate( bands(1), eta_thin_band(1) )
    band     => bands(1)
    allocate( band%x(npts,2), band%y(npts,2), band%z(npts,2),  band%phi(npts,2) )
    band%len = npts
    
    ! --- Determine "length" of the coil
    len = 0.
    do i = 2, npts
      len = len + sqrt( (coil%xpts(i)-coil%xpts(i-1))**2 + (coil%ypts(i)-coil%ypts(i-1))**2 + (coil%zpts(i)-coil%zpts(i-1))**2 )
    end do
    
    ! --- Determine geometrical center of the coil
    center = (/ sum(coil%xpts(1:npts-1))/(npts-1),  sum(coil%ypts(1:npts-1))/(npts-1),  sum(coil%zpts(1:npts-1))/(npts-1) /)
    
    do i = 1, npts-1
      pos =  (/ coil%xpts(i),  coil%ypts(i),  coil%zpts(i) /) 
      r1  =  pos - center
      d1  =  (/ coil%xpts(modulo(i-2,npts-1)+1), coil%ypts(modulo(i-2,npts-1)+1), coil%zpts(modulo(i-2,npts-1)+1) /) &
           - (/ coil%xpts(modulo(i,npts-1)+1),   coil%ypts(modulo(i,npts-1)+1),   coil%zpts(modulo(i,npts-1)+1)   /)
      buf =  coil%width * my_cross( my_cross(r1,d1 ), d1 ) / my_norm( my_cross( my_cross(r1,d1 ), d1 ) )
      
      !### make sure buf always points away from center
      
      band%x(i,1)   = pos(1) - buf(1)
      band%y(i,1)   = pos(2) - buf(2)
      band%z(i,1)   = pos(3) - buf(3)
      band%x(i,2)   = pos(1) + buf(1)
      band%y(i,2)   = pos(2) + buf(2)
      band%z(i,2)   = pos(3) + buf(3)
      band%phi(i,1) = 0.
      band%phi(i,2) = real(coil%nturns)
    end do
    
    ! ---The last point in the band has to be equal to the first one
    band%x(npts,:)   = band%x(1,:)
    band%y(npts,:)   = band%y(1,:)
    band%z(npts,:)   = band%z(1,:)
    band%phi(npts,:) = band%phi(1,:)
    
    eta_thin_band(1) = coil%resist * coil%width / len
    
    ! --- Create triangles
    call create_triangles_for_bands(coil_set, i_coil, bands, 1, tri, eta_thin_band)
    call merge_triangles(coil_set%tri, tri)
    
    coil_set%coil(i_coil) = coil
    
    deallocate(eta_thin_band)
    
    if ( COIL_DEBUG ) write(*,*) '      Finished create_general_thin_triangles'
    
  end subroutine create_general_thin_triangles
  
  
  
  ! --- Merge triangles into another triangle data structure
  subroutine merge_triangles(tri_target, tri_source)
    
    ! --- Routine parameters
    type(t_triangles), intent(inout) :: tri_target
    type(t_triangles), intent(in)    :: tri_source
    
    ! --- Local variables
    type(t_triangles) :: tri
    integer           :: nc_target, nc_source, nc_all
    
    if ( tri_target%ntri_c <= 0 ) then
      
      nc_source         = size(tri_source%jtri_c,1)
      tri_target%ntri_c = tri_source%ntri_c
      allocate( tri_target%jtri_c(nc_source), tri_target%x_coil(tri_target%ntri_c,3), tri_target%y_coil(tri_target%ntri_c,3), tri_target%z_coil(tri_target%ntri_c,3), tri_target%phi_coil(tri_target%ntri_c,3), tri_target%eta_thin_coil(tri_target%ntri_c) )
      tri_target%jtri_c        = tri_source%jtri_c
      tri_target%x_coil        = tri_source%x_coil
      tri_target%y_coil        = tri_source%y_coil
      tri_target%z_coil        = tri_source%z_coil
      tri_target%phi_coil      = tri_source%phi_coil
      tri_target%eta_thin_coil = tri_source%eta_thin_coil
    else
      
      nc_target = size(tri_target%jtri_c,1)
      nc_source = size(tri_source%jtri_c,1)
      nc_all    = nc_target + nc_source
      
      ! --- "Backup" tri_target to tri
      tri%ntri_c = tri_target%ntri_c
      allocate( tri%jtri_c(nc_target), tri%x_coil(tri%ntri_c,3), tri%y_coil(tri%ntri_c,3), tri%z_coil(tri%ntri_c,3), tri%phi_coil(tri%ntri_c,3), tri%eta_thin_coil(tri%ntri_c) )
      tri%jtri_c        = tri_target%jtri_c
      tri%x_coil        = tri_target%x_coil
      tri%y_coil        = tri_target%y_coil
      tri%z_coil        = tri_target%z_coil
      tri%phi_coil      = tri_target%phi_coil
      tri%eta_thin_coil = tri_target%eta_thin_coil
          
      ! --- Re-allocate tri_target
      call dealloc_triangles(tri_target)
      tri_target%ntri_c = tri%ntri_c + tri_source%ntri_c
      allocate( tri_target%jtri_c(nc_all), tri_target%x_coil(tri_target%ntri_c,3), tri_target%y_coil(tri_target%ntri_c,3), tri_target%z_coil(tri_target%ntri_c,3), tri_target%phi_coil(tri_target%ntri_c,3), tri_target%eta_thin_coil(tri_target%ntri_c) )
      
      ! --- Merge
      tri_target%jtri_c(1:nc_target)                           = tri%jtri_c(1:nc_target)
      tri_target%jtri_c(nc_target+1:nc_all)                    = tri_source%jtri_c(1:nc_source)
      tri_target%x_coil(1:tri%ntri_c,:)                        = tri%x_coil(1:tri%ntri_c,:)
      tri_target%x_coil(tri%ntri_c+1:tri_target%ntri_c,:)      = tri_source%x_coil(1:tri_source%ntri_c,:)
      tri_target%y_coil(1:tri%ntri_c,:)                        = tri%y_coil(1:tri%ntri_c,:)
      tri_target%y_coil(tri%ntri_c+1:tri_target%ntri_c,:)      = tri_source%y_coil(1:tri_source%ntri_c,:)
      tri_target%z_coil(1:tri%ntri_c,:)                        = tri%z_coil(1:tri%ntri_c,:)
      tri_target%z_coil(tri%ntri_c+1:tri_target%ntri_c,:)      = tri_source%z_coil(1:tri_source%ntri_c,:)
      tri_target%phi_coil(1:tri%ntri_c,:)                      = tri%phi_coil(1:tri%ntri_c,:)
      tri_target%phi_coil(tri%ntri_c+1:tri_target%ntri_c,:)    = tri_source%phi_coil(1:tri_source%ntri_c,:)
      tri_target%eta_thin_coil(1:tri%ntri_c)                   = tri%eta_thin_coil(1:tri%ntri_c)
      tri_target%eta_thin_coil(tri%ntri_c+1:tri_target%ntri_c) = tri_source%eta_thin_coil(1:tri_source%ntri_c)
    
    
      
    end if
    
    call dealloc_triangles(tri)
    
  end subroutine merge_triangles
  
  
  
  !> Merge one coil set into another one
  !! Note: target%name, target%description, and target%index_start remain intentionally untouched!
  subroutine merge_coil_sets(target, source)
    
    ! --- Routine parameters
    type(t_coil_set), intent(inout)  :: target !< Merge into this coil set
    type(t_coil_set), intent(inout)  :: source !< Merge this coil set
    
    ! --- Local variables
    integer :: i, ncoil_source, ncoil_target, ncoil_all
    type(t_coil), allocatable :: tmp_coil(:)
    type(t_triangles) :: tri
    
    if ( COIL_DEBUG ) write(*,*)
    if ( COIL_DEBUG ) write(*,*) '  merge_coil_sets: ', trim(source%name), ' into ', trim(target%name)
    
    if ( source%ncoil <= 0 ) then
      if ( COIL_DEBUG ) write(*,*) '    merge_coil_sets is doing nothing since coil set ', trim(source%name), ' is empty.'
      return
    end if
    
    ncoil_source = source%ncoil
    ncoil_target = max(0, target%ncoil)
    ncoil_all    = ncoil_source + ncoil_target
    
    if ( target%ncoil <= 0 ) then
      call dealloc_coil_set(target)
      target%ncoil = source%ncoil
      allocate(target%coil(target%ncoil))
      target%coil(:) = source%coil(:)
    else
      ! --- "Backup" information of target
      allocate(tmp_coil(ncoil_target))
      tmp_coil(:) = target%coil(:)
      call merge_triangles(tri, target%tri)
      ! --- Re-allocate target and fill in
      call dealloc_coil_set(target)
      call merge_triangles(target%tri, tri)
      allocate(target%coil(ncoil_all))
      target%coil(1:ncoil_target)           = tmp_coil(1:ncoil_target)
      target%coil(ncoil_target+1:ncoil_all) = source%coil(1:ncoil_source)
      target%ncoil = ncoil_all
      deallocate(tmp_coil)
    end if
    
    call merge_triangles(target%tri, source%tri)
    
    if ( target%name == 'all_coil_set' ) source%index_start = ncoil_target + 1
    
    if ( COIL_DEBUG ) write(*,*) '  Finished merge_coil_sets'
    
  end subroutine merge_coil_sets
  
  
  
  ! --- Fill all_coil_set and similar data structures
  subroutine collect_all_coil_info(rank)
    
    ! --- Routine parameters
    integer, intent(in) :: rank
    
    if ( .not. was_initialized ) then
      write(*,*) 'ERROR: Module coils was not initialized before calling collect_all_coil_info.'
      write(*,*) 'That probably means you have never called read_coil_set.'
      stop
    end if
    
    if ( COIL_DEBUG ) write(*,*)
    if ( COIL_DEBUG ) write(*,*) 'collect_all_coil_info', rank
    
    call broadcast_coil_sets(rank)
    
    call dealloc_coil_set(all_coil_set)
    
    call check_coil_set(pol_coil_set,     accept_empty=.true., warn_only=.false., check_index_start=.false., check_tria=.true.)
    call check_coil_set(rmp_coil_set,     accept_empty=.true., warn_only=.false., check_index_start=.false., check_tria=.true.)
    call check_coil_set(voltage_coil_set, accept_empty=.true., warn_only=.false., check_index_start=.false., check_tria=.true.)
    call check_coil_set(diag_coil_set,    accept_empty=.true., warn_only=.false., check_index_start=.false., check_tria=.true.)
    call check_coil_set(all_coil_set,     accept_empty=.true., warn_only=.false., check_index_start=.false., check_tria=.true.)
    
    call merge_coil_sets(all_coil_set, pol_coil_set)
    call merge_coil_sets(all_coil_set, rmp_coil_set)
    call merge_coil_sets(all_coil_set, voltage_coil_set)
    call merge_coil_sets(all_coil_set, diag_coil_set)
    
    if ( COIL_DEBUG .and. (rank==0) ) then
      call print_coil_set(pol_coil_set,     COIL_DEBUG)
      call print_coil_set(rmp_coil_set,     COIL_DEBUG)
      call print_coil_set(voltage_coil_set, COIL_DEBUG)
      call print_coil_set(diag_coil_set,    COIL_DEBUG)
      call print_coil_set(all_coil_set,     COIL_DEBUG)
    end if
    
    call check_coil_set(pol_coil_set,     accept_empty=.true., warn_only=.false., check_index_start=.true., check_tria=.true.)
    call check_coil_set(rmp_coil_set,     accept_empty=.true., warn_only=.false., check_index_start=.true., check_tria=.true.)
    call check_coil_set(voltage_coil_set, accept_empty=.true., warn_only=.false., check_index_start=.true., check_tria=.true.)
    call check_coil_set(diag_coil_set,    accept_empty=.true., warn_only=.false., check_index_start=.true., check_tria=.true.)
    call check_coil_set(all_coil_set,     accept_empty=.true., warn_only=.false., check_index_start=.true., check_tria=.true.)
    
    ! --- Fill in the information of module coil_tria for use in other parts of STARWALL
    call extract_info_for_coil_tria_module()
  
    if ( COIL_DEBUG ) write(*,*) 'Finished collect_all_coil_info', rank
    
  end subroutine collect_all_coil_info
  
  
  
  !> Extract information for the module coil_tria that is used in most parts of STARWALL.
  subroutine extract_info_for_coil_tria_module()
    
    use coil_tria
    integer      :: i
    call check_coil_set(all_coil_set, accept_empty=.false., warn_only=.false., check_index_start=.false., check_tria=.true.)
    
    ! --- Clean up first
    ncoil  = 0
    ntri_c = 0
    if ( allocated(jtri_c) ) deallocate(jtri_c)
    if ( allocated(x_coil) ) deallocate(x_coil)
    if ( allocated(y_coil) ) deallocate(y_coil)
    if ( allocated(z_coil) ) deallocate(z_coil)
    if ( allocated(phi_coil) ) deallocate(phi_coil)
    if ( allocated(eta_thin_coil) ) deallocate(eta_thin_coil)
    if ( allocated(coil_resist) )   deallocate(coil_resist)
    ind_start_pol_coils     = -999
    ind_start_rmp_coils     = -999
    ind_start_voltage_coils = -999
    ind_start_diag_coils    = -999
    
    ! --- Then fill in the information
    ncoil         = all_coil_set%ncoil
    ntri_c        = all_coil_set%tri%ntri_c
    allocate( jtri_c(ncoil), x_coil(ntri_c,3), y_coil(ntri_c,3), z_coil(ntri_c,3), phi_coil(ntri_c,3), eta_thin_coil(ntri_c), coil_resist(ncoil) )
    jtri_c        = all_coil_set%tri%jtri_c
    x_coil        = all_coil_set%tri%x_coil
    y_coil        = all_coil_set%tri%y_coil
    z_coil        = all_coil_set%tri%z_coil
    phi_coil      = all_coil_set%tri%phi_coil
    eta_thin_coil = all_coil_set%tri%eta_thin_coil
    n_pol_coils             = pol_coil_set%ncoil
    n_rmp_coils             = rmp_coil_set%ncoil
    n_voltage_coils         = voltage_coil_set%ncoil
    n_diag_coils            = diag_coil_set%ncoil
    ind_start_pol_coils     = pol_coil_set%index_start
    ind_start_rmp_coils     = rmp_coil_set%index_start
    ind_start_voltage_coils = voltage_coil_set%index_start
    ind_start_diag_coils    = diag_coil_set%index_start
    do i = 1, ncoil
      coil_resist(i)        = all_coil_set%coil(i)%resist
    end do
  end subroutine extract_info_for_coil_tria_module
  
  
  
  ! --- Create a vtk file for triangles
  subroutine triangles2vtk(tri, filename)
    140 format(a) 
    141 format(a,i8,a)
    142 format(3es24.16)
    143 format(a,2i8)
    144 format(4i8)

    ! --- Routine parameters
    type(t_triangles), intent(inout)  :: tri
    character(len=*),  intent(in)     :: filename
    
    ! --- local variabels
    integer          :: nbtri, i
    nbtri  = tri%ntri_c

    if ( COIL_DEBUG ) write(*,*)
    if ( COIL_DEBUG ) write(*,*) 'triangles2vtk'
    
    open(60, file=filename)
    write(60,140) '# vtk DataFile Version 2.0'
    write(60,140) 'testdata'
    write(60,140) 'ASCII'
    write(60,140) 'DATASET POLYDATA'
    write(60,141) 'POINTS', 3*nbtri, ' float'
    do i = 1, nbtri
      write(60,142) tri%x_coil(i,1), tri%y_coil(i,1), tri%z_coil(i,1)
      write(60,142) tri%x_coil(i,2), tri%y_coil(i,2), tri%z_coil(i,2)
      write(60,142) tri%x_coil(i,3), tri%y_coil(i,3), tri%z_coil(i,3)
    end do
    write(60,143) 'POLYGONS', nbtri, nbtri*4
    do i = 1, nbtri
      write(60,144) 3, 3 * i - (/ 3, 2, 1 /)
    end do
    write(60,141) 'POINT_DATA', 3*nbtri
    write(60,140) 'SCALARS potentials float'
    write(60,140) 'LOOKUP_TABLE default'
    do i = 1, nbtri
      write(60,142) tri%phi_coil(i,1)
      write(60,142) tri%phi_coil(i,2)
      write(60,142) tri%phi_coil(i,3)
    end do
    write(60,141) 'POINT_DATA', 3*nbtri
    write(60,140) 'SCALARS eta_thin_coil float'
    write(60,140) 'LOOKUP_TABLE default'
    do i = 1, nbtri
      write(60,142) tri%eta_thin_coil(i)
      write(60,142) tri%eta_thin_coil(i)
      write(60,142) tri%eta_thin_coil(i)
    end do
    
    close(60)

    if ( COIL_DEBUG ) write(*,*) 'Finished triangles2vtk'
    
  end subroutine triangles2vtk
  
  
  
  ! --- Write ascii file for triangles
  subroutine triangles2ascii(tri, filename)
    
    ! --- Routine parameters
    type(t_triangles), intent(inout)  :: tri
    character(len=*),  intent(in)     :: filename
    
    ! --- Local variables
    integer :: i, err
    
    if ( COIL_DEBUG ) write(*,*)
    if ( COIL_DEBUG ) write(*,*) 'triangles2ascii'
    
    open(IOCH, file=trim(filename), status='replace', action='write', iostat=err)
    if ( err /= 0 ) then
      write(*,*) '  ERROR creating file', trim(filename), '!'
      stop
    end if
    
    do i = 1, tri%ntri_c
      write(IOCH,*) tri%x_coil(i,1), tri%y_coil(i,1), tri%z_coil(i,1)
      write(IOCH,*) tri%x_coil(i,2), tri%y_coil(i,2), tri%z_coil(i,2)
      write(IOCH,*) tri%x_coil(i,3), tri%y_coil(i,3), tri%z_coil(i,3)
      write(IOCH,*) tri%x_coil(i,1), tri%y_coil(i,1), tri%z_coil(i,1)
      write(IOCH,*)
      write(IOCH,*)
    end do
    
    close(IOCH)
    
    if ( COIL_DEBUG ) write(*,*) 'Finished triangles2ascii'
    
  end subroutine triangles2ascii
  
  
  
  ! --- Export all coil sets
  subroutine export_coil_triangles(rank, vtk, ascii)
    
    ! --- Routine parameters
    integer, intent(in) :: rank
    logical, intent(in) :: vtk, ascii
    
    if ( .not. was_initialized ) then
      write(*,*) 'ERROR: Module coils was not initialized before calling collect_all_coil_info.'
      write(*,*) 'That probably means you have never called read_coil_set.'
      stop
    end if
    
    if ( rank /= 0 ) return
    
    if ( vtk ) then
     if ( all_coil_set%tri%ntri_c > 0 )                                                            &
       call triangles2vtk  (all_coil_set%tri,     'all_coil_triangles.vtk')
     if ( pol_coil_set%tri%ntri_c > 0 )                                                            &
       call triangles2vtk  (pol_coil_set%tri,     'pol_coil_triangles.vtk')
     if ( rmp_coil_set%tri%ntri_c > 0 )                                                            &
       call triangles2vtk  (rmp_coil_set%tri,     'rmp_coil_triangles.vtk')
     if ( voltage_coil_set%tri%ntri_c > 0 )                                                        &
       call triangles2vtk  (voltage_coil_set%tri, 'voltage_coil_triangles.vtk')
     if ( diag_coil_set%tri%ntri_c > 0 )                                                           &
       call triangles2vtk  (diag_coil_set%tri,    'diag_coil_triangles.vtk')
    end if
    
    if ( ascii ) then
     if ( all_coil_set%tri%ntri_c > 0 )                                                            &
       call triangles2ascii(all_coil_set%tri,     'all_coil_triangles.ascii')
     if ( pol_coil_set%tri%ntri_c > 0 )                                                            &
       call triangles2ascii(pol_coil_set%tri,     'pol_coil_triangles.ascii')
     if ( rmp_coil_set%tri%ntri_c > 0 )                                                            &
       call triangles2ascii(rmp_coil_set%tri,     'rmp_coil_triangles.ascii')
     if ( voltage_coil_set%tri%ntri_c > 0 )                                                        &
       call triangles2ascii(voltage_coil_set%tri, 'voltage_coil_triangles.ascii')
     if ( diag_coil_set%tri%ntri_c > 0 )                                                           &
       call triangles2ascii(diag_coil_set%tri,    'diag_coil_triangles.ascii')
    end if
    
  end subroutine export_coil_triangles
  
  
  
  ! --- Broadcast all coil sets from rank 0 to the other MPI ranks
  subroutine broadcast_coil_sets(rank)
    
    use mpi
    
    ! --- Routine parameters
    integer, intent(in) :: rank
    
    ! --- Local variables
    integer :: ierr
    
    if ( COIL_DEBUG ) write(*,*)
    if ( COIL_DEBUG ) write(*,*) 'broadcast_coil_sets rank=', rank
    
    call broadcast_coil_set(all_coil_set,     rank)
    call broadcast_coil_set(pol_coil_set,     rank)
    call broadcast_coil_set(rmp_coil_set,     rank)
    call broadcast_coil_set(voltage_coil_set, rank)
    call broadcast_coil_set(diag_coil_set,    rank)
    
    ! --- Make sure the coil sets on all MPI ranks are consistent
    call verify_checksum(all_coil_set,     rank)
    call verify_checksum(pol_coil_set,     rank)
    call verify_checksum(rmp_coil_set,     rank)
    call verify_checksum(voltage_coil_set, rank)
    call verify_checksum(diag_coil_set,    rank)
    
    if ( COIL_DEBUG ) write(*,*) 'Finished broadcast_coil_sets'
    
  end subroutine broadcast_coil_sets
  
  
  
  ! --- Broadcast one coil sets from rank 0 to the other MPI ranks
  subroutine broadcast_coil_set(coil_set, rank)
    
    use mpi
    
    ! --- Routine parameters
    type (t_coil_set), intent(inout) :: coil_set
    integer,           intent(in)    :: rank
    
    ! --- Local variables
    integer :: ierr
    integer :: ntri_c
    
    if ( COIL_DEBUG ) write(*,*)
    if ( COIL_DEBUG ) write(*,*) '  broadcast_coil_set ', trim(coil_set%name), ' rank=', rank
    
    call MPI_bcast(coil_set%name,         32, MPI_CHARACTER, 0, MPI_COMM_WORLD, ierr)
    call MPI_bcast(coil_set%description, 256, MPI_CHARACTER, 0, MPI_COMM_WORLD, ierr)
    call MPI_bcast(coil_set%ncoil,         1, MPI_INTEGER,   0, MPI_COMM_WORLD, ierr)
    call MPI_bcast(coil_set%index_start,   1, MPI_INTEGER,   0, MPI_COMM_WORLD, ierr)
    ntri_c = coil_set%tri%ntri_c
    call MPI_bcast(ntri_c,                 1, MPI_INTEGER,   0, MPI_COMM_WORLD, ierr)
    
    ! --- Broadcast coil data structure
    if ( coil_set%ncoil <= 0 ) then
      if ( COIL_DEBUG ) write(*,*) '    Skipping coil_set ', trim(coil_set%name), ' since ncoil<=0)', rank
      return
    end if
    if ( rank /= 0 ) then
      if ( allocated(coil_set%coil) ) deallocate( coil_set%coil )
      allocate( coil_set%coil(coil_set%ncoil) )
    end if
    call broadcast_coils(coil_set%coil, coil_set%ncoil, rank)
    
    ! --- Broadcast triangle data structure
    if ( ntri_c <= 0 ) then
      write(*,*) '    Skipping triangles since ntri_c<=0'
      return
    end if
    if ( rank /= 0 ) then
      call alloc_triangles(coil_set%tri, ntri_c, coil_set%ncoil)
    end if
    call broadcast_triangles(coil_set%tri, coil_set%ncoil, rank)
    
    if ( COIL_DEBUG ) write(*,*) '  finished broadcast_coil_set ', trim(coil_set%name), ' rank=', rank
    
  end subroutine broadcast_coil_set
  
  
  
  ! --- Broadcast an array of coils from rank 0 to the other MPI ranks
  subroutine broadcast_coils(coil, ncoil, rank)
    
    use mpi
    
    ! --- Routine parameters
    type (t_coil), intent(inout) :: coil(ncoil)
    integer,       intent(inout) :: ncoil
    integer,       intent(in)    :: rank
    
    ! --- Local variable
    integer :: i, ierr
    
    if ( COIL_DEBUG ) write(*,*)
    if ( COIL_DEBUG ) write(*,*) '    broadcast_coils'
    
    do i = 1, ncoil
      
      call MPI_bcast(coil(i)%name,                128, MPI_CHARACTER,        0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%coil_type,            32, MPI_CHARACTER,        0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%resist,                1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%nturns,                1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%ntorpts,               1, MPI_INTEGER,          0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%R,                     1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%Z,                     1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%dR,                    1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%dZ,                    1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%nbands_R,              1, MPI_INTEGER,          0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%nbands_Z,              1, MPI_INTEGER,          0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%n_fila,                1, MPI_INTEGER,          0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%n_fila_turns, N_MAX_FILA, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%R_fila,       N_MAX_FILA, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%Z_fila,       N_MAX_FILA, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%n_pts,                 1, MPI_INTEGER,          0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%width,                 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%xpts,          N_MAX_PTS, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%ypts,          N_MAX_PTS, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      call MPI_bcast(coil(i)%zpts,          N_MAX_PTS, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
      
    end do
    
    if ( COIL_DEBUG ) write(*,*) '    Finished broadcast_coils'
    
  end subroutine broadcast_coils
  
  
  
  ! --- Broadcast an array of coils from rank 0 to the other MPI ranks
  subroutine broadcast_triangles(tri, ncoil, rank)
    
    use mpi
    
    ! --- Routine parameters
    type (t_triangles), intent(inout) :: tri
    integer,            intent(in)    :: ncoil
    integer,            intent(in)    :: rank
    
    ! --- Local variables
    integer :: ierr
    
    if ( COIL_DEBUG ) write(*,*)
    if ( COIL_DEBUG ) write(*,*) '    broadcast_triangles'
    
    if ( rank==0 ) then
      if ( size(tri%jtri_c,1) /= ncoil ) then
        write(*,*) 'ERROR: size(jtri_c,1) /= ncoil'
        stop
      end if
    end if
    
    call MPI_bcast(tri%ntri_c,                   1, MPI_INTEGER,          0, MPI_COMM_WORLD, ierr)
    call MPI_bcast(tri%jtri_c,               ncoil, MPI_INTEGER,          0, MPI_COMM_WORLD, ierr)
    call MPI_bcast(tri%x_coil,        3*tri%ntri_c, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    call MPI_bcast(tri%y_coil,        3*tri%ntri_c, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    call MPI_bcast(tri%z_coil,        3*tri%ntri_c, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    call MPI_bcast(tri%phi_coil,      3*tri%ntri_c, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    call MPI_bcast(tri%eta_thin_coil,   tri%ntri_c, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    
    if ( COIL_DEBUG ) write(*,*) '    Finished broadcast_triangles'
    
  end subroutine broadcast_triangles
  
  
  
  ! --- Calculate a checksum for a coil set (useful for testing correct MPI broadcast)
  real function checksum_coil_set(coil_set)
    
    ! --- Routine parameters
    type (t_coil_set),  intent(in) :: coil_set
    
    checksum_coil_set = coil_set%ncoil + coil_set%index_start + checksum_triangles(coil_set%tri)
    
    if ( coil_set%ncoil > 0 )                                                                &
      checksum_coil_set = checksum_coil_set + checksum_coils(coil_set%coil, coil_set%ncoil)
    
  end function checksum_coil_set
  
  
  
  ! --- Calculate a checksum for a coil (useful for testing correct MPI broadcast)
  real function checksum_coils(coil, ncoil)
    
    ! --- Routine parameters
    type (t_coil),     intent(in) :: coil(ncoil)
    integer,           intent(in) :: ncoil
    
    ! --- Local variables
    integer :: i
    
    checksum_coils = 0.
    do i = 1, ncoil
      checksum_coils = checksum_coils + coil(i)%resist + coil(i)%nturns + coil(i)%ntorpts          &
        + coil(i)%R + coil(i)%Z + coil(i)%dR + coil(i)%dZ + coil(i)%nbands_R + coil(i)%nbands_Z    &
        + coil(i)%n_fila + sum(coil(i)%n_fila_turns) + sum(coil(i)%R_fila) + sum(coil(i)%Z_fila)   &
        + coil(i)%n_pts + coil(i)%width + sum(coil(i)%xpts) + sum(coil(i)%ypts) + sum(coil(i)%zpts)
    end do
    
  end function checksum_coils
  
  
  
  ! --- Calculate a checksum for coil triangles (useful for testing correct MPI broadcast)
  real function checksum_triangles(tri)
    
    ! --- Routine parameters
    type (t_triangles), intent(in) :: tri
    
    checksum_triangles = tri%ntri_c
    if ( allocated(tri%jtri_c       ) ) checksum_triangles = checksum_triangles + sum(tri%jtri_c)
    if ( allocated(tri%x_coil       ) ) checksum_triangles = checksum_triangles + sum(tri%x_coil)
    if ( allocated(tri%y_coil       ) ) checksum_triangles = checksum_triangles + sum(tri%y_coil)
    if ( allocated(tri%z_coil       ) ) checksum_triangles = checksum_triangles + sum(tri%z_coil)
    if ( allocated(tri%phi_coil     ) ) checksum_triangles = checksum_triangles + sum(tri%phi_coil)
    if ( allocated(tri%eta_thin_coil) ) checksum_triangles = checksum_triangles + sum(tri%eta_thin_coil)
    
  end function checksum_triangles
  
  
  
  ! --- Verify consistency of checksum for a coil set across MPI processors
  subroutine verify_checksum(coil_set, rank)
    
    use mpi
    
    ! --- Routine parameters
    type (t_coil_set),  intent(in) :: coil_set
    integer,            intent(in) :: rank
    
    ! --- Local variables
    integer :: ierr
    real    :: checksum_local, checksum_0
    
    if ( COIL_DEBUG ) write(*,*)
    if ( COIL_DEBUG ) write(*,*) '    verify_checksum ', trim(coil_set%name), rank
    
    checksum_local = checksum_coil_set(coil_set)
    checksum_0     = checksum_local
    call MPI_bcast(checksum_0, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    
    if ( checksum_local /= checksum_0 ) then
      write(*,'(a,i5,2a)') 'ERROR: Checksum problem on rank ', rank, ' for coil set ', trim(coil_set%name)
      stop
    end if
    
    if ( COIL_DEBUG ) write(*,*) '    Finished verify_checksum'
    
  end subroutine verify_checksum
  
  
  
  ! --- Auxilliary function calculating the norm of a vector
  real function my_norm(vec)
    real, dimension(3), intent(in) :: vec
    my_norm = sqrt( vec(1)**2 + vec(2)**2 + vec(3)**2)
  end function my_norm
  
  
  
  ! --- Auxilliary function calculating the cross-product of two vectors
  function my_cross(vec1, vec2)
    real, dimension(3), intent(in) :: vec1, vec2
    real, dimension(3)             :: my_cross, prod
    
    prod(1)  =  vec1(2)*vec2(3) - vec1(3)*vec2(2)
    prod(2)  =  vec1(3)*vec2(1) - vec1(1)*vec2(3)
    prod(3)  =  vec1(1)*vec2(2) - vec1(2)*vec2(1)
    my_cross = prod
  end function my_cross
  
end module coils
