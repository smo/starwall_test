!----------------------------------------------------------------------
subroutine matrix_ww
! ----------------------------------------------------------------------
!   purpose:                                                  16/11/05
!! ----------------------------------------------------------------------
      use solv
      use tri_w
      use coil_tria
      use tri ! new module with arrays and variables for tri_induct subroutine
      use mpi_v ! new module with mpi variables
      use sca ! new module for ScaLAPACK variables
      use mpi
! ----------------------------------------------------------------------
      implicit none

      real,   dimension(:,:),allocatable  :: dxw,dyw,dzw
      real,   dimension(  :),allocatable  :: jx,jy,jz,jwx,jwy,jwz,jwx2,jwy2,jwz2
      integer,dimension(:,:), allocatable :: ipot_w

      real                                :: temp,aa
      integer                             :: i,i1,j,j1,k,k1
      real :: dima_sca,dima_sca2,dima_sum
    
      integer    j_loc,i_loc
      logical    inside_j,inside_i
     

! ----------------------------------------------------------------------
      if(rank==0) write(*,*) 'compute matrix a_ww'

      allocate(dxw(ntri_w,3),dyw(ntri_w,3),dzw(ntri_w,3)             &
              ,jx(ntri_w),   jy(ntri_w),   jz(ntri_w)                &
              ,jwx(ntri_w),  jwy(ntri_w),  jwz(ntri_w)               &
              ,jwx2(ntri_w),  jwy2(ntri_w),  jwz2(ntri_w)            &
              ,ipot_w(ntri_w,3),stat=ier           )
              if (ier /= 0) then
                  print *,'Matrix_ww.f90: Can not allocate local arrays a_ww, ..., ipot_w'
                  stop
             endif

      dxw=0.; dyw=0.; dzw=0.; jx=0.; jy=0.; jz=0.
      jwx=0.; jwy=0.; jwz=0.;  jwx2=0.; jwy2=0.; jwz2=0.; ipot_w=0
! Additional arrays that will be used for tri_induct subroutine
! For the production run with n_tri_w=5*10^5 the size of all array will be around 300Mb
! if they will not be distributed among MPI tasks

     allocate(nx(ntri_w),ny(ntri_w),nz(ntri_w),tx1(ntri_w),         &
              ty1(ntri_w),tz1(ntri_w),tx2(ntri_w),ty2(ntri_w),      &
              tz2(ntri_w),tx3(ntri_w),ty3(ntri_w),tz3(ntri_w),      &
              d221(ntri_w),d232(ntri_w),d213(ntri_w),area(ntri_w),  &
              xm(7*ntri_w), ym(7*ntri_w), zm(7*ntri_w),stat=ier)
              if (ier /= 0) then
                  print *,'Matrix_ww.f90: Can not allocate local arrays nx, ..., zm'
                  stop
              endif

  nx=0.; ny=0.; nz=0.; tx1=0.; ty1=0.;tz1=0.; tx2=0.
  ty2=0.; tz2=0.; tx3=0.; ty3=0.; tz3=0.; d221=0.
  d232=0.; d213=0.;area=0.; xm=0.; ym=0.; zm=0.
! ----------------------------------------------------------------------
      ipot_w = jpot_w

      do i=1,ntri_w
         dxw(i,1) = xw(i,2) - xw(i,3)
         dyw(i,1) = yw(i,2) - yw(i,3)
         dzw(i,1) = zw(i,2) - zw(i,3)
         dxw(i,2) = xw(i,3) - xw(i,1)
         dyw(i,2) = yw(i,3) - yw(i,1)
         dzw(i,2) = zw(i,3) - zw(i,1)
         dxw(i,3) = xw(i,1) - xw(i,2)
         dyw(i,3) = yw(i,1) - yw(i,2)
         dzw(i,3) = zw(i,1) - zw(i,2)

         jx(i) = phi0_w(i,1)*dxw(i,1)+phi0_w(i,2)*dxw(i,2)+phi0_w(i,3)*dxw(i,3)
         jy(i) = phi0_w(i,1)*dyw(i,1)+phi0_w(i,2)*dyw(i,2)+phi0_w(i,3)*dyw(i,3)
         jz(i) = phi0_w(i,1)*dzw(i,1)+phi0_w(i,2)*dzw(i,2)+phi0_w(i,3)*dzw(i,3)

         call tri_induct_1(i,xw,yw,zw,ntri_w)
         call tri_induct_2(ntri_w,i,xw,yw,zw)

         do k=1,3
            if(jpot_w(i,k).eq.npot_w) then
              dxw(i,k) = 0.
              dyw(i,k) = 0.
              dzw(i,k) = 0.
              ipot_w(i,k) = 1
             endif
        enddo
       enddo


    call  SCA_GRID(npot_w+ncoil,npot_w+ncoil)
    !allocate local matrix
    allocate(a_ww_loc(MP_A,NQ_A), stat=ier)
    IF (IER /= 0) THEN
             WRITE (*,*) "a_ww, can not allocate local matrix a_ww_loc MYPROC_NUM=",MYPNUM
             STOP
    END IF
    a_ww_loc=0.
    LDA_ww= LDA_A

    step=ntri_w/NPROCS
    ntri_w_loc_b=(rank)*step+1
    ntri_w_loc_e=(rank+1)*step
    if(rank==NPROCS-1) ntri_w_loc_e=ntri_w


    do i =ntri_w_loc_b,ntri_w_loc_e
        do i1=1,ntri_w

            dima_sca=0
            call tri_induct_3_2(ntri_w,i,i1,xw,yw,zw,dima_sca)
            jwx(i) = jwx(i) + jx(i1)*dima_sca
            jwy(i) = jwy(i) + jy(i1)*dima_sca
            jwz(i) = jwz(i) + jz(i1)*dima_sca

        enddo ! do i1=1,ntri_w
    enddo !do i =ntri_w_loc_b,ntri_w_loc_e


    CALL MPI_ALLREDUCE(jwx, jwx2, ntri_w, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
    CALL MPI_ALLREDUCE(jwy, jwy2, ntri_w, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
    CALL MPI_ALLREDUCE(jwz, jwz2, ntri_w, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)


    jwx=jwx2
    jwy=jwy2
    jwz=jwz2

    deallocate(jwx2,jwy2,jwz2)


    do i =1,ntri_w
          do i1=1,ntri_w
               do k =1,3
                   j = ipot_w(i,k) + 1
                       call  ScaLAPACK_mapping_i(j+ncoil,i_loc,inside_i)
                       if (inside_i) then! If not inside

                            do k1=1,3
                               j1 = ipot_w(i1,k1) + 1
                               call ScaLAPACK_mapping_j(j1+ncoil,j_loc,inside_j)
                               if (inside_j) then! If not inside
                               
                                    dima_sca=0
                                    dima_sca2=0
                                    call tri_induct_3_2(ntri_w,i,i1,xw,yw,zw,dima_sca)
                                    call tri_induct_3_2(ntri_w,i1,i,xw,yw,zw,dima_sca2)

                                    dima_sum=dima_sca+dima_sca2

                                    temp  = .5*(dxw(i,k)*dxw(i1,k1)             &
                                               +dyw(i,k)*dyw(i1,k1)             &
                                               +dzw(i,k)*dzw(i1,k1))            &
                                               *dima_sum

                                    a_ww_loc(i_loc,j_loc) = a_ww_loc(i_loc,j_loc) + temp

                                endif

                            enddo ! do k1=1,3
                      endif ! if (inside_i /= .true.)
               enddo ! do k =1,3
          enddo ! do i1=1,ntri_w
       enddo !do i =1,ntri_w

    call ScaLAPACK_mapping_i(1+ncoil,i_loc,inside_i)
    if (inside_i) then

        call ScaLAPACK_mapping_j(1+ncoil,j_loc,inside_j)
        if (inside_j) then

            aa = 0.
            do i=1,ntri_w
                aa = aa +jx(i)*jwx(i)+jy(i)*jwy(i)+jz(i)*jwz(i)
            enddo

            a_ww_loc(i_loc,j_loc) = aa

        endif
    endif

 
       do i=1,ntri_w
        do k=1,3
               j = ipot_w(i,k)
                call ScaLAPACK_mapping_i(1+ncoil,i_loc,inside_i)
                if (inside_i) then

                    call ScaLAPACK_mapping_j(1+j+ncoil,j_loc,inside_j)
                    if (inside_j) then

                        temp  = jwx(i)*dxw(i,k)                            &
                               +jwy(i)*dyw(i,k)                            &
                               +jwz(i)*dzw(i,k)

                        a_ww_loc(i_loc,j_loc) = a_ww_loc(i_loc,j_loc) +temp

                    endif
                endif

                call ScaLAPACK_mapping_i(1+ncoil+j,i_loc,inside_i)
                if (inside_i) then

                    call ScaLAPACK_mapping_j(1+ncoil,j_loc,inside_j)
                    if (inside_j) then


                       temp  = jwx(i)*dxw(i,k)                            &
                               +jwy(i)*dyw(i,k)                            &
                               +jwz(i)*dzw(i,k)

                        a_ww_loc(i_loc,j_loc) = a_ww_loc(i_loc,j_loc) +temp

                   endif
               endif
        enddo
       enddo
       

      deallocate(ipot_w,jwz,jwy,jwx,jz,jy,jx,dzw,dyw,dxw)
!deallocating all temporary arrays
      deallocate(nx,ny,nz,tx1,ty1,tz1,tx2,ty2, &
                 tz2,tx3,ty3,tz3,d221,d232,d213,area,xm,ym,zm)

if(rank==0) write(*,*) 'matrix_ww  done'
if(rank==0) write(*,*) '==============================================================='

end subroutine matrix_ww


