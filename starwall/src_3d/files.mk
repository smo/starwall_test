F_MAIN_SRC = mod_icontr.f90\
             mod_contr_su.f90\
             mod_tri_w.f90\
             mod_tri_p.f90\
             mod_coils.f90\
             mod_coil_tria.f90\
             mod_solv.f90\
             mod_tri.f90\
             mod_tri_b.f90\
             mod_mpi_v.f90\
             mod_sca.f90\
             mod_time.f90\
             mod_resistive.f90\
             main.f90\
             input.f90\
             solver.f90\
             simil_trafo.f90\
             resistive_wall_response.f90\
             bfield_c.f90\
             bfield_par.f90\
             tri_induct.f90\
             matrix_pp.f90\
             matrix_wp.f90\
             matrix_ww.f90\
             matrix_rw.f90\
             matrix_cc.f90\
             matrix_cp.f90\
             matrix_wc.f90\
             matrix_rc.f90\
             basisfunctions1.f90\
             control_boundary.f90\
             surface_wall.f90\
             tri_contr_surf.f90\
             real_space2bezier.f90\
             real_space2bezier_par.f90\
             matrix_ep.f90\
             matrix_ew.f90\
             matrix_ec.f90\
             matrix_pe.f90\
             mpi_grid.f90\
             sca_grid.f90\
             scalapack_mapping.f90\
             a_pe_transpose_sca.f90\
             cholesky_solver.f90\
             a_ee_computing.f90\
             a_ew_computing.f90\
             a_we_computing.f90\
             a_pwe_s_computing.f90\
             matrix_multiplication.f90\
             a_ey_computing.f90\
             a_ye_computing.f90\
             d_ee_computing.f90\
             mpi_and_scalapack_init.f90\
             control_array_distribution.f90\
             computing_s_ww_inverse.f90\
             get_index_dima.f90\
             wall2vtk.f90\
             write_response.f90\
             write_response_formatted.f90\
             write_response_unformatted.f90\
             write_routines.f90\
             memory_prediction.f90
F_MAIN_OBJ = $(addprefix $(OBJ_DIR)/,$(F_MAIN_SRC:.f90=.o))

MAIN_OBJ = $(F_MAIN_OBJ) 

########################################################################
#     D E P E N D E N C I E S                                          #
########################################################################
$(OBJ_DIR)/main.o: \
            $(OBJ_DIR)/mod_icontr.o $(OBJ_DIR)/mod_mpi_v.o $(OBJ_DIR)/mod_coils.o\
            $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/MPI_and_Scalapack_Init.o: \
            $(OBJ_DIR)/mod_mpi_v.o
$(OBJ_DIR)/.o: \
            $(OBJ_DIR)/mod_mpi_v.o $(OBJ_DIR)/mod_sca.o
$(OBJ_DIR)/input.o: \
            $(OBJ_DIR)/mod_icontr.o $(OBJ_DIR)/mod_contr_su.o\
            $(OBJ_DIR)/mod_coils.o $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_mpi_v.o  $(OBJ_DIR)/mod_resistive.o\
            $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/control_boundary.o: \
            $(OBJ_DIR)/mod_icontr.o $(OBJ_DIR)/mod_contr_su.o\
            $(OBJ_DIR)/mod_mpi_v.o
$(OBJ_DIR)/matrix_pp.o: \
            $(OBJ_DIR)/mod_icontr.o $(OBJ_DIR)/mod_solv.o \
            $(OBJ_DIR)/mod_contr_su.o $(OBJ_DIR)/mod_tri_p.o\
            $(OBJ_DIR)/mod_tri.o $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_sca.o
$(OBJ_DIR)/matrix_wp.o: \
            $(OBJ_DIR)/mod_icontr.o $(OBJ_DIR)/mod_solv.o \
            $(OBJ_DIR)/mod_contr_su.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_tri_p.o  $(OBJ_DIR)/mod_coils.o\
            $(OBJ_DIR)/mod_tri.o    $(OBJ_DIR)/mod_tri_b.o\
            $(OBJ_DIR)/mod_mpi_v.o  $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/matrix_ww.o: \
            $(OBJ_DIR)/mod_tri_w.o $(OBJ_DIR)/mod_solv.o \
            $(OBJ_DIR)/mod_contr_su.o $(OBJ_DIR)/mod_coils.o\
            $(OBJ_DIR)/mod_tri.o $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_sca.o $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/matrix_rw.o: \
            $(OBJ_DIR)/mod_tri_w.o $(OBJ_DIR)/mod_solv.o\
            $(OBJ_DIR)/mod_icontr.o $(OBJ_DIR)/mod_coils.o\
            $(OBJ_DIR)/mod_mpi_v.o  $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/matrix_cc.o: \
            $(OBJ_DIR)/mod_coils.o $(OBJ_DIR)/mod_solv.o \
            $(OBJ_DIR)/mod_icontr.o $(OBJ_DIR)/mod_tri.o  \
            $(OBJ_DIR)/mod_tri_w.o  $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_sca.o $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/matrix_cp.o: \
            $(OBJ_DIR)/mod_coils.o $(OBJ_DIR)/mod_solv.o\
            $(OBJ_DIR)/mod_tri_p.o $(OBJ_DIR)/mod_icontr.o\
            $(OBJ_DIR)/mod_tri.o   $(OBJ_DIR)/mod_tri_b.o \
            $(OBJ_DIR)/mod_mpi_v.o $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/matrix_wc.o: \
            $(OBJ_DIR)/mod_coils.o $(OBJ_DIR)/mod_solv.o\
            $(OBJ_DIR)/mod_tri_w.o $(OBJ_DIR)/mod_icontr.o\
            $(OBJ_DIR)/mod_tri.o   $(OBJ_DIR)/mod_tri_b.o \
            $(OBJ_DIR)/mod_mpi_v.o $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/matrix_rc.o: \
            $(OBJ_DIR)/mod_coils.o $(OBJ_DIR)/mod_solv.o\
            $(OBJ_DIR)/mod_mpi_v.o $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/matrix_pe.o: \
            $(OBJ_DIR)/mod_contr_su.o $(OBJ_DIR)/mod_solv.o\
            $(OBJ_DIR)/mod_icontr.o   $(OBJ_DIR)/mod_tri_p.o\
            $(OBJ_DIR)/mod_tri_w.o    $(OBJ_DIR)/mod_coils.o\
            $(OBJ_DIR)/mod_mpi_v.o  $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/matrix_ep.o: \
            $(OBJ_DIR)/mod_contr_su.o $(OBJ_DIR)/mod_solv.o\
            $(OBJ_DIR)/mod_icontr.o   $(OBJ_DIR)/mod_tri_p.o\
            $(OBJ_DIR)/mod_mpi_v.o  $(OBJ_DIR)/mod_sca.o
$(OBJ_DIR)/matrix_ec.o: \
            $(OBJ_DIR)/mod_contr_su.o $(OBJ_DIR)/mod_solv.o\
            $(OBJ_DIR)/mod_icontr.o   $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_coils.o   $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/matrix_ew.o: \
            $(OBJ_DIR)/mod_contr_su.o $(OBJ_DIR)/mod_solv.o\
            $(OBJ_DIR)/mod_icontr.o   $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_coils.o  $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_sca.o $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/solver.o: \
            $(OBJ_DIR)/mod_solv.o     $(OBJ_DIR)/mod_coils.o\
            $(OBJ_DIR)/mod_tri_p.o    $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_icontr.o   $(OBJ_DIR)/mod_contr_su.o\
            $(OBJ_DIR)/mod_mpi_v.o    $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_time.o
$(OBJ_DIR)/resistive_wall_response.o: \
            $(OBJ_DIR)/mod_solv.o     $(OBJ_DIR)/mod_coils.o\
            $(OBJ_DIR)/mod_tri_w.o    $(OBJ_DIR)/mod_icontr.o\
            $(OBJ_DIR)/mod_contr_su.o $(OBJ_DIR)/mod_time.o\
            $(OBJ_DIR)/mod_tri_p.o    $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_resistive.o $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/tri_contr_surf.o: \
            $(OBJ_DIR)/mod_icontr.o $(OBJ_DIR)/mod_contr_su.o\
            $(OBJ_DIR)/mod_tri_p.o  $(OBJ_DIR)/mod_mpi_v.o
$(OBJ_DIR)/surface_wall.o: \
            $(OBJ_DIR)/mod_icontr.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_mpi_v.o
$(OBJ_DIR)/read_coil_data.o: \
            $(OBJ_DIR)/mod_coils.o 
$(OBJ_DIR)/simil_trafo.o: \
            $(OBJ_DIR)/mod_sca.o $(OBJ_DIR)/mod_mpi_v.o
$(OBJ_DIR)/control_array_distribution.o:\
            $(OBJ_DIR)/mod_mpi_v.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_tri_p.o $(OBJ_DIR)/mod_coils.o\
            $(OBJ_DIR)/mod_sca.o $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/a_pe_transpose_sca.o:\
            $(OBJ_DIR)/mod_sca.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_tri_p.o $(OBJ_DIR)/mod_solv.o\
            $(OBJ_DIR)/mod_coils.o $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/cholesky_solver.o:\
            $(OBJ_DIR)/mod_solv.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_tri_p.o $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_sca.o $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/a_pwe_s_computing.o:\
            $(OBJ_DIR)/mod_solv.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_tri_p.o $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_sca.o
$(OBJ_DIR)/a_ee_computing.o:\
            $(OBJ_DIR)/mod_solv.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_tri_p.o $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_sca.o $(OBJ_DIR)/mod_coils.o
$(OBJ_DIR)/a_ew_computing.o:\
            $(OBJ_DIR)/mod_solv.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_tri_p.o $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_sca.o $(OBJ_DIR)/mod_coils.o
$(OBJ_DIR)/a_we_computing.o:\
            $(OBJ_DIR)/mod_solv.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_tri_p.o $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_sca.o $(OBJ_DIR)/mod_coils.o
$(OBJ_DIR)/matrix_multiplication.o:\
            $(OBJ_DIR)/mod_solv.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_tri_p.o $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_sca.o $(OBJ_DIR)/mod_coils.o
$(OBJ_DIR)/sca_grid.o:\
            $(OBJ_DIR)/mod_sca.o
$(OBJ_DIR)/a_ey_computing.o:\
            $(OBJ_DIR)/mod_solv.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_mpi_v.o $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_resistive.o
$(OBJ_DIR)/a_ye_computing.o:\
            $(OBJ_DIR)/mod_solv.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_mpi_v.o $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_resistive.o
$(OBJ_DIR)/d_ee_computing.o:\
            $(OBJ_DIR)/mod_solv.o $(OBJ_DIR)/mod_tri_w.o\
            $(OBJ_DIR)/mod_mpi_v.o $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_resistive.o
$(OBJ_DIR)/computing_s_ww_inverse.o:\
            $(OBJ_DIR)/mod_mpi_v.o $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_resistive.o
$(OBJ_DIR)/get_index_dima.o:\
            $(OBJ_DIR)/mod_tri_p.o $(OBJ_DIR)/mod_icontr.o\
            $(OBJ_DIR)/mod_contr_su.o
$(OBJ_DIR)/mod_coils.o:\
            $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/wall2vtk.o:\
            $(OBJ_DIR)/mod_tri_w.o
$(OBJ_DIR)/write_response.o:\
            $(OBJ_DIR)/mod_solv.o     $(OBJ_DIR)/mod_coils.o\
            $(OBJ_DIR)/mod_tri_w.o    $(OBJ_DIR)/mod_icontr.o\
            $(OBJ_DIR)/mod_contr_su.o $(OBJ_DIR)/mod_time.o\
            $(OBJ_DIR)/mod_tri_p.o    $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_resistive.o\
            $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/mpi_and_scalapack_init.o:\
            $(OBJ_DIR)/mod_mpi_v.o $(OBJ_DIR)/mod_time.o 
$(OBJ_DIR)/bfield_par.o:\
            $(OBJ_DIR)/mod_mpi_v.o
$(OBJ_DIR)/write_response_unformatted.o:\
            $(OBJ_DIR)/mod_solv.o     $(OBJ_DIR)/mod_coils.o\
            $(OBJ_DIR)/mod_tri_w.o    $(OBJ_DIR)/mod_icontr.o\
            $(OBJ_DIR)/mod_contr_su.o $(OBJ_DIR)/mod_time.o\
            $(OBJ_DIR)/mod_tri_p.o    $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_resistive.o\
            $(OBJ_DIR)/mod_coil_tria.o $(OBJ_DIR)/write_routines.o
$(OBJ_DIR)/write_response_formatted.o:\
            $(OBJ_DIR)/mod_solv.o     $(OBJ_DIR)/mod_coils.o\
            $(OBJ_DIR)/mod_tri_w.o    $(OBJ_DIR)/mod_icontr.o\
            $(OBJ_DIR)/mod_contr_su.o $(OBJ_DIR)/mod_time.o\
            $(OBJ_DIR)/mod_tri_p.o    $(OBJ_DIR)/mod_sca.o\
            $(OBJ_DIR)/mod_resistive.o\
            $(OBJ_DIR)/mod_coil_tria.o
$(OBJ_DIR)/basic_check.o:\
            $(OBJ_DIR)/mod_tri_w.o    $(OBJ_DIR)/mod_tri_p.o\
            $(OBJ_DIR)/mod_resistive.o
$(OBJ_DIR)/mpi_grid.o:\
            $(OBJ_DIR)/mod_sca.o    $(OBJ_DIR)/mod_mpi_v.o
$(OBJ_DIR)/write_routines.o:\
            $(OBJ_DIR)/mod_mpi_v.o  $(OBJ_DIR)/mod_resistive.o
$(OBJ_DIR)/memory_prediction.o:\
            $(OBJ_DIR)/mod_coil_tria.o $(OBJ_DIR)/mod_mpi_v.o\
            $(OBJ_DIR)/mod_tri_w.o     $(OBJ_DIR)/mod_tri_p.o\
            $(OBJ_DIR)/mod_resistive.o $(OBJ_DIR)/mod_solv.o\
            $(OBJ_DIR)/mod_icontr.o    $(OBJ_DIR)/mod_contr_su.o
