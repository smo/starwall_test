module tri_w

   implicit none
   integer                            :: ntri_w,npot_w,npot_w1
   real,   allocatable,dimension(:,:) :: xw, yw, zw, phi0_w
   real,   allocatable,dimension(:)   :: eta_thin_wall
   integer,allocatable,dimension(:,:) :: jpot_w

end module tri_w
