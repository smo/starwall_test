module sca
       
        implicit none
        
        integer :: MYPNUM,NPROCS,CONTEXT,NPROW,NPCOL,MYCOL,MYROW,NB,MP_A,NQ_A,LDA_A
        integer :: ipc,ipr
        integer :: LDA_wp,LDA_pwe, LDA_pp,LDA_ep,LDA_ew, LDA_pwe_s,&
                   LDA_we,LDA_ee,LDA_ww,LDA_rw,LDA_sww, LDA_s_ww_inv
        integer :: LDA_ey,LDA_ye,LDA_dee
        integer :: DESCA(9),DESCB(9),DESCZ(9),DESCC(9)
        integer :: DESC_ye(9)
        integer :: INFO,INFO_A,INFO_B,INFO_Z,INFO_C
        real    :: ORFAC
        integer :: lwork_cooficient  

end module sca
