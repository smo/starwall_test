module contr_su

   implicit none
   integer                            :: n_points, nu, n_bnd, n_bnd_nodes, n_dof_bnd
   integer,dimension(:,:),allocatable :: bnd_node_index
   integer,dimension(:,:),allocatable :: bnd_node
   integer,dimension(:)  ,allocatable :: bnd_node_ndof
   real,dimension(:,:,:) ,allocatable :: Lij
   real,dimension(    :) ,allocatable :: Rc,Zc,Rc_s,Zc_s
   real,dimension(    :) ,allocatable :: x,y,z,xu,yu,zu
   real,dimension(  2,2)              :: H, H_s, H_ss

end module contr_su
