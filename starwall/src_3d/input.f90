subroutine input
! my new input

  use icontr
  use contr_su
  use coil_tria

  use sca
  use mpi_v
  use resistive
  use mpi
  
  implicit none

  integer :: i
  
  ! Namelist with input parameters
  namelist / params / i_response, n_harm, n_tor, nv, delta, n_points, nwall, iwall, nu_coil,&
             NB, ORFAC, lwork_cooficient, format_type, pol_coil_file, rmp_coil_file,        &
             voltage_coil_file, diag_coil_file
  ! --- Output code information
  if(rank==0)  write(*,*) '-------------------------------------------'
  if(rank==0)  write(*,*) 'STARWALL-JOREK vacuum response code'
  if(rank==0)  write(*,*) '   version 2010-08-10 by P. Merkel'
  if(rank==0)  write(*,*) 'Parallel version 2016-06-01 by S. Mochalskyy'
  if(rank==0)  write(*,*) '-------------------------------------------'
  
  ! --- Preset and read input parameters
  i_response = 2
  nv = 32
  delta = 0.001
  n_points = 10
  nwall = 1
  iwall = 1
  nu_coil = 0
  n_harm = 11
  ncoil=0
  ntri_c=0
  pol_coil_file     = 'none'
  rmp_coil_file     = 'none'
  voltage_coil_file = 'none'
  diag_coil_file    = 'none'

  NB = 64
  ORFAC = 0.00000001
  lwork_cooficient = 3
  format_type = 'unformatted'

  if(n_harm>1) then
     do i=1,n_harm
         n_tor(i)=i
     enddo
  else
         n_tor=0
  endif
  
  if ( nu_coil /= 0 ) then
    write(*,*) 'ERROR: Input parameter nu_coil is not supported any more.'
    write(*,*) '  If you want to use coils, polcoil_file etc needs to be set.'
    stop
  end if
  
  ! Only one task read data from input file and send them after to all tasks
  if(rank==0) read(*, params) ! Read namelist from STDIN
  !==========================================================================
  call MPI_BCAST(i_response,        1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(n_harm,            1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(n_tor,             n_harm, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(nv,                1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(delta,             1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(n_points,          1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(nwall,             1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(iwall,             1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(nu_coil,           1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(pol_coil_file,     256, MPI_CHARACTER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(rmp_coil_file,     256, MPI_CHARACTER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(voltage_coil_file, 256, MPI_CHARACTER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(diag_coil_file,    256, MPI_CHARACTER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(NB,                1, MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(ORFAC,             1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(lwork_cooficient,  1, MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(format_type,       len(format_type), MPI_CHARACTER,0, MPI_COMM_WORLD, ier)
 !==========================================================================

  coils_present = ( trim(pol_coil_file) /= 'none' ) .or. ( trim(rmp_coil_file) /= 'none' ) &
    .or. ( trim(voltage_coil_file) /= 'none' ) .or. ( trim(diag_coil_file) /= 'none' ) 

  ! --- Log input parameters

  if(rank==0) then
       write(*,'(A,I4)')
       write(*,*) 'Input parameters:'
       write(*,'(A,I4)')     '  i_response         =', i_response
       write(*,'(A,I4)')     '  n_harm             =', n_harm
       write(*,'(A,20I4)')   '  n_tor              =', n_tor(1:n_harm)
       write(*,'(A,I4)')     '  nv                 =', nv
       write(*,'(A,ES10.3)') '  delta              =', delta
       write(*,'(A,I4)')     '  n_points           =', n_points
       write(*,'(A,I4)')     '  nwall              =', nwall
       write(*,'(A,I4)')     '  iwall              =', iwall
       write(*,'(2A)')       '  pol_coil_file      = ', trim(pol_coil_file)
       write(*,'(2A)')       '  rmp_coil_file      = ', trim(rmp_coil_file)
       write(*,'(2A)')       '  voltage_coil_file  = ', trim(voltage_coil_file)
       write(*,'(2A)')       '  diag_coil_file     = ', trim(diag_coil_file)
       write(*,'(A,L)')      '  coils_present      = ', coils_present
       write(*,'(A,I4)')
 
       write(*,*) ' ScaLAPACK Input parameters:'
       write(*,'(A,I4)')     '  NB                =', NB
       write(*,'(A,ES10.3)') '  ORFAC             =', ORFAC
       write(*,'(A,I4)')

       write(*,*) 'Output starwall-response.dat:'
       write(*,'(A,A)')     ' file format         =  ',format_type
       write(*,'(A,I4)')
  endif


  ! --- Check input parameters
  if ( ( i_response < 0 ) .or. ( i_response > 2 ) ) then
    write(*,*) 'ERROR: i_response must have a value between 0 and 2'
    stop 1
  end if
  
  if ( n_harm < 1 ) then
    write(*,*) 'ERROR: n_harm must be one or larger'
    stop 1
  end if

  do i=2,n_harm
    if(n_tor(i).le.n_tor(i-1)) then
      write(*,*) 'ERROR: toroidal harmonics have to be in increasing order'
      stop 1
    endif
  end do

  if ( ( iwall < 1 ) .or. ( iwall > 3 ) ) then
    write(*,*) 'ERROR: iwall must have a value between 1 and 3'
    stop 1
  end if 
 
   if ( lwork_cooficient<1) then
    write(*,*) 'ERROR: lwork_cooficient  must have a value between >1'
    stop 1
  end if
 

  ! --- Output additional information
  if(i_response.le.1 .AND. rank==0)  &
     write(*,*)'response matrix without wall will be computed' 
  if(i_response.eq.1 .AND. rank==0)  &
     write(*,*)'response matrix with wall will be computed' 
  if(i_response.eq.2 .AND. rank==0)  then
     write(*,*)'resistive wall response is computed' 
  end if
  if(rank==0) write(*,*)
  
  ! --- Derive quantities from input parameters
  nd_harm0 = 0
  nd_harm  = 4*n_harm
  if(n_tor(1).eq.0) then
    nd_harm0 = 2
    nd_harm  = 4*n_harm - 4
  end if
  
end subroutine input
