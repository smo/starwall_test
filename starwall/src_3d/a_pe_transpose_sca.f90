subroutine a_pe_transpose_sca

use sca
use tri_w
use tri_p
use solv
use coil_tria
use mpi_v

implicit none



if(rank==0) write(*,*) 'a_pwe transpose begins'

  ! Make matrix transpose
  ! This equvivalent to sequential version: 
  !do i=1,npot_p
  !  do k=1,nd_w
  !    a_pwe(i,k) = a_wp(k,i)
  !  enddo
  !enddo

   DESCA=0
   DESCC=0

   CALL DESCINIT( DESCA, npot_w+ncoil, npot_p, NB, NB, 0, 0, CONTEXT, LDA_wp, INFO_A )
   if(INFO_A .NE. 0) then
          write(6,*) "Something is wrong in a_pe_transpose_sca.f90 CALL DESCINIT DESCA, INFO_A=",INFO_A
          stop
   endif

   CALL DESCINIT( DESCC, npot_p, npot_w+ncoil, NB, NB, 0, 0, CONTEXT, LDA_pwe, INFO_C )
   if(INFO_C .NE. 0) then
          write(6,*) "Something is wrong in a_pe_transpose_sca.f90 CALL DESCINIT DESCC, INFO_C=",INFO_C
          stop
   endif

   ! ScaLAPACK function for parallel matrix transpose. Results will be directly inside matrix a_pwe_loc
   CALL PDTRAN (npot_p, npot_w+ncoil,  1.0D0, a_wp_loc, 1 , 1 ,  DESCA ,  0.0D0 , a_pwe_loc , 1 , 1 , DESCC  )
   

if(rank==0) write(*,*) 'a_pwe transpose done'
if(rank==0) write(*,*) '==============================================================='

end subroutine a_pe_transpose_sca







