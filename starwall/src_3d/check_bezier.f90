program check_bezier

real*8, allocatable :: f(:,:), bez(:,:), index(:), freco(:), Lij(:,:,:), Rij(:,:,:), Zij(:,:,:)
real*8              :: H(2,2)
integer,allocatable :: bnd_node(:,:), bnd_node_index(:,:), bnd_node_ndof(:)

open(921,file='fstar.dat',form='unformatted')
read(921) n_f,ndim

allocate(f(n_f,ndim))

read(921) f
close(921)

open(922,file='bstar.dat',form='unformatted')
read(922) n_dof_bnd,ndim 

allocate(bez(n_dof_bnd,ndim))
read(922) bez
close(922)

write(*,*) ' f   : ',n_f,ndim
write(*,*) ' bez : ',n_dof_bnd,ndim

!------------------------------------------------------------------------------
open(22,file='boundary.txt')

read(22,*) n_bnd, n_bnd_nodes        ! the number of boundary elements, nodes

allocate(Lij(2,2,n_bnd),Rij(2,2,n_bnd), Zij(2,2,n_bnd), bnd_node(n_bnd,2))

!----- Rij  : first index counts the two nodes of a boundary element
!             second index counts the basis function H
!             third index counts the number of the  boundary element

do i=1,n_bnd
  read(22,*) ibnd, bnd_node(i,1), bnd_node(i,2), &
             Rij(1,1,i), Zij(1,1,i), Rij(1,2,i), Zij(1,2,i), Lij(1,1,i), Lij(1,2,i), &
             Rij(2,1,i), Zij(2,1,i), Rij(2,2,i), Zij(2,2,i), Lij(2,1,i), Lij(2,2,i)
enddo

allocate(bnd_node_index(n_bnd_nodes,2),bnd_node_ndof(n_bnd_nodes))

do i=1,n_bnd_nodes
  read(22,*) j,bnd_node_index(i,:),bnd_node_ndof(i)   ! the index in the starwall response (as requested by JOREK)
  write(*,'(A,3i5)') 'bnd_node_index : ',i,bnd_node_index(i,:)
enddo

close(22)

n_bnd_elm = n_bnd
n_points  = 10

allocate(freco(n_bnd_elm*n_points))
freco = 0.

n = 32

do j=1,n_points

  s = float(j-1)/10.

  call basisfunctions1(s,H,H_s,H_ss)

  do k=1, n_bnd_elm

    im  = bnd_node_index(bnd_node(k,1),1)    ! node 1, first dof 
    ip  = bnd_node_index(bnd_node(k,1),2)    ! node 1, second dof
    im2 = bnd_node_index(bnd_node(k,2),1)    ! node 2, first dof
    ip2 = bnd_node_index(bnd_node(k,2),2)    ! node 2, second dof

    freco((k-1)*n_points+j) = freco((k-1)*n_points+j)            &
                            + bez(im,n)  * H(1,1) * Lij(1,1,k)   &
                            + bez(ip,n)  * H(1,2) * Lij(1,2,k)   &
                            + bez(im2,n) * H(2,1) * Lij(2,1,k)   &
                            + bez(ip2,n) * H(2,2) * Lij(2,2,k)

  enddo

enddo

do i=1,n_points*n_bnd_elm
  write(*,*) i,f(i,n),freco(i)
enddo

allocate(index(ndim))

np1 = 20 !ndim/16
np2 = n_f

do i=1,np2
  index(i) = i
enddo

call begplt('check.ps')

call lplot6(1,1,index(np1:np2),f(np1:np2,n),np2-np1+1,' ')
call lplot6(1,1,index(np1:np2),freco(np1:np2),-(np2-np1+1),' ')

call finplt

end

subroutine basisfunctions1(s,H,H_s,H_ss)
!--------------------------------------------------------------
! subroutine which defines the basis functions in one dimension
!
! derived from a mixed Bezier/Cubic finite element representation
!   index 1 : counts the vertex
!   index 2 : counts the variables (p,u,v,w)
!
! the functions are defined on the interval [0,1]
!----------------------------------------------------------------
implicit none
real*8 :: s, H(2,2), H_s(2,2), H_ss(2,2)


!---------------------------------------------------------- vertex (1)
H(1,1)   =(-1.d0 + s)**2*(1.d0 + 2.d0*s)
H_s(1,1) =6.d0*(-1.d0 + s)*s
H_ss(1,1)=6.d0*(-1.d0 + 2.d0*s)

H(1,2)   =3.d0*(-1.d0 + s)**2*s
H_s(1,2) =3.d0*(-1.d0 + s)*(-1.d0 + 3.d0*s)
H_ss(1,2)=6.d0*(-2.d0 + 3.d0*s)

!---------------------------------------------------------- vertex (2)
H(2,1)   =-s**2*(-3.d0 + 2.d0*s)
H_s(2,1) =-6.d0*(-1.d0 + s)*s
H_ss(2,1)=-6.d0*(-1.d0 + 2.d0*s)

H(2,2)   =-3.d0*(-1.d0 + s)*s**2
H_s(2,2) =-3.d0*s*(-2.d0 + 3.d0*s)
H_ss(2,2)=-6.d0*(-1.d0 + 3.d0*s)

end subroutine basisfunctions1
