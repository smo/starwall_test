subroutine solver
use icontr
use solv 
use contr_su
use tri_w
use tri_p
use coil_tria
use time
use mpi_v

implicit none

if(rank==0) write(*,*) ' SOLVER: n_dof_bnd : ',n_dof_bnd

nd_bez  = (nd_harm+nd_harm0)/2 * n_dof_bnd 
nd_w    = ncoil + npot_w
nd_we   = nd_w + nd_bez

if(rank==0) write(*,*) ' SOLVER: nd_bez : ',nd_bez
if(rank==0) write(*,*) ' SOLVER: nd_w :   ',nd_w
if(rank==0) write(*,*) ' SOLVER: nd_we :  ',nd_we
if(rank==0) write(*,*) ' SOLVER: npot_p : ',npot_p

!==================================================================
call matrix_pp
!==================================================================
    atime(5)=MPI_WTIME()
    if(rank==0) write(*,*) 'matrix_pp=',(atime(5)-atime(4)), '  seconds'


!==================================================================
call matrix_wp
!==================================================================
    atime(6)=MPI_WTIME()
    if(rank==0) write(*,*) 'matrix_wp=',(atime(6)-atime(5)), '  seconds'

!==================================================================
call matrix_ww
!==================================================================

    atime(7)=MPI_WTIME()
    if(rank==0) write(*,*) 'matrix_ww=',(atime(7)-atime(6)), '  seconds'

!==================================================================
call matrix_rw
!==================================================================

    atime(8)=MPI_WTIME()
    if(rank==0) write(*,*) 'matrix_rw=',(atime(8)-atime(7)), '  seconds'


!==================================================================
if ( coils_present ) then
  call matrix_cc
  call matrix_cp
  call matrix_wc
  call matrix_rc
endif
!==================================================================

    atime(9)=MPI_WTIME()
    if(rank==0) write(*,*) 'matrix_cc=',(atime(9)-atime(8)), '  seconds'


!==================================================================
call matrix_pe
!==================================================================

    atime(10)=MPI_WTIME()
    if(rank==0) write(*,*) 'matrix_cc=',(atime(10)-atime(9)), '  seconds'

!==================================================================
call matrix_ep
!==================================================================

    atime(11)=MPI_WTIME()
    if(rank==0) write(*,*) 'matrix_cc=',(atime(11)-atime(10)), '  seconds'

!==================================================================
call matrix_ew
!==================================================================


    atime(12)=MPI_WTIME()
    if(rank==0) write(*,*) 'matrix_cc=',(atime(12)-atime(11)), '  seconds'


!==================================================================
if ( coils_present ) then 
  call matrix_ec ! Here was a bug in sequential version
endif
!==================================================================

    atime(13)=MPI_WTIME()
    if(rank==0) write(*,*) 'matrix_ec=',(atime(13)-atime(12)), '  seconds'




if(rank==0) write(*,'(A,i6)') 'SOLVER : npot_p : ',npot_p 
if(rank==0) write(*,'(A,i6)') 'SOLVER : nd_w   : ',nd_w 
if(rank==0) write(*,'(A,2i7,A,2i7)') 'SOLVER : copying a_wp ', nd_w, npot_p,' to a_pwe ',npot_w,nd_we
if(rank==0) write(*,*) '==============================================================='

!==================================================================
call a_pe_transpose_sca


    atime(14)=MPI_WTIME()
    if(rank==0) write(*,*) 'a_pe_transpose_sca=',(atime(14)-atime(13)), '  seconds'

call cholesky_solver
!==================================================================

    atime(15)=MPI_WTIME()
    if(rank==0) write(*,*) 'cholesky_solver=',(atime(15)-atime(14)), 'seconds'

deallocate(a_pp_loc)

!==================================================================
call a_pwe_s_computing
!==================================================================

    atime(16)=MPI_WTIME()
    if(rank==0) write(*,*) 'a_pwe_s_computing=',(atime(16)-atime(15)), 'seconds'

!==================================================================
call a_ee_computing
!==================================================================


    atime(17)=MPI_WTIME()
    if(rank==0) write(*,*) 'a_ee_computing=',(atime(17)-atime(16)), 'seconds'


!==================================================================
call a_ew_computing
!==================================================================


    atime(18)=MPI_WTIME()
    if(rank==0) write(*,*) 'a_ew_computing=',(atime(18)-atime(17)), 'seconds'


!==================================================================
call a_we_computing
!==================================================================


    atime(19)=MPI_WTIME()
    if(rank==0) write(*,*) 'a_we_computing=',(atime(19)-atime(18)), 'seconds'



!==================================================================
call matrix_multiplication
!==================================================================

    atime(20)=MPI_WTIME()
    if(rank==0) write(*,*) 'matrix_multiplication=',(atime(20)-atime(19)), 'seconds'


deallocate(a_wp_loc,a_pwe_loc,a_ep_loc_sca)

end subroutine solver
