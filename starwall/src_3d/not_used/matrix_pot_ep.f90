! ----------------------------------------------------------------------
subroutine matrix_pot_ep
! ----------------------------------------------------------------------
!     purpose:                                              17/08/09
!
! ----------------------------------------------------------------------
use icontr
use contr_su
use tri_p
use tri_w
use solv
! ----------------------------------------------------------------------
implicit none
! ----------------------------------------------------------------------
real,dimension(:,:),allocatable :: phi,b_par0
real,dimension(  :),allocatable :: bx,by,bz
real,dimension(:,:),allocatable :: a_c,a_s,b_c,b_s
real                            :: pi2,alv,fnv
integer                         :: i,i0,i1,ier,j,k,kp,ku,kv,nuv
! ----------------------------------------------------------------------
write(6,*) 'compute matrix_ep'
pi2 = 4.*asin(1.)
nuv = nu*nv
fnv = 1./float(nv)
alv = pi2*fnv

allocate(phi(nuv,npot_p),stat=ier) ; phi = 0. 
allocate(a_c(nu,npot_p),  a_s(nu,npot_p)) ; a_c=0.; a_s=0.
allocate(b_c(n_dof_bnd,npot_p),b_s(n_dof_bnd,npot_p)); b_c =0.; b_s=0.
 
call potential(phi,x,y,z,nuv,xp,yp,zp,ntri_p,jpot_p,npot_p)

if(n_tor(1).eq.0) then

  allocate(b_par0(nuv,npot_p),stat=ier) ; b_par0 = 0. 
  allocate(a_ep(n_dof_bnd,npot_p),stat=ier) ; a_ep = 0.
  allocate(bx(nuv),by(nuv),bz(nuv),stat=ier); bx=0.; by=0.; bz=0.

  call bfield_c(x,y,z,bx,by,bz,nuv,xp,yp,zp,phi0_p,ntri_p)
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO
  do i=1,nuv
    b_par0(i,1) = -(xu(i)*bx(i)+yu(i)*by(i)+zu(i)*bz(i)) 
  enddo
!$OMP END DO
!$OMP DO
  do k=1,npot_p1
    do i=1,nuv
!        b_par0(i,k+1) = -b_par(i,k)
    enddo
  enddo
!$OMP END DO
!$OMP DO  
  do k=1,npot_p
    do  ku=1,nu
      do  kv =1,nv
        a_c(ku,k) = a_c(ku,k) + b_par0(ku+nu*(kv-1),k)*fnv
      enddo
    enddo
  enddo
!$OMP END DO
!$OMP END PARALLEL
!--- -----------------------------------------------------------------------
  call real_space2bezier(b_c,a_c,N_bnd,n_points,npot_p,n_bnd_nodes,bnd_node,bnd_node_index,n_dof_bnd,Lij)
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i)
!$OMP DO
  do i =1,n_dof_bnd
    do k=1,npot_p
      a_ep(i  ,k)  = b_c(i,k) 
    enddo
  enddo
!$OMP END DO
!$OMP END PARALLEL
endif
!--------------------------------------------------------------------------
if (n_tor(1).ne.0.or.n_harm.gt.1) then
  allocate(a_ep(N_bnd*nd_harm,npot_p)) ; a_ep = 0.
!--------------------------------------------------------------------------
  i0 = 1
  if(n_tor(1).eq.0) &
    i0 = 2

  do i=i0,n_harm           !   toroidal harmonics

    a_c = 0.
    a_s = 0.
    b_c = 0.
    b_s = 0. 

    do  ku=1,nu
      do  kv =1,nv
        j = ku+nu*(kv-1)
        do k=1,npot_p
          a_c(ku,k) = a_c(ku,k) + phi(j,k)*cos(alv*n_tor(i)*(kv-1))*fnv*2
          a_s(ku,k) = a_s(ku,k) + phi(j,k)*sin(alv*n_tor(i)*(kv-1))*fnv*2
        enddo
      enddo
    enddo

    call real_space2bezier(b_c,a_c,N_bnd,n_points,npot_p,n_bnd_nodes,bnd_node,bnd_node_index,n_dof_bnd,Lij)
    call real_space2bezier(b_s,a_s,N_bnd,n_points,npot_p,n_bnd_nodes,bnd_node,bnd_node_index,n_dof_bnd,Lij)

    do k =1,N_bnd
      j = 1+4*(i-i0) + nd_harm*(k-1)
      do kp=1,npot_p
        a_ep(j  ,kp) = b_c(2*k-1,kp) 
        a_ep(j+1,kp) = b_c(2*k,  kp) 
        a_ep(j+2,kp) = b_s(2*k-1,kp) 
        a_ep(j+3,kp) = b_s(2*k,  kp) 
      enddo
    enddo

  enddo
endif

deallocate(b_s,b_c,a_s,a_c)
write(6,*) ' matrix_pot_ep done'

end subroutine matrix_pot_ep 
