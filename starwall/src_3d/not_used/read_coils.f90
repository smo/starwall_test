! ----------------------------------------------------------------------
      subroutine read_coils
! ----------------------------------------------------------------------
!     purpose:                                                 11/11/05
!
!
! ----------------------------------------------------------------------
      use fila
      use coil2d
! ----------------------------------------------------------------------
      implicit none
      real   ,dimension(:,:),allocatable :: xa,ya,za,xc,yc,zc,phic
      integer                            :: i,i1,j,k,k1,ne2,ntr,ier
! ----------------------------------------------------------------------
        ntr  = 0
        ne2 = 2*(nel-1)
! ----------------------------------------------------------------------
    allocate(jtri_c(nu_coil),xa(nel,  2),ya(nel,  2),za(nel,  2)       &
            ,xa(nel,  2),ya(nel,  2),za(nel,  2)                       &
            ,x_coil(ne2,3,nu_coil),  y_coil(ne2,3,nu_coil)             &
            ,z_coil(ne2,3,nu_coil),phi_coil(ne2,3,nu_coil)             &
            ,xc(ne2,3),yc(ne2,3),zc(ne2,3),phic(ne2,3),stat=ier        )
! ----------------------------------------------------------------------
       write(6,*) 'read coils filamente=',nu_coil

      do j=1,nu_coil
         jtri_c(j)= ne2
         ntr=ntr+jtri_c(j)

         xc =0. ; yc =0. ; zc = 0.

       do i=1,nel
         do  k=1,2
         xa(i,k) = x_fila(i,j,k)  
         ya(i,k) = y_fila(i,j,k)  
         za(i,k) = z_fila(i,j,k)  
       enddo
      enddo
 
        call coil_2d(xa,ya,za,nel,cur(j),xc,yc,zc,phic) 
         
      do i=1,ne2
       do k=1,3
           x_coil(i,k,j) =   xc(i,k)
           y_coil(i,k,j) =   yc(i,k)
           z_coil(i,k,j) =   zc(i,k)
         phi_coil(i,k,j) = phic(i,k)
       enddo
      enddo
     enddo
       write(6,*) 'read coils done'
   end subroutine  read_coils
