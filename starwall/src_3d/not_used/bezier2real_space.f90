!----------------------------------------------------------------------
subroutine bezier2real_space(bez,f,N_bnd,n_points,ndim)
! ----------------------------------------------------------------------
!   purpose:                                                  12/08/09
!
! ----------------------------------------------------------------------
implicit none
integer                              :: N_bnd,n_points,ndim
real,dimension(2*N_bnd,       ndim) :: bez 
real,dimension(N_bnd*n_points,ndim) :: f 
real,dimension(n_points           ) :: h11,h12,h21,h22
real,dimension(2   ,             2) :: H,H_s,H_ss 
real                                :: s
integer                             :: i,idim,im,im2,ip,ip2,j,k
 
f  = 0.

do j=1,n_points

  s = float(j-1)/float(n_points)

  call basisfunctions1(s,H,H_s,H_ss)

   h11(j) =  H(1,1)
   h12(j) =  H(1,2)
   h21(j) =  H(2,1)
   h22(j) = -H(2,2)
enddo

do i = 1, N_bnd

  im  = 2*i -1
  ip  = 2*i   
  im2 = im + 2
  ip2 = ip + 2
  if(im2.gt.2*N_bnd) im2 = 1           ! needs update for grid with corners
  if(ip2.gt.2*N_bnd) ip2 = 2

  do k=1,n_points
    j = k+n_points*(i-1)
    f(j,:) =  f(j,:) + h11(k)*bez(im ,:)                     &  ! the element size is missing
                     + h21(k)*bez(im2,:)                     & 
                     + h12(k)*bez(ip ,:)                     & 
                     + h22(k)*bez(ip2,:)
  enddo
enddo
end subroutine bezier2real_space
