!> Read input for various coil types
subroutine read_coil_data

  use icontr
  use coil2d
  use mpi_v
  use mpi
  
  implicit none
  
  if ( rank == 0 ) then
    write(*,*)
    write(*,*) '============================================================='
    write(*,*) ' read_coil_data'
  end if
  
  ncoil  = 0
  ntri_c = 0
  
   if ( pol_coil_file /= 'none' ) call read_poloidal_field_coils()
  
  if ( rank == 0 ) then
#ifdef DEBUG_OUTPUT 
    call print_coil_debug('coil_triangle.debug')
    close(61)
#endif
    write(*,*) ' End: read_coil_data'
    write(*,*) '============================================================='
  end if
  
  
end subroutine read_coil_data





!> Read input file defining the poloidal field coil geometry by R,Z positions and dR, dZ extents
!!
!! Each coil is described by several triangle bands.
subroutine read_poloidal_field_coils

  use icontr
  use coil2d
  use mpi_v
  use mpi

  implicit none

  real,    dimension(:),       allocatable :: cur_band
  real,    dimension(:,:),     allocatable :: xa, ya, za, xc, yc, zc, phic, eta_thin
  real,    dimension(:,:,:,:), allocatable :: x_band, y_band, z_band
  integer                                  :: i, j, j_tri, k, kR, kZ, l, ntot_bands, ntri_per_band
  integer                                  :: nmax_bands, nbands, nR, nZ
  real                                     :: pi2, dphi, r1, r2, z1, z2, rr, zz, dr, dz

  ! --- Read the poloidal coil input file and communicate to all MPI processors
  if ( rank == 0 ) then
    open(60, file=trim(pol_coil_file), status='old')
    read(60, polcoil_input)
    701 format(1x,3a)
    702 format(1x,a,i8)
    write(*,701) '=== Poloidal field coils ==='
    write(*,701) 'Filename:        "', trim(pol_coil_file), '"'
    write(*,701) 'Coil set:        "', trim(polcoil_set), '"'
    write(*,702) 'Toroidal points: ', polcoils_torpts
    write(*,702) 'Number of coils: ', n_polcoils
    write(*,701,advance='no') 'Coil names: '
    do i = 1, n_polcoils
      write(*,701,advance='no') '"', trim(polcoil(i)%name), '", '
    end do
    write(*,*)
#ifdef DEBUG_OUTPUT 
    write(*,701) 'Writing out coil geometry to file "polcoils.debug".'
    open(61, file='polcoils.debug', status='replace')
    do i = 1, n_polcoils
      write(61,*) polcoil(i)%R-polcoil(i)%dR/2, polcoil(i)%Z-polcoil(i)%dZ/2
      write(61,*) polcoil(i)%R+polcoil(i)%dR/2, polcoil(i)%Z-polcoil(i)%dZ/2
      write(61,*) polcoil(i)%R+polcoil(i)%dR/2, polcoil(i)%Z+polcoil(i)%dZ/2
      write(61,*) polcoil(i)%R-polcoil(i)%dR/2, polcoil(i)%Z+polcoil(i)%dZ/2
      write(61,*) polcoil(i)%R-polcoil(i)%dR/2, polcoil(i)%Z-polcoil(i)%dZ/2
      write(61,*)
      write(61,*)
    end do
#endif
    close(60)
  end if
  
  call MPI_BCAST(n_polcoils,      1, MPI_INTEGER,   0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(polcoil_set,    64, MPI_CHARACTER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(polcoils_torpts, 1, MPI_INTEGER,   0, MPI_COMM_WORLD, ier)
  do i = 1, n_polcoils
    call MPI_BCAST(polcoil(i)%name,     256, MPI_CHARACTER,        0, MPI_COMM_WORLD, ier)
    call MPI_BCAST(polcoil(i)%R,        1,   MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
    call MPI_BCAST(polcoil(i)%Z,        1,   MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
    call MPI_BCAST(polcoil(i)%dR,       1,   MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
    call MPI_BCAST(polcoil(i)%dZ,       1,   MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
    call MPI_BCAST(polcoil(i)%nbands_R, 1,   MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
    call MPI_BCAST(polcoil(i)%nbands_Z, 1,   MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
    call MPI_BCAST(polcoil(i)%resist,   1,   MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
  end do
  
  ! --- Construct geometry of the triangle bands
  pi2           = 4.*asin(1.)
  dphi          = pi2/float(polcoils_torpts-1)   ! Toroidal extent of one triangle [rad]
  ntri_per_band = 2*(polcoils_torpts-1)          ! Triangles per band
  ! Determine ntot_bands (total triangle bands) and nmax_bands (max triangle bands per coil)
  ntot_bands    = 0
  nmax_bands    = 0
  do i = 1, n_polcoils
    ntot_bands = ntot_bands + polcoil(i)%nbands_R * polcoil(i)%nbands_Z
    nmax_bands = max( nmax_bands, polcoil(i)%nbands_R * polcoil(i)%nbands_Z)
  end do
  ntri_c  = ntri_c + ntri_per_band * ntot_bands ! Total number of triangles of all coils

  if ( rank == 0 ) then
    write(*,*) 'Number of triangle "bands":  ', ntot_bands
    write(*,*) 'Max bands for a single coil: ', nmax_bands
    write(*,*) 'Number of triangles:  ', ntri_c
  end if
  
  allocate( x_coil(ntri_per_band*ntot_bands,3), y_coil(ntri_per_band*ntot_bands,3), z_coil(ntri_per_band*ntot_bands,3), &
    phi_coil(ntri_per_band*ntot_bands,3), jtri_c(n_polcoils), x_band(n_polcoils,nmax_bands,polcoils_torpts,2),         &
    y_band(n_polcoils,nmax_bands,polcoils_torpts,2), z_band(n_polcoils,nmax_bands,polcoils_torpts,2), cur_band(n_polcoils), &
    eta_thin(n_polcoils,nmax_bands), eta_thin_coil(ntri_per_band*ntot_bands) )

  do i = 1, n_polcoils
    nbands = polcoil(i)%nbands_R*polcoil(i)%nbands_Z
    cur_band(i)     = 1./float(nbands)      ! Distribute "current" between bands
    jtri_c(ncoil+i) = ntri_per_band*nbands  ! Triangles for this coil
  end do
  
  do i = 1, n_polcoils
    rr = polcoil(i)%R
    zz = polcoil(i)%Z
    dr = polcoil(i)%dR
    dz = polcoil(i)%dZ
    nR = polcoil(i)%nbands_R
    nZ = polcoil(i)%nbands_Z
    do kR = 1, nR
      do kZ = 1, nZ
        k = (kZ-1)*nR + kR
        ! R and Z positions of the triangle band
        r1 = rr-dr/2.*(1.-1./real(nR))+real(kR-1)*dr/real(nR)
        r2 = rr-dr/2.*(1.-1./real(nR))+real(kR-1)*dr/real(nR)
        z1 = zz+dz/2.-real(kZ-1)*dz/real(nZ)
        z2 = zz+dz/2.-real(kZ  )*dz/real(nZ)
        eta_thin(i,k) = polcoil(i)%resist * nR * dZ / ( pi2 * r1 )
        ! -------------------------------------------------------------------------
        ! Derivation for eta_thin:
        !
        !  Res: Resistance in Ohm
        !  eta: Resistivity on Ohm m
        !  eta_thin = eta / d: Thin wall resistivity
        !  nR: Number of bands in R direction
        !  nZ: Number of bands in Z direction
        !  i: band index in R direction
        !  j: band index in Z direction
        !  A_i,j = A / (nR * nZ): Surface of coil cross-section divided by bands
        ! 
        !  Res_i,j = Res * nR * nZ                ! Distribute resistance across bands
        !  eta_i,j = Res_i,j * A_i,j / l_i
        !          = Res_i,j * [ dR * dZ / (nR * nZ) ] / ( 2pi * R_i )
        !  eta_thin,i,j = eta / d
        !               = eta * n_R / dR
        !               = Res_i,j * dZ / ( 2pi * R_i * nZ )
        !               = Res * nR * dZ / ( 2pi * R_i )
        ! -------------------------------------------------------------------------
#ifdef DEBUG_OUTPUT 
        if ( rank == 0 ) then
          write(61,*) r1, z1
          write(61,*) r2, z2
          write(61,*)
          write(61,*)
        end if
#endif
        do j = 1, polcoils_torpts
          ! Cartesian coordinates of triangle nodes
          x_band(i,k,j,:) = (/r1, r2/) * cos(dphi*(j-1))
          y_band(i,k,j,:) = (/r1, r2/) * sin(dphi*(j-1))
          z_band(i,k,j,:) = (/z1, z2/)
        end do
      end do
    end do
  end do
  
  ! --- Construct triangles inside each band
  allocate(xc(ntri_per_band,3), yc(ntri_per_band,3), zc(ntri_per_band,3), phic(ntri_per_band,3), xa(polcoils_torpts,2), ya(polcoils_torpts,2), za(polcoils_torpts,2) )
  
  j_tri = 0
  do i = 1, n_polcoils
    do kR = 1, polcoil(i)%nbands_R
      do kZ = 1, polcoil(i)%nbands_Z
        k = (kZ-1)*polcoil(i)%nbands_R + kR
        xc = 0.
        yc = 0.
        zc = 0.
        do j = 1, polcoils_torpts
          do l = 1, 2
            xa(j,l) = x_band(i,k,j,l)
            ya(j,l) = y_band(i,k,j,l)
            za(j,l) = z_band(i,k,j,l)
          end do
        end do
        call coil_2d(xa, ya, za, polcoils_torpts, cur_band(i), xc, yc, zc, phic)
        do j = 1, ntri_per_band
          j_tri = j_tri + 1
          x_coil(j_tri,:)   = xc(j,:)
          y_coil(j_tri,:)   = yc(j,:)
          z_coil(j_tri,:)   = zc(j,:)
          phi_coil(j_tri,:) = phic(j,:)
          eta_thin_coil(j_tri) = eta_thin(i,k)
        end do
      end do
    end do
  end do
  
  ncoil = ncoil + n_polcoils ! total number of coils
  
  ! --- check that coil resistance is set up
  
  do i = 1, n_polcoils
    if (polcoil(i)%resist < 0.d0 ) then
      if (rank == 0) then
        write(*,*) ' ' 
        write(*,*) ' ERROR: RESISTANCE OF PF COIL ',i, ' NEEDS TO BE SET UP'
        write(*,*) ' ' 
      endif
      call MPI_FINALIZE(ier)
      stop
    endif
  end do 
   
end subroutine read_poloidal_field_coils
