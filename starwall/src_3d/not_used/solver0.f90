      subroutine solver0
       use icontr
       use solv 
       use contr_su
       use tri_w
       use tri_p
       use coil2d
!-----------------------------------------------------------------------
      implicit none
      integer                         :: i,ier,j,jp,k,nd_wcb,nd,nd_bez,n_w,info
!-----------------------------------------------------------------------
         nd_bez = 2*N_bnd
         nd_wcb = npot_w+ncoil+nd_bez
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO
      do i=1,npot_p
       do k=1,npot_w
        a0_pwe(i,k) = a0_wp(k,i)
       enddo
      enddo
!$OMP END DO
!$OMP END PARALLEL
!      call  dpof('U',a0_pp,npot_p,npot_p)
!      call dposm('U',a0_pp,npot_p,npot_p,a0_pwe,npot_p,nd_wcb)
      call dpotrf('U',npot_p,a0_pp,npot_p,info)
      call dpotrs('U',npot_p,nd_wcb,a0_pp,npot_p,a0_pwe,npot_p,info)
!--------------------------------------------------
!    compute parallel component of magnetic field
  allocate(a0_we(npot_w,nd_bez),a0_ee(N_bnd*nd_harm0,nd_bez))
       a0_ee = 0.
       a0_we = 0.
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(i,j,k)
!  compute    a_ee = a_ep*a_pp^(-1)*a_pe    a_ee = vacuum_response
!$OMP DO

     do k=1,nd_bez
    do i=1,N_bnd*nd_harm0
      do j=1,npot_p
         a0_ee(i,k) = a0_ee(i,k)+a0_ep(i,j)*a0_pwe(j,k+npot_w)
      enddo
     enddo
    enddo

!$OMP END DO
!$OMP END PARALLEL
!  compute    a_we = a_wp*a_pp^(-1)*a_pe
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(i,j,k)
!$OMP DO

     do i=1,npot_w
      do k= 1,nd_bez
       do j=1,npot_p                         
        a0_we(i,k) = a0_we(i,k) +a0_wp(i,j)*a0_pwe(j,k+npot_w)
       enddo
      enddo
     enddo

!$OMP END DO
!$OMP END PARALLEL
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(i,j,k)
!  compute    a_ew = a_ew - a_ep*a_pp^(-1)*a_pw
!$OMP DO

       do k=1,npot_w
      do i=1,N_bnd*nd_harm0
        do j=1,npot_p
           a0_ew(i,k) = a0_ew(i,k) - a0_ep(i,j)*a0_pwe(j,k)
        enddo
       enddo
      enddo

!$OMP END DO
!$OMP END PARALLEL
!    a01_ww = a0_ww - a0_wp*a0_pp^(-1)*a0_pw
!!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(i,j,k)
!!$OMP DO

!       do k=1,npot_w
!        do i=1,npot_w
!         do j=1,npot_p
!           a0_ww(i,k) = a0_ww(i,k) - a0_wp(i,j)*a0_pwe(j,k)
!         enddo
!        enddo
!       enddo

!!$OMP END DO
!!$OMP END PARALLEL
   call dgemm('N','N',npot_w,npot_w,npot_p,-1.,a0_wp,npot_w,a0_pwe,npot_p,1.,a0_ww,npot_w)
  if(i_response.eq.1)  then
      call ideal_wall_response_0
  else 
      call resistive_wall_response_0
     endif
!-------------------------------------------------------
 end subroutine solver0
