      subroutine solver1
       use icontr
       use solv 
       use contr_su
       use tri_w
       use tri_p
       use coil2d
!-----------------------------------------------------------------------
      implicit none
      integer                         :: i,ier,j,jp,k,n_w,n_we,nd_bez,info
!-----------------------------------------------------------------------
      nd_bez = nd_harm*N_bnd 
      n_w    = ncoil + npot_w1
      n_we   = n_w+nd_bez     
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(i,k)
!$OMP DO

      do i=1,npot_p1
       do k=1,n_w
        a_pwe(i,k) = a_wp(k,i)
       enddo
      enddo

!$OMP END DO
!$OMP END PARALLEL
!      call  dpof('U',a_pp,npot_p1,npot_p1)
!      call dposm('U',a_pp,npot_p1,npot_p1,a_pwe,npot_p1,n_we)
      call dpotrf('U',npot_p1,a_pp,npot_p1,info)
      call dpotrs('U',npot_p1,n_we,a_pp,npot_p1,a_pwe,npot_p1,info)
   deallocate(a_pp)
!-----------------------------------------------------------------------
!    compute parallel component of magnetic field
 
  allocate(a_we(n_w,nd_bez),a_ee(nd_bez,nd_bez))
       a_ee = 0.
       a_we = 0.

!  compute    a_ee = a_ep*a_pp^(-1)*a_pe    a_ee = vacuum_response
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(i,j,k)
!$OMP DO

    do i=1,nd_bez
     do k=1,nd_bez
      do j=1,npot_p1
         a_ee(i,k) = a_ee(i,k)+a_ep(i,j)*a_pwe(j,k+npot_w1)
      enddo
     enddo
    enddo

!$OMP END DO
!$OMP END PARALLEL
!  compute    a_ew = a_ew - a_ep*a_pp^(-1)*a_pw
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(i,j,k)
!$OMP DO
       do i=1,nd_bez
       do k=1,npot_w1
         do j=1,npot_p1
           a_ew(i,k) = a_ew(i,k) - a_ep(i,j)*a_pwe(j,k+ncoil)
       enddo
       enddo
       enddo

!$OMP END DO
!$OMP DO
       do i=1,nd_bez
       do k=1,ncoil
         do j=1,npot_p1
           a_ec(i,k) = a_ec(i,k) - a_ep(i,j)*a_pwe(j,k)
       enddo
       enddo
       enddo

!$OMP END DO
!$OMP END PARALLEL
!  compute    a_we = a_wp*a_pp^(-1)*a_pe
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(i,j,k)
!$OMP DO

     do i=1,n_w
      do k= 1,nd_bez
       do j=1,npot_p1                         
        a_we(i,k) = a_we(i,k) +a_wp(i,j)*a_pwe(j,k+n_w)
       enddo
      enddo
     enddo

!$OMP END DO
!$OMP END PARALLEL
!
!    a_ww = a_ww - a_wp*a_pp^(-1)*a_pw

      call dgemm('N','N',n_w,n_w,npot_p1                           &
            ,-1.,a_wp,n_w,a_pwe,npot_p1,1.,a_ww,n_w)

!-------------------------------------------------------
 end subroutine solver1
