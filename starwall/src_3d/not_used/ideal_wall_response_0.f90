      subroutine ideal_wall_response_0
       use icontr
       use contr_su
       use solv 
       use tri_w
       use coil2d
!-----------------------------------------------------------------------
      implicit none
!     real,dimension(:,:),allocatable    :: b0_ee
      integer                            :: i,ier,j,k,nrs,nd,info
       nd  = npot_w
       nrs = 2*N_bnd
       write(6,*) 'solve a0_ww * Phi_w = a0_we'
!    call  dpof('U',a0_ww,nd,nd)
!    call dposm('U',a0_ww,nd,nd,a0_we,nd,nrs)
    call dpotrf('U',nd,a0_ww,nd,info)
    call dpotrs('U',nd,nrs,a0_ww,nd,a0_we,nd,info)
    write(6,*) 'a0_ww * Phi_w = a0_we  done'

!GTAH (debugging)
    if (allocated(b0_ee)) then
      write(6,*) 'CHECK (GTAH) : ',nrs,size(b0_ee,1),size(b0_ee,2)
    else
      write(6,*) 'CHECK (GTAH) : allocating b0_ee ',nrs
      allocate(b0_ee(nrs,nrs))
    endif
         
    b0_ee= a0_ee
!$OMP PARALLEL  DEFAULT(SHARED) PRIVATE(i,j,k)
!$OMP DO

     do i=1,2*N_bnd
     do k=1,2*N_bnd
      do j=1,npot_w
         b0_ee(i,k) = b0_ee(i,k) - a0_ew(i,j)*a0_we(j,k)
      enddo
     enddo
    enddo

!$OMP END DO
!$OMP END PARALLEL
 end subroutine ideal_wall_response_0
