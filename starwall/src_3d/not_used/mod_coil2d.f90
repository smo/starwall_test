module coil2d

   implicit none
   
   ! --- Global for all coil types
   integer                            :: ncoil !< Total number of coils
   integer                            :: ntri_c
   integer,dimension(:  ),allocatable :: jtri_c
   real   ,dimension(:,:),allocatable :: x_coil,y_coil,z_coil,phi_coil
   real,   dimension(:  ),allocatable :: eta_thin_coil
   
   ! --- Data structure for axisymmetric coils
   type :: t_polcoil
    character(len=64) :: name     = 'NONE'
    real              :: R        = -999.
    real              :: Z        = -999.
    real              :: dR       = -999.
    real              :: dZ       = -999.
    integer           :: nbands_R = 0
    integer           :: nbands_Z = 0
    real              :: resist   = -999.  !< Coil resistivity in Ohm
   end type t_polcoil
   
   integer, parameter :: max_polcoils = 100
   type(t_polcoil)    :: polcoil(max_polcoils)
   character(len=256) :: polcoil_set
   integer            :: n_polcoils
   integer            :: polcoils_torpts
   namelist /polcoil_input/ polcoil_set, n_polcoils, polcoil, polcoils_torpts
   
   
   contains
   
   
   subroutine print_coil_debug(filename)
     character(len=*), intent(in) :: filename
     
     integer :: i, j
     
     open(93, file=trim(filename), status='replace')
     111 format(1x,a,99i8)
     112 format(1x,a,i8,a,i8,a,5es23.15)
     write(93,111) 'Number of coils: ', ncoil
     write(93,111)' Number of poloidal field coils: ', n_polcoils
     write(93,111) 'Number of coil triangles: ', ntri_c
     write(93,111) 'Number of triangles per coil: ', jtri_c
     do i = 1, ntri_c
       do j = 1, 3
         write(93,112) 'Triangle', i, ' corner ', j, ': x,y,z,phi=', x_coil(i,j), y_coil(i,j), z_coil(i,j), phi_coil(i,j)
       end do
     end do
     close(93)
   end subroutine print_coil_debug
   
end module coil2d
