!> Construct triangles for one band of one coil
subroutine  coil_2d(xc, yc, zc, nc, cur, xb, yb, zb, phib)

  implicit none

  ! --- Routine parameters
  integer, intent(in)  :: nc
  real,    intent(in)  :: xc(nc,2), yc(nc,2), zc(nc,2)
  real,    intent(in)  :: cur
  real,    intent(out) :: xb(2*nc-2,3), yb(2*nc-2,3), zb(2*nc-2,3), phib(2*nc-2,3)
  
  ! --- Local variables
  real    :: dist
  integer :: j, k

  ! --- Check whether coil is closed in itself
  dist =  (xc(nc,1)-xc(1,1))**2+(yc(nc,1)-yc(1,1))**2               &
             +(zc(nc,1)-zc(1,1))**2+(xc(nc,2)-xc(1,2))**2               &
             +(yc(nc,2)-yc(1,2))**2+(zc(nc,2)-zc(1,2))**2
  if ( dist > 1.e-6 ) then
    write(*,*) 'ERROR in coil_2d: Coil is not closed in itself!'
    write(*,*) 'dist=', dist
    write(*,*) 'xc(nc,1)=', xc(nc,1)
    write(*,*) 'xc(1,1) =', xc(1,1) 
    write(*,*) 'yc(nc,1)=', yc(nc,1)
    write(*,*) 'yc(1,1) =', yc(1,1) 
    write(*,*) 'zc(nc,1)=', zc(nc,1)
    write(*,*) 'zc(1,1) =', zc(1,1) 
    write(*,*) 'xc(nc,2)=', xc(nc,2)
    write(*,*) 'xc(1,2) =', xc(1,2) 
    write(*,*) 'yc(nc,2)=', yc(nc,2)
    write(*,*) 'yc(1,2) =', yc(1,2) 
    write(*,*) 'zc(nc,2)=', zc(nc,2)
    write(*,*) 'zc(1,2) =', zc(1,2) 
    stop
  end if
  
  ! --- Construct triangles
  j = 0
  do k = 1, nc - 1
    
    ! Inside the band, for each rectangle create two triangles:
    !                     
    !          phi=cur    
    !   k,1              k+1,1
    ! ---+----------------+---
    !    . .             .
    !    .   .    T2    . 
    !    .     .       .  
    !    . T1    .    .   
    !    .         . .    
    ! ---+----------+-----
    !   k,2       k+1,2   
    !       phi=0         
    
    ! --- First triangle within present rectangle
    j  = j + 1
    xb(j,1)   = xc(k,  2)
    yb(j,1)   = yc(k,  2)
    zb(j,1)   = zc(k,  2)
    xb(j,2)   = xc(k+1,2)
    yb(j,2)   = yc(k+1,2)
    zb(j,2)   = zc(k+1,2)
    xb(j,3)   = xc(k,  1)
    yb(j,3)   = yc(k,  1)
    zb(j,3)   = zc(k,  1)
    phib(j,1) = cur
    phib(j,2) = cur
    phib(j,3) = 0.
    
    ! --- Second triangle within present rectangle
    j  = j + 1
    xb(j,1)   = xc(k+1,1)
    yb(j,1)   = yc(k+1,1)
    zb(j,1)   = zc(k+1,1)
    xb(j,2)   = xc(k,  1)
    yb(j,2)   = yc(k,  1)
    zb(j,2)   = zc(k,  1)
    xb(j,3)   = xc(k+1,2)
    yb(j,3)   = yc(k+1,2)
    zb(j,3)   = zc(k+1,2)
    phib(j,1) = 0.
    phib(j,2) = 0.
    phib(j,3) = cur
    
  end do

end subroutine  coil_2d
