      subroutine resistive_wall_response_0    
       use icontr
       use solv 
       use contr_su
       use tri_w
       use tri_p
       use coil2d
!-----------------------------------------------------------------------
      implicit none
      integer                         :: i,ier,j,jp,k,nd_wcb,nd,nd_bez,n_w
!-----------------------------------------------------------------------
         nd_bez = 2*N_bnd
         nd_wcb = npot_w+ncoil+nd_bez
      n_w = npot_w

!gtah-start (19-11-2010)     
     if (.not. allocated(gamma)) then
       allocate(gamma(npot_w),S0_ww(npot_w,npot_w))
     else
       write(6,'(A,4i5)') ' WARNING gamma already allocated ',size(gamma),npot_w 
     endif
     if (.not. allocated(S0_ww)) then
       allocate(S0_ww(npot_w,npot_w))
     else
       write(6,'(A,4i5)') ' WARNING S0_ww already allocated ',size(s0_ww,1),npot_w 
     endif     
!gtah-end (19-11-2010)     
     
     call simil_trafo(a0_ww,a0_rw,npot_w,gamma,S0_ww)
     
!gtah-start 18-11-2010
     if (.not. allocated(a_ye)) then
       allocate(a_ye(n_w,nd_bez))
     else
       write(6,'(A,4i5)') ' WARNING a_ye already allocated ',size(a_ye,1),size(a_ye,2), n_w, nd_bez
     endif     
     a_ye = 0.d0
!gtah-end

     open(60,file='eigen_values0_ww',form="formatted",iostat=ier)
     
     write(60,'(i8)') n_w
     do i=1,n_w
        write(60,'(i8,1pe16.8)') i,1./gamma(n_w+1-i)
     enddo
     close(60)
    
     open(60,file='matrixS0_we',form="formatted",iostat=ier)
            write(60,'(2i8)') n_w,nd_bez
     do i=1,n_w
      do k=1,nd_bez
        write(60,'(2i8,1pe16.8)') i,k,a_ye(i,k)
      enddo
     enddo
     close(60)
!-------------------------------------------------------
 end subroutine resistive_wall_response_0    
