      subroutine resistive_wall_response_1
       use icontr
       use solv 
       use contr_su
       use tri_w
       use tri_p
       use coil2d
!-----------------------------------------------------------------------
      implicit none
      integer                         :: i,ier,j,jp,k,n_w,n_we,nd_bez
      nd_bez = nd_harm*N_bnd 
      n_w    = ncoil + npot_w1
      n_we   = n_w+nd_bez     
     allocate(gamma(n_w),S_ww(n_w,n_w))
     call simil_trafo(a_ww,a_rw,n_w,gamma,S_ww)
     allocate(a_ye(n_w,nd_bez),a_ey(nd_bez,n_w))
         a_ye =0.
     do i=1,n_w
      do k=1,nd_bez
       do j=1,n_w
          a_ye(i,k) = a_ye(i,k) + S_ww(j,i)*a_we(j,k)
       enddo
      enddo
     enddo
     do i=1,n_w
      do k=1,nd_bez
         a_ye(i,k) = a_ye(i,k)/gamma(i)	
      enddo
     enddo
!------------------------------------------------------------------
         a_ey = 0.
     do i=1,nd_bez
      do k=1,n_w
       do j=1,n_w
          a_ey(i,k) = a_ey(i,k) + a_ew(i,j)*S_ww(j,k)
       enddo
      enddo
     enddo
!-------------------------------------------------------
 end subroutine resistive_wall_response_1
