! ----------------------------------------------------------------------
subroutine matrix_ew
! ----------------------------------------------------------------------
!     purpose:                                              17/08/09
!
! ----------------------------------------------------------------------
use icontr
use contr_su
use tri_w
use solv
use coil_tria
use mpi_v
use sca
use mpi


! ----------------------------------------------------------------------
implicit none
! ----------------------------------------------------------------------
real,dimension(  :),allocatable :: bx,by,bz
real                            :: pi2,alv,fnv
integer                         :: i,i_start,j, k_start
integer                         :: k,ku,kv,l,nuv
! ----------------------------------------------------------------------

real,dimension(:,:),allocatable :: b_par_loc,b_par0_loc
real,dimension(:,:),allocatable :: a_c_loc,b_c_loc

real,dimension(:,:),allocatable :: a_s1_loc,a_s2_loc,  b_s1_loc, b_s2_loc
real,dimension(:  ),allocatable :: a_ew_arr_s , a_ew_arr_tot
real,dimension(:  ),allocatable :: a_ew_arr_s2, a_ew_arr_tot2
real,dimension(:  ),allocatable :: a_ew_arr_s3, a_ew_arr_tot3
real,dimension(:  ),allocatable :: a_ew_arr_s4, a_ew_arr_tot4

integer :: npot_w_loc_b_m1, npot_w_loc_b_m1_copy

integer   BLACS_PNUM
EXTERNAL  BLACS_PNUM

integer :: ipr_rec= 0, ipc_rec=0 , ip_proc_rec=0
integer :: ipr_rec2=0, ipc_rec2=0, ip_proc_rec2=0
integer :: ipr_rec3=0, ipc_rec3=0, ip_proc_rec3=0
integer :: ipr_rec4=0, ipc_rec4=0, ip_proc_rec4=0
integer ::                         ip_proc_send=0

integer :: i_loc=0 ,j_loc=0 , i_loc2=0, j_loc2=0
integer :: i_loc3=0,j_loc3=0, i_loc4=0, j_loc4=0


if(rank==0) write(6,*) 'compute matrix_ew'

    pi2 = 4.*asin(1.)
    nuv = nu*nv
    fnv = 1./float(nv)
    alv = pi2*fnv
    nd_e = (nd_harm+nd_harm0)/2*n_dof_bnd
    i_start = 1

!==================================================================================
    step=(npot_w)/NPROCS
    npot_w_loc_b=(rank)*step+1
    npot_w_loc_e=(rank+1)*step
    if(rank==NPROCS-1) npot_w_loc_e=npot_w
    npot_w_loc_b_m1=npot_w_loc_b
    if(rank>0) npot_w_loc_b_m1=npot_w_loc_b-1
    npot_w_loc_b_m1_copy=npot_w_loc_b_m1
!==================================================================================



allocate(b_par_loc(nuv,npot_w_loc_b_m1      : npot_w_loc_e),   &
         a_c_loc  (nu,npot_w_loc_b          : npot_w_loc_e),   &
         a_s1_loc (nu,npot_w_loc_b_m1       : npot_w_loc_e),   &
         a_s2_loc (nu,npot_w_loc_b_m1       : npot_w_loc_e),   &
         b_c_loc  (n_dof_bnd,npot_w_loc_b   : npot_w_loc_e),   &
         b_s1_loc (n_dof_bnd,npot_w_loc_b_m1: npot_w_loc_e),   &
         b_s2_loc (n_dof_bnd,npot_w_loc_b_m1: npot_w_loc_e),   &
         a_ew_arr_s (npot_w+ncoil), a_ew_arr_tot(npot_w+ncoil),  &
         a_ew_arr_s2 (npot_w+ncoil), a_ew_arr_tot2(npot_w+ncoil),&
         a_ew_arr_s3 (npot_w+ncoil), a_ew_arr_tot3(npot_w+ncoil),&
         a_ew_arr_s4 (npot_w+ncoil), a_ew_arr_tot4(npot_w+ncoil),&
         stat=ier )
         IF (IER /= 0) THEN
             WRITE (*,*) "Matrrix ew: Can not allocate local matrix b_par_loc,... : MY_PROC_NUM=",MYPNUM
             STOP
         END IF
   
   !Set up scalapack sub grid
   call SCA_GRID(nd_e,npot_w+ncoil)
   ! allocate local matrix
   allocate(a_ew_loc_sca(MP_A,NQ_A), stat=ier)
   IF (IER /= 0) THEN
             WRITE(*,*) "Matrrix ew: Can not allocate local matrix a_ew_loc_sca : MY_PROC_NUM=",MYPNUM, &
                         "MP_A,NQ_A=", MP_A,NQ_A
             STOP
   END IF
   LDA_ew=LDA_A
   a_ew_loc_sca=0.

   b_par_loc = 0.  ; a_c_loc=0.    ; a_s1_loc=0.     ; a_s2_loc=0.; b_c_loc=0.; b_s1_loc=0.; b_s2_loc=0.;
   a_ew_arr_tot=0. ; a_ew_arr_s=0. ; a_ew_arr_tot2=0.; a_ew_arr_s2=0.
   a_ew_arr_tot3=0.; a_ew_arr_s3=0.; a_ew_arr_tot4=0.; a_ew_arr_s4=0.


call bfield_par_par(b_par_loc,x,y,z,xu,yu,zu,nuv,npot_w_loc_b_m1, npot_w_loc_e, xw,yw,zw,ntri_w,jpot_w)

!--------------------------------------------------------------------------
if (n_tor(1).eq.0) then

  allocate(bx(nuv),by(nuv),bz(nuv), stat=ier)

  bx=0.; by=0.; bz=0.
  allocate(b_par0_loc(nuv,npot_w_loc_b:npot_w_loc_e), stat=ier)
       if (ier /= 0) then
             print *,'Matrix_ew.f90: Can not allocate local arrays b_par0_loc', rank
             stop
       endif

  b_par0_loc=0.
  call bfield_c(x,y,z,bx,by,bz,nuv,xw,yw,zw,phi0_w,ntri_w)

  if(1>=npot_w_loc_b) then
     do i=1,nuv
         b_par0_loc(i,1) =  -(xu(i)*bx(i)+yu(i)*by(i)+zu(i)*bz(i))
     enddo
  endif

  if(rank==0) npot_w_loc_b=npot_w_loc_b+1
  do k=npot_w_loc_b,npot_w_loc_e
    do i=1,nuv
       b_par0_loc(i,k) = -b_par_loc(i,k-1)
    enddo
  enddo


  if(rank==0) npot_w_loc_b=npot_w_loc_b-1
  do k=npot_w_loc_b,npot_w_loc_e
   do  ku=1,nu
      do  kv =1,nv
         a_c_loc(ku,k) = a_c_loc(ku,k) + b_par0_loc(ku+nu*(kv-1),k)*fnv
      enddo
    enddo
  enddo

  call real_space2bezier_par(b_c_loc,a_c_loc,N_bnd,n_points,npot_w,n_bnd_nodes,bnd_node,bnd_node_index,n_dof_bnd, &
                             Lij, npot_w_loc_b,npot_w_loc_e)

 do i =1,n_bnd_nodes
    
    k_start = bnd_node_index(i,1)
    j = 1 + (nd_harm+nd_harm0)/2 *(k_start-1) 

    call ScaLAPACK_mapping_i_2(j,i_loc,ipr_rec)
    call ScaLAPACK_mapping_i_2(j+1,i_loc2,ipr_rec2)

    do  k=npot_w_loc_b,npot_w_loc_e

       ip_proc_send=(k-1)/step
       if(ip_proc_send>NPROCS-1) ip_proc_send=NPROCS-1

        if( MYPNUM==ip_proc_send ) then
          a_ew_arr_s (k+ncoil)   = b_c_loc(k_start    , k)
          a_ew_arr_s2(k+ncoil)   = b_c_loc(k_start + 1, k)
       endif

    enddo
     
   CALL MPI_ALLREDUCE(a_ew_arr_s, a_ew_arr_tot, npot_w+ncoil, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
   CALL MPI_ALLREDUCE(a_ew_arr_s2, a_ew_arr_tot2, npot_w+ncoil, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)

 
    do  k=1,npot_w

          call ScaLAPACK_mapping_j_2(k+ncoil,j_loc,ipc_rec)

          j_loc2=j_loc
          ipc_rec2=ipc_rec

          ip_proc_rec  = BLACS_PNUM(CONTEXT, ipr_rec,  ipc_rec)
          ip_proc_rec2 = BLACS_PNUM(CONTEXT, ipr_rec2, ipc_rec2)

          if( MYPNUM==ip_proc_rec)  a_ew_loc_sca(i_loc,j_loc)   = a_ew_arr_tot(k+ncoil)
          if( MYPNUM==ip_proc_rec2) a_ew_loc_sca(i_loc2,j_loc2) = a_ew_arr_tot2(k+ncoil)
          
     enddo
     a_ew_arr_tot=0.;  a_ew_arr_s=0.
     a_ew_arr_tot2=0.; a_ew_arr_s2=0.

  enddo
  i_start =2

  deallocate(bx,by,bz,b_par0_loc,b_c_loc,a_c_loc)


endif
!--------------------------------------------------------------------------


if(rank==0) npot_w_loc_b_m1_copy=npot_w_loc_b_m1_copy+1

do i= i_start , n_harm           !   toroidal harmonics

  a_s1_loc=0.; a_s2_loc=0.; b_s1_loc=0.; b_s2_loc=0.;

  do ku=1,nu
    do kv =1,nv
      j = ku+nu*(kv-1)
      do k=npot_w_loc_b_m1,npot_w_loc_e
        a_s2_loc(ku,k) = a_s2_loc(ku,k) +b_par_loc(j,k)*cos(alv*n_tor(i)*(kv-1))*fnv*2
        a_s1_loc(ku,k) = a_s1_loc(ku,k) +b_par_loc(j,k)*sin(alv*n_tor(i)*(kv-1))*fnv*2
      enddo
    enddo
  enddo


  call real_space2bezier_par(b_s2_loc,a_s2_loc,N_bnd,n_points,npot_w,n_bnd_nodes,bnd_node,bnd_node_index,&
                            n_dof_bnd,Lij, npot_w_loc_b_m1,npot_w_loc_e)     ! cosine part
  call real_space2bezier_par(b_s1_loc,a_s1_loc,N_bnd,n_points,npot_w,n_bnd_nodes,bnd_node,bnd_node_index,&
                            n_dof_bnd,Lij, npot_w_loc_b_m1,npot_w_loc_e)     ! sine part

 do k=1, n_bnd_nodes       

    k_start = bnd_node_index(k,1)
    j = 2*i_start-1 +4*(i-i_start) + (nd_harm+nd_harm0)/2 *(k_start-1)

     call ScaLAPACK_mapping_i_2(j  ,i_loc ,ipr_rec)
     call ScaLAPACK_mapping_i_2(j+1,i_loc2,ipr_rec2)
     call ScaLAPACK_mapping_i_2(j+2,i_loc3,ipr_rec3)
     call ScaLAPACK_mapping_i_2(j+3,i_loc4,ipr_rec4)


   do l=npot_w_loc_b_m1_copy,npot_w_loc_e
      
       ip_proc_send=(l-1)/step
   
    if(ip_proc_send>NPROCS-1) ip_proc_send=NPROCS-1
       if( MYPNUM==ip_proc_send) then 
          a_ew_arr_s (l+ncoil) = b_s2_loc(k_start,  l-1)
          a_ew_arr_s2(l+ncoil) = b_s2_loc(k_start+1,l-1)
          a_ew_arr_s3(l+ncoil) = b_s1_loc(k_start,  l-1)
          a_ew_arr_s4(l+ncoil) = b_s1_loc(k_start+1,l-1)
       endif   
            
   enddo


 CALL MPI_ALLREDUCE(a_ew_arr_s, a_ew_arr_tot, npot_w+ncoil, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
 CALL MPI_ALLREDUCE(a_ew_arr_s2, a_ew_arr_tot2, npot_w+ncoil, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
 CALL MPI_ALLREDUCE(a_ew_arr_s3, a_ew_arr_tot3, npot_w+ncoil, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
 CALL MPI_ALLREDUCE(a_ew_arr_s4, a_ew_arr_tot4, npot_w+ncoil, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)

     do l=2,npot_w

         call ScaLAPACK_mapping_j_2(l+ncoil,j_loc,ipc_rec)
         j_loc2=j_loc
         ipc_rec2=ipc_rec
   
         j_loc3=j_loc
         ipc_rec3=ipc_rec

         j_loc4=j_loc
         ipc_rec4=ipc_rec

         ip_proc_rec  = BLACS_PNUM(CONTEXT, ipr_rec,  ipc_rec)
         ip_proc_rec2 = BLACS_PNUM(CONTEXT, ipr_rec2, ipc_rec2)
         ip_proc_rec3 = BLACS_PNUM(CONTEXT, ipr_rec3, ipc_rec3)
         ip_proc_rec4 = BLACS_PNUM(CONTEXT, ipr_rec4, ipc_rec4)       

         if( MYPNUM==ip_proc_rec)  a_ew_loc_sca(i_loc,j_loc)   = a_ew_arr_tot(l+ncoil)     
         if( MYPNUM==ip_proc_rec2) a_ew_loc_sca(i_loc2,j_loc2) = a_ew_arr_tot2(l+ncoil)
         if( MYPNUM==ip_proc_rec3) a_ew_loc_sca(i_loc3,j_loc3) = a_ew_arr_tot3(l+ncoil)
         if( MYPNUM==ip_proc_rec4) a_ew_loc_sca(i_loc4,j_loc4) = a_ew_arr_tot4(l+ncoil)
        
     enddo


     a_ew_arr_tot=0.;  a_ew_arr_s=0.
     a_ew_arr_tot2=0.; a_ew_arr_s2=0.
     a_ew_arr_tot3=0.; a_ew_arr_s3=0.
     a_ew_arr_tot4=0.; a_ew_arr_s4=0.

 enddo !loop over nodes NOT elements 

enddo  !   toroidal harmonics

deallocate(b_s1_loc, b_s2_loc, a_s1_loc,a_s2_loc, b_par_loc)
deallocate(a_ew_arr_tot, a_ew_arr_tot2,a_ew_arr_tot3,a_ew_arr_tot4)
deallocate(a_ew_arr_s, a_ew_arr_s2, a_ew_arr_s3,a_ew_arr_s4)


! ----------------------------------------------------------------------
if(rank==0) write(6,*) 'matrix_ew done'
if(rank==0) write(*,*) '==============================================================='
end subroutine matrix_ew 

