   subroutine control_array_distribution
! ----------------------------------------------------------------------
   use mpi_v 
   use tri_w
   use tri_p
   use coil_tria
   use sca
   use mpi
   implicit none
   integer :: ierr
    
   step=(npot_w+ncoil)/NPROCS
   if(step<1) then
          if(rank==0) then
              write(6,*) "Number of MPI task more than value of npot_w+ncoil; ",&
                     "npot_w+ncoil=",npot_w+ncoil, "n_task=",NPROCS
              call MPI_BARRIER(MPI_COMM_WORLD,ierr)
              stop
          endif
   endif
       

   step=(npot_p)/NPROCS
   if(step<1) then
          if(rank==0) then
                write(6,*) "Number of MPI task more than value of npot_p; ",&
                           "npot_p=",npot_p, "n_task=",NPROCS
                call MPI_BARRIER(MPI_COMM_WORLD,ierr)
                stop
          endif
   endif

   if(rank==0) write(*,*) "Control array distribution done"
end subroutine control_array_distribution

