module  icontr

   implicit none
   integer,parameter,private :: MAX_N_TOR = 101 ! maximum number of toroidal modes
   integer,parameter         :: MAX_MN_W  = 200 ! maximum number of wall Fourier harmonics
   integer                   :: n_harm,nd_harm,nd_harm0
   integer                   :: nv,iwall,nwall,i_response,nu_coil
   real                      :: delta
   real                      :: eta_thin_w
   integer                   :: n_tor(MAX_N_TOR)
   character(len=256)        :: pol_coil_file, rmp_coil_file, voltage_coil_file, diag_coil_file
   logical                   :: coils_present

end module icontr
