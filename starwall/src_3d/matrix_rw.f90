!----------------------------------------------------------------------
subroutine matrix_rw
! ----------------------------------------------------------------------
!   purpose:                                                  12/03/07
!
! ----------------------------------------------------------------------
      use solv
      use tri_w
      use coil_tria
      use icontr

      use sca
      use mpi_v
! ----------------------------------------------------------------------
      implicit none
      
      real,   dimension(:,:), allocatable :: dxw,dyw,dzw
      integer,dimension(:,:), allocatable :: ipot_w
      real   ,dimension(:  ), allocatable :: area1,jx,jy,jz
      real                                :: temp,aa
      integer                             :: i,j,k,j1,k1

          
      integer :: j_loc,i_loc
      logical :: inside_j,inside_i
 
! ----------------------------------------------------------------------
      if(rank==0) write(*,*) 'compute matrix_rw'
      allocate(dxw(ntri_w,3),dyw(ntri_w,3),dzw(ntri_w,3),area1(ntri_w) & 
              ,jx(ntri_w),jy(ntri_w),jz(ntri_w)                        &
              ,ipot_w(ntri_w,3),stat=ier                               )

      dxw=0.; dyw=0.; dzw=0.; area1=0.
      jx=0.; jy=0.; jz=0.

! ----------------------------------------------------------------------
      ipot_w = jpot_w

      do i = 1,ntri_w
         dxw(i,1) = xw(i,2) - xw(i,3)
         dyw(i,1) = yw(i,2) - yw(i,3)
         dzw(i,1) = zw(i,2) - zw(i,3)
         dxw(i,2) = xw(i,3) - xw(i,1)
         dyw(i,2) = yw(i,3) - yw(i,1)
         dzw(i,2) = zw(i,3) - zw(i,1)
         dxw(i,3) = xw(i,1) - xw(i,2)
         dyw(i,3) = yw(i,1) - yw(i,2)
         dzw(i,3) = zw(i,1) - zw(i,2)
         area1(i) = .5/sqrt( (dyw(i,1)*dzw(i,3)-dzw(i,1)*dyw(i,3))**2   &
                            +(dzw(i,1)*dxw(i,3)-dxw(i,1)*dzw(i,3))**2   &
                            +(dxw(i,1)*dyw(i,3)-dyw(i,1)*dxw(i,3))**2 )
      enddo


     do i = 1,ntri_w
      jx(i) = phi0_w(i,1)*dxw(i,1)+phi0_w(i,2)*dxw(i,2)+phi0_w(i,3)*dxw(i,3)
      jy(i) = phi0_w(i,1)*dyw(i,1)+phi0_w(i,2)*dyw(i,2)+phi0_w(i,3)*dyw(i,3)
      jz(i) = phi0_w(i,1)*dzw(i,1)+phi0_w(i,2)*dzw(i,2)+phi0_w(i,3)*dzw(i,3)
     enddo


      do i=1,ntri_w
       do k=1,3
         if(jpot_w(i,k).eq.npot_w) then
              dxw(i,k) = 0.
              dyw(i,k) = 0.
              dzw(i,k) = 0.
           ipot_w(i,k) = 1
          endif
        enddo
       enddo


   !Set up scalapack sub grid
   call SCA_GRID(npot_w+ncoil,npot_w+ncoil)
   !allocate local matrix
   allocate(a_rw_loc(MP_A,NQ_A), stat=ier)
   IF (IER /= 0) THEN
             WRITE (*,*) "Matrrix rw: Can not allocate local matrix A_loc_rw : MY_PROC_NUM=",MYPNUM,&
                          MP_A,NQ_A
             STOP
   END IF
   LDA_rw=LDA_A
   a_rw_loc=0.

   do i=1,ntri_w
       do k1 = 1,3
           j1 = ipot_w(i,k1) + 1
           
           call ScaLAPACK_mapping_j(j1+ncoil,j_loc,inside_j)
                 if (inside_j) then! If not inside
                       
                    do k = 1,3
                        j  = ipot_w(i,k ) + 1
                        call  ScaLAPACK_mapping_i(j+ncoil,i_loc,inside_i)
                        if (inside_i) then! If not inside 
                             temp = area1(i)*( dxw(i,k)*dxw(i,k1)          &
                                              +dyw(i,k)*dyw(i,k1)          &
                                              +dzw(i,k)*dzw(i,k1) )        &
                                              * eta_thin_wall(i) / eta_thin_w

                             a_rw_loc(i_loc,j_loc) = a_rw_loc(i_loc,j_loc) &
                                                 + temp 
                             
                       endif !(inside_i /= .true.)
                   enddo !k = 1,3
                endif !(inside_j /= .true.)
         enddo
      enddo


    call ScaLAPACK_mapping_i(1+ncoil,i_loc,inside_i)
    if (inside_i) then

        call ScaLAPACK_mapping_j(1+ncoil,j_loc,inside_j)
        if (inside_j) then

            aa = 0.
            do i=1,ntri_w
               aa = aa  +area1(i)*(jx(i)*jx(i)+jy(i)*jy(i)+jz(i)*jz(i)) &
                                                      *eta_thin_wall(i) / eta_thin_w
            enddo

            a_rw_loc(i_loc,j_loc) = aa

        endif
    endif

  
   
      do i =1,ntri_w
       do k = 1,3
           j = ipot_w(i,k)
               call ScaLAPACK_mapping_i(1+ncoil,i_loc,inside_i)
               if (inside_i) then
 
                   call ScaLAPACK_mapping_j(1+j+ncoil,j_loc,inside_j)
                   if (inside_j) then

                     a_rw_loc(i_loc,j_loc) = a_rw_loc(i_loc,j_loc)               &
                        +area1(i)*(jx(i)*dxw(i,k)+jy(i)*dyw(i,k)+jz(i)*dzw(i,k)) &
                        *eta_thin_wall(i) / eta_thin_w

                   endif
                endif


               call ScaLAPACK_mapping_i(1+j+ncoil,i_loc,inside_i)
               if (inside_i) then

                   call ScaLAPACK_mapping_j(1+ncoil,j_loc,inside_j)
                   if (inside_j) then

                     a_rw_loc(i_loc,j_loc) = a_rw_loc(i_loc,j_loc)               &
                        +area1(i)*(jx(i)*dxw(i,k)+jy(i)*dyw(i,k)+jz(i)*dzw(i,k)) &
                                                               *eta_thin_wall(i) / eta_thin_w

                     
                   endif
                endif


       enddo
      enddo


      deallocate(ipot_w,jz,jy,jx,area1,dzw,dyw,dxw)

      if(rank==0) write(*,*) 'matrix_rw done'
      if(rank==0) write(*,*) '==============================================================='

end subroutine matrix_rw
