! ----------------------------------------------------------------------
  subroutine potential(phi,xp,yp,zp,n,x,y,z,ntri,j_pot,n_pot)
! ----------------------------------------------------------------------
!                                                         02.08.10   
!     purpose:
!
!
! ----------------------------------------------------------------------
      implicit none
      real,   dimension (n)       :: xp,yp,zp
      real,   dimension (n,n_pot) :: phi
      real,   dimension (ntri,3)  :: x,y,z
      integer,dimension (ntri,3)  :: j_pot
      integer                     :: n,ntri,n_pot  
! ----------------------------------------------------------------------
!  local  variables
      real              :: x1,y1,z1,x2,y2,z2,x3,y3,z3,sn
      real              :: s1,s2,s3,d221,d232,d213,al1,al2,al3         &
                          ,vx32,vy32,vz32,vx13,vy13,vz13               &
                          ,ata1,ata2,ata3,v21,v32,v13,ata              &
                          ,s21,s22,s23,dp1,dm1,dp2,dm2,dp3,dm3         &
                          ,ap1,am1,ap2,am2,ap3,am3                     &
                          ,h,ln1,ln2,ln3,ar1,ar2,ar3,vx,vy,vz          & 
                          ,x21,y21,z21,x32,y32,z32,x13,y13,z13         &
                          ,tx1,ty1,tz1,tx2,ty2,tz2,tx3,ty3,tz3         &
                          ,nx,ny,nz,pi41,area,d21,d32,d13,temp3        &
                          ,dep1,dep2,dep3,dem1,dem2,dem3,temp1,temp2
      integer           :: i,k,j1,j2,j3
! ----------------------------------------------------------------------
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(                                &
!$OMP                      x1,y1,z1,x2,y2,z2,x3,y3,z3,sn               &
!$OMP                     ,s1,s2,s3,d221,d232,d213,al1,al2,al3         &
!$OMP                     ,vx32,vy32,vz32,vx13,vy13,vz13               &
!$OMP                     ,ata1,ata2,ata3,v21,v32,v13,ata              &
!$OMP                     ,s21,s22,s23,dp1,dm1,dp2,dm2,dp3,dm3         &
!$OMP                     ,ap1,am1,ap2,am2,ap3,am3                     &
!$OMP                     ,h,ln1,ln2,ln3,ar1,ar2,ar3,vx,vy,vz          & 
!$OMP                     ,x21,y21,z21,x32,y32,z32,x13,y13,z13         &
!$OMP                     ,tx1,ty1,tz1,tx2,ty2,tz2,tx3,ty3,tz3         &
!$OMP                     ,nx,ny,nz,pi41,area,d21,d32,d13,temp3        &
!$OMP                     ,dep1,dep2,dep3,dem1,dem2,dem3,temp1,temp2   &
!$OMP                     ,i,k,j1,j2,j3                                )
!$OMP DO
      do k=1,ntri
         x21   = x(k,2) - x(k,1) 
         y21   = y(k,2) - y(k,1) 
         z21   = z(k,2) - z(k,1) 
         x32   = x(k,3) - x(k,2) 
         y32   = y(k,3) - y(k,2) 
         z32   = z(k,3) - z(k,2) 
         x13   = x(k,1) - x(k,3) 
         y13   = y(k,1) - y(k,3) 
         z13   = z(k,1) - z(k,3) 
         d221   =x21**2+y21**2+z21**2
         d232   =x32**2+y32**2+z32**2
         d213   =x13**2+y13**2+z13**2
         d21   = sqrt(d221)
         d32   = sqrt(d232)
         d13   = sqrt(d213)
         nx    = -y21*z13 + z21*y13
         ny    = -z21*x13 + x21*z13
         nz    = -x21*y13 + y21*x13
         area  = 1./sqrt(nx*nx+ny*ny+nz*nz)
         pi41   = .125/asin(1.)

         nx    = nx*area
         ny    = ny*area
         nz    = nz*area
         area  = area*pi41
         tx1   = (y32*nz-z32*ny)
         ty1   = (z32*nx-x32*nz)
         tz1   = (x32*ny-y32*nx)
         tx2   = (y13*nz-z13*ny)
         ty2   = (z13*nx-x13*nz)
         tz2   = (x13*ny-y13*nx)
         tx3   = (y21*nz-z21*ny)
         ty3   = (z21*nx-x21*nz)
         tz3   = (x21*ny-y21*nx)
         j1    = j_pot(k,1)
         j2    = j_pot(k,2)
         j3    = j_pot(k,3)
! ----------------------------------------------------------------------
       do i=1,n
         x1 = x(k,1) - xp(i)  
         y1 = y(k,1) - yp(i)  
         z1 = z(k,1) - zp(i)  
         x2 = x(k,2) - xp(i)  
         y2 = y(k,2) - yp(i)  
         z2 = z(k,2) - zp(i)  
         x3 = x(k,3) - xp(i)  
         y3 = y(k,3) - yp(i)  
         z3 = z(k,3) - zp(i)  
         sn = nx*x1+ny*y1+nz*z1
          h    = abs(sn)
         s21    = x1**2+y1**2+z1**2
         s22    = x2**2+y2**2+z2**2
         s23    = x3**2+y3**2+z3**2
         s1     =sqrt(s21)
         s2     =sqrt(s22)
         s3     =sqrt(s23)
         al1   = alog((s2+s1+d21)/(s1+s2-d21))
         al2   = alog((s3+s2+d32)/(s3+s2-d32)) 
         al3   = alog((s1+s3+d13)/(s1+s3-d13))
!---------------------------------------------------------------------
         ar1   = x1*tx3+y1*ty3+z1*tz3
         ar2   = x2*tx1+y2*ty1+z2*tz1
         ar3   = x3*tx2+y3*ty2+z3*tz2
         dp1   = .5*(s22-s21+d221)
         dp2   = .5*(s23-s22+d232)
         dp3   = .5*(s21-s23+d213)
         dm1   = dp1-d221
         dm2   = dp2-d232
         dm3   = dp3-d213
         ap1   = ar1*dp1
         dep1  = ar1**2+h*d221*(h+s2)
         ap2   = ar2*dp2
         dep2  = ar2**2+h*d232*(h+s3)
         ap3   = ar3*dp3
         dep3  = ar3**2+h*d213*(h+s1)
         am1   = ar1*dm1
         dem1  = ar1**2+h*d221*(h+s1)
         am2   = ar2*dm2
         dem2  = ar2**2+h*d232*(h+s2)
         am3   = ar3*dm3
         dem3  = ar3**2+h*d213*(h+s3)
         ata1  = atan2(ap1*dem1-am1*dep1,dep1*dem1+ap1*am1)
         ata2  = atan2(ap2*dem2-am2*dep2,dep2*dem2+ap2*am2)
         ata3  = atan2(ap3*dem3-am3*dep3,dep3*dem3+ap3*am3)
         ata   = (ata1+ata2+ata3)/h       
         vx    =  -al1*tx3/d21-al2*tx1/d32-al3*tx2/d13
         vy    =  -al1*ty3/d21-al2*ty1/d32-al3*ty2/d13
         vz    =  -al1*tz3/d21-al2*tz1/d32-al3*tz2/d13
         temp1 =  sn*area*(tx1*(vx-x2*ata)+ty1*(vy-y2*ata)+tz1*(vz-z2*ata))
         temp2 =  sn*area*(tx2*(vx-x3*ata)+ty2*(vy-y3*ata)+tz2*(vz-z3*ata))
         temp3 =  sn*area*(tx3*(vx-x1*ata)+ty3*(vy-y1*ata)+tz3*(vz-z1*ata))

!$OMP ATOMIC
         phi(i,j1) = phi(i,j1)+ temp1
!$OMP ATOMIC
         phi(i,j2) = phi(i,j2)+ temp2
!$OMP ATOMIC
         phi(i,j3) = phi(i,j3)+ temp3

       enddo
      enddo
!$OMP END DO
!$OMP END PARALLEL
      end subroutine  potential
