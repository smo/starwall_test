subroutine memory_prediction
! ----------------------------------------------------------------------
!   purpose:                                                  11/12/2017
!   The subroutine is developed to predict the amount of memory (DRAM) 
!   which will be necessaryy for the calculation and the size of the
!   starwall-response.dat output file 
! ----------------------------------------------------------------------

   use mpi_v
   use tri_w
   use tri_p
   use coil_tria
   use resistive
   use solv
   use icontr
   use contr_su
   use mpi

   implicit none
   integer :: ierr
   real*8  :: tot_mem, gbyte_conv, real_npot_p, real_npot_w,  &
              real_nd_bez, real_nd_we, real_nd_w, total_file_size

    100 format(80('-'))
    101 format(5x, a,'(',i6,',',i6,') = ',ES11.3E2,' GB total / ',ES11.3E2, ' GB per MPI task')
    102 format(10x,'TOTAL MEMORY CONSUMPTION PREDICTION = ', ES11.3E2, ' GB total /', ES11.3E2, 'GB per MPI task')
    103 format(10x,'The expected size of the starwall-response.dat output file = ', ES11.3E2, ' GB')

    !1 DP converted to GB 
    gbyte_conv = 8.d0/1024.d0/1024.d0/1024.d0
    nd_w    = ncoil + npot_w
    nd_bez  = (nd_harm+nd_harm0)/2 * n_dof_bnd
    nd_we   = nd_w + nd_bez

    real_nd_bez = real(nd_bez,8); real_npot_p = real(npot_p,8)
    real_npot_w = real(npot_w,8); real_nd_we  = real(nd_we,8)
    real_nd_w   = real(nd_w,8)

    if(rank == 0) then 
      write(*,100)
      write(*,*) 'Predicted memory consumption in STARWALL:'
      write(*,*)
      write(*,101)'s_ww    ', nd_w,   nd_w,   real_nd_w   * real_nd_w   * gbyte_conv, real_nd_w   * real_nd_w   * gbyte_conv / numtasks
      write(*,101)'s_ww_inv', nd_w,   nd_w,   real_nd_w   * real_nd_w   * gbyte_conv, real_nd_w   * real_nd_w   * gbyte_conv / numtasks
      write(*,101)'a_ye    ', nd_w,   nd_bez, real_nd_w   * real_nd_bez * gbyte_conv, real_nd_w   * real_nd_bez * gbyte_conv / numtasks
      write(*,101)'a_ey    ', nd_bez, nd_w,   real_nd_bez * real_nd_w   * gbyte_conv, real_nd_bez * real_nd_w   * gbyte_conv / numtasks
      write(*,101)'a_ee    ', nd_bez, nd_bez, real_nd_bez * real_nd_bez * gbyte_conv, real_nd_bez * real_nd_bez * gbyte_conv / numtasks
      write(*,101)'a_pp    ', npot_p, npot_p, real_npot_p * real_npot_p * gbyte_conv, real_npot_p * real_npot_p * gbyte_conv / numtasks
      write(*,101)'a_pwe   ', npot_p, nd_we,  real_npot_p * real_nd_we  * gbyte_conv, real_npot_p * real_nd_we  * gbyte_conv / numtasks
      write(*,101)'a_ep    ', nd_bez, npot_p, real_nd_bez * real_npot_p * gbyte_conv, real_nd_bez * real_npot_p * gbyte_conv / numtasks
      write(*,101)'a_ew    ', nd_bez, nd_w,   real_nd_bez * real_nd_w   * gbyte_conv, real_nd_bez * real_nd_w   * gbyte_conv / numtasks
      write(*,101)'a_wp    ', nd_w,   npot_p, real_nd_w   * real_npot_p * gbyte_conv, real_nd_w   * real_npot_p * gbyte_conv / numtasks
      write(*,101)'a_ww    ', nd_w,   nd_w,   real_nd_w   * real_nd_w   * gbyte_conv, real_nd_w   * real_nd_w   * gbyte_conv / numtasks
      write(*,101)'a_rw    ', nd_w,   nd_w,   real_nd_w   * real_nd_w   * gbyte_conv, real_nd_w   * real_nd_w   * gbyte_conv / numtasks

      ! total DRAM memory requirements
      tot_mem =  (4.d0*real_nd_w**2 + 3.d0*real_nd_w*real_nd_bez + real_nd_bez**2 + real_npot_p**2 + real_npot_p*real_nd_we + &
                  real_nd_bez*real_npot_p + real_nd_w*real_npot_p) * gbyte_conv 

      ! predicted size of the starwall-response.dat output file 
      total_file_size = (2.d0*real_nd_w**2 + 2.d0*real_nd_w*real_nd_bez + real_nd_bez**2) * gbyte_conv

      write(*,*)
      write(*,102) tot_mem, tot_mem / numtasks
      write(*,103) total_file_size
      write(*,100)
    end if

end subroutine memory_prediction
