!> Write the STARWALL response to an unformatted file using MPI I/O.
!!
!! * Small items will be written by MPI task 0.
!! * Large matrices distributed over the tasks in ScaLapack format are
!!   written by all MPI tasks simultaneously.
subroutine write_response_unformatted

use contr_su
use solv
use tri_w
use coil_tria
use icontr
use sca
use mpi_v
use resistive
use mpi
use write_routines

implicit none

  integer :: i, j
  real, allocatable :: xyzpot_w(:,:)

! Parallel MPI-IO begins here. Open the file.


! If file is already exist. Delete old one and create new one
   call MPI_FILE_DELETE('starwall-response.dat',MPI_INFO_NULL, ier)
   call MPI_FILE_OPEN(MPI_COMM_WORLD, 'starwall-response.dat', &
                       MPI_MODE_WRONLY + MPI_MODE_CREATE + MPI_MODE_EXCL, &
                       MPI_INFO_NULL, file_handle, ier)


! Displacment in bytes from the beggining of the file for later parallel I/O
disp=0

if ( rank == 0 ) then 

  call write_header
  
  call write_int_parameter('file_version',3)
  call write_int_parameter('n_bnd',N_bnd)
  call write_int_parameter('nd_bez',nd_bez)
  call write_int_parameter('ncoil',ncoil)
  call write_int_parameter('npot_w',npot_w)
  call write_int_parameter('n_w',n_w)
  call write_int_parameter('ntri_w',ntri_w)
  call write_int_parameter('n_tor',n_tor_jorek)

  call write_1d_int_arr_sequential('i_tor',1,'int',n_tor_jorek,0,i_tor_jorek)

  call write_int_parameter('ntri_c',ntri_c)
  call write_int_parameter('n_pol_coils',n_pol_coils)
  call write_int_parameter('n_rmp_coils',n_rmp_coils)
  call write_int_parameter('n_voltage_coils',n_voltage_coils)
  call write_int_parameter('n_diag_coils',n_diag_coils)
  call write_int_parameter('ind_start_pol_coils',ind_start_pol_coils)
  call write_int_parameter('ind_start_rmp_coils',ind_start_rmp_coils)
  call write_int_parameter('ind_start_voltage_coils',ind_start_voltage_coils)
  call write_int_parameter('ind_start_diag_coils',ind_start_diag_coils)

  if ( ncoil > 0 ) then
  
      call write_1d_int_arr_sequential  ('jtri_c',     1, 'int',   ncoil,  0, jtri_c)
      call write_2d_float_arr_sequential('x_coil',     2, 'float', ntri_c, 3, x_coil)
      call write_2d_float_arr_sequential('y_coil',     2, 'float', ntri_c, 3, y_coil)
      call write_2d_float_arr_sequential('z_coil',     2, 'float', ntri_c, 3, z_coil)
      call write_2d_float_arr_sequential('phi_coil',   2, 'float', ntri_c, 3, phi_coil)
      call write_1d_float_arr_sequential('eta_thin_coil',1, 'float',ntri_c,0,eta_thin_coil)
      call write_1d_float_arr_sequential('coil_resist',  1, 'float',ncoil,0,coil_resist)
      
  endif

  call write_1d_float_arr_sequential('eta_thin_w',1,'float',1,0,(/eta_thin_w/))
 
  gamma(:)=1.d0/gamma(:)
  call write_1d_float_arr_sequential('yy',1,'float',n_w,0,gamma)
     
endif !if(rank==0)

! Variables for MPI I/O defining Block Cycling distribution schema.
distribs = [MPI_DISTRIBUTE_CYCLIC, MPI_DISTRIBUTE_CYCLIC]
dargs    = [NB, NB]
pdims    = [NPROW, NPCOL]

call SCA_GRID(n_w,nd_bez) ! Calculate the local sizes of distributed matrices
                          ! and store them in variables MP_A, NQ_A 
call write_2d_float_arr_parallel('ye',       2, 'float',   n_w,      nd_bez, MP_A, NQ_A, a_ye_loc)

call SCA_GRID(nd_bez,n_w)
call write_2d_float_arr_parallel('ey',       2, 'float',   nd_bez,   n_w,    MP_A, NQ_A, a_ey_loc)

call SCA_GRID(nd_bez,nd_bez)
call write_2d_float_arr_parallel('ee',       2, 'float',   nd_bez,   nd_bez, MP_A, NQ_A, a_ee_loc)

call SCA_GRID(n_w,n_w)
call write_2d_float_arr_parallel('s_ww',     2, 'float',   n_w,      n_w,    MP_A, NQ_A, s_ww_loc)

call SCA_GRID(n_w,n_w)
call write_2d_float_arr_parallel('s_ww_inv', 2, 'float',   n_w,      n_w,    MP_A, NQ_A, s_ww_inv_loc)

if (rank == 0)   then
  
  allocate( xyzpot_w(npot_w,3) )
  do i = 1, ntri_w
    do j = 1, 3
      xyzpot_w(jpot_w(i,j),:) = (/ xw(i,j), yw(i,j), zw(i,j) /)
    end do
  end do
  call write_2d_float_arr_sequential('xyzpot_w',2,'float',npot_w,3,xyzpot_w)
  deallocate(xyzpot_w)
  
  call write_2d_int_arr_sequential  ('jpot_w',2,'int',ntri_w,3,jpot_w)
  
endif

deallocate (gamma, a_ye_loc, a_ey_loc, a_ee_loc, s_ww_loc, s_ww_inv_loc,jpot_w)

! Close the file
call MPI_BARRIER(MPI_COMM_WORLD, ier)
call MPI_FILE_CLOSE(file_handle, ier)

end subroutine write_response_unformatted
