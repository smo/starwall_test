! The modul consiste of auxilirary varialbes for MPI subroutines
! including the parallel IO and some some variables for
! operations that require parallel computing. 

module mpi_v

         use mpi 
         ! The data type MPI_OFFSET_KIND and MPI_STATUS_SIZE are located in
         ! module mpi library 

         implicit none

         integer :: rank,numtasks,ERRORCODE,ier
         integer :: sqrtnp,step
         integer :: ntri_p_loc_b,ntri_p_loc_e, ntri_w_loc_b,ntri_w_loc_e
        
         ! Variables for parallel IO
         integer :: file_handle,my_darray  
         integer, dimension(2) :: pdims, dims, distribs,dargs
         integer(kind=MPI_OFFSET_KIND) :: disp
         integer, dimension (MPI_STATUS_SIZE) :: status
         integer loc_arr_size
        
end module mpi_v
