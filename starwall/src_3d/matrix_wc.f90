!----------------------------------------------------------------------
 subroutine matrix_wc 
! ----------------------------------------------------------------------
!   purpose:                                                  25/06/02
!
!
! ----------------------------------------------------------------------
      use coil_tria
      use tri_w
      use solv
      use tri
      use tri_b
      use mpi_v
      use mpi
       
! ----------------------------------------------------------------------
      implicit none

      integer                            :: i,ic,ic0,ic1,j,jw,k
      real                               :: dx1,dy1,dz1,dx2,dy2,dz2
      real                               :: dx3,dy3,dz3,temp
      real   ,dimension(:  ),allocatable :: jx,jy,jz
      real   ,dimension(:  ),allocatable :: jx_w,jy_w,jz_w
      real   ,dimension(:,:),allocatable :: dxw,dyw,dzw
     
      real :: dima_sca=0, dima_sca_b=0
      integer :: j_loc,i_loc
      logical :: inside_j,inside_i
      real   ,dimension(:  ),allocatable :: arr,arr_tot




! ----------------------------------------------------------------------
if (rank==0) write(6,*) 'compute matrix_wc'
! ----------------------------------------------------------------------
      allocate(dxw(ntri_w,3),dyw(ntri_w,3),dzw(ntri_w,3)               &
              ,jx(ntri_c),jy(ntri_c),jz(ntri_c)                        &
              ,jx_w(ntri_w),jy_w(ntri_w),jz_w(ntri_w),  stat=ier       )
              if (ier /= 0) then
                  print *,'Matrix_wc.f90: Can not allocate local arrays nx, ..., zm'
                  stop
               endif
 
   
      dxw=0.; dyw=0.; dzw=0.;  jx=0.; jy=0.; jz=0.
      jx_w=0.; jy_w=0.; jz_w=0.

! ----------------------------------------------------------------------

! Additional arrays that will be used for tri_induct subroutine

 allocate(nx(ntri_w),ny(ntri_w),nz(ntri_w),tx1(ntri_w),         &
          ty1(ntri_w),tz1(ntri_w),tx2(ntri_w),ty2(ntri_w),      &
          tz2(ntri_w),tx3(ntri_w),ty3(ntri_w),tz3(ntri_w),      &
          d221(ntri_w),d232(ntri_w),d213(ntri_w),area(ntri_w),  &
          xm(7*ntri_c), ym(7*ntri_c), zm(7*ntri_c),  stat=ier)
          if (ier /= 0) then
             print *,'Matrix_wc.f90: Can not allocate local arrays nx, ..., zm'
             stop
          endif

nx=0.; ny=0.; nz=0.; tx1=0.; ty1=0.;tz1=0.; tx2=0.
ty2=0.; tz2=0.; tx3=0.; ty3=0.; tz3=0.; d221=0.
d232=0.; d213=0.;area=0.; xm=0.; ym=0.; zm=0.


 allocate(nx_b(ntri_c),ny_b(ntri_c),nz_b(ntri_c),tx1_b(ntri_c),         &
          ty1_b(ntri_c),tz1_b(ntri_c),tx2_b(ntri_c),ty2_b(ntri_c),      &
          tz2_b(ntri_c),tx3_b(ntri_c),ty3_b(ntri_c),tz3_b(ntri_c),      &
          d221_b(ntri_c),d232_b(ntri_c),d213_b(ntri_c),area_b(ntri_c),  &
          xm_b(7*ntri_w), ym_b(7*ntri_w), zm_b(7*ntri_w),stat=ier)
          if (ier /= 0) then
             print *,'Matrix_wc.f90: Can not allocate local arrays nx_b, ..., zm_b'
             stop
         endif

nx_b=0.; ny_b=0.; nz_b=0.; tx1_b=0.; ty1_b=0.;tz1_b=0.; tx2_b=0.
ty2_b=0.; tz2_b=0.; tx3_b=0.; ty3_b=0.; tz3_b=0.; d221_b=0.
d232_b=0.; d213_b=0.;area_b=0.; xm_b=0.; ym_b=0.; zm_b=0.
! ----------------------------------------------------------------------



      do i = 1,ntri_w
         dxw(i,1) = xw(i,2) - xw(i,3)
         dyw(i,1) = yw(i,2) - yw(i,3)
         dzw(i,1) = zw(i,2) - zw(i,3)
         dxw(i,2) = xw(i,3) - xw(i,1)
         dyw(i,2) = yw(i,3) - yw(i,1)
         dzw(i,2) = zw(i,3) - zw(i,1)
         dxw(i,3) = xw(i,1) - xw(i,2)
         dyw(i,3) = yw(i,1) - yw(i,2)
         dzw(i,3) = zw(i,1) - zw(i,2)

         call tri_induct_1_b(i,xw,yw,zw,ntri_w)
         call tri_induct_2(ntri_w,i,xw,yw,zw)


      enddo

      do j=1,ntri_c
         dx1 = x_coil(j,2)-x_coil(j,3)
         dy1 = y_coil(j,2)-y_coil(j,3)
         dz1 = z_coil(j,2)-z_coil(j,3)
         dx2 = x_coil(j,3)-x_coil(j,1)
         dy2 = y_coil(j,3)-y_coil(j,1)
         dz2 = z_coil(j,3)-z_coil(j,1)
         dx3 = x_coil(j,1)-x_coil(j,2)
         dy3 = y_coil(j,1)-y_coil(j,2)
         dz3 = z_coil(j,1)-z_coil(j,2)

         jx(j) = dx1*phi_coil(j,1)+dx2*phi_coil(j,2)+dx3*phi_coil(j,3)
         jy(j) = dy1*phi_coil(j,1)+dy2*phi_coil(j,2)+dy3*phi_coil(j,3)
         jz(j) = dz1*phi_coil(j,1)+dz2*phi_coil(j,2)+dz3*phi_coil(j,3)
     
         call tri_induct_1(j,x_coil,y_coil,z_coil ,ntri_c)
         call tri_induct_2_b(ntri_c,j,x_coil,y_coil,z_coil)

      enddo


     ic0 =0
      do j  =1,ncoil
      call ScaLAPACK_mapping_j(j,j_loc,inside_j)
          if (inside_j) then! If not inside
          
             do ic =1,jtri_c(j)
             ic1 = ic0 +ic
                do k=1,3
                  do i=1,ntri_w
                   jw  =jpot_w(i,k)+1 + ncoil
                      call  ScaLAPACK_mapping_i(jw,i_loc,inside_i)
                      if (inside_i) then! If not inside       

                         dima_sca=0
                         dima_sca_b=0

                         call tri_induct_3_2(ntri_w,i,ic1,xw,yw,zw,dima_sca)  !dima
                         call tri_induct_3_2_b(ntri_c,ic1,i,x_coil,y_coil,z_coil,dima_sca_b) !dimb

                         temp =  (dxw(i,k)*jx(ic1)                                   & 
                                + dyw(i,k)*jy(ic1)                                   &
                                + dzw(i,k)*jz(ic1))*.5*(dima_sca+dima_sca_b)
                                 if(jpot_w(i,k).ne.npot_w) &
                         a_ww_loc(i_loc,j_loc) = a_ww_loc(i_loc,j_loc) + temp
     
                      endif    
                   enddo
                   enddo
               enddo
            endif
        ic0 = ic0+jtri_c(j)
      enddo

!---------------------------------------------------------------------------
     do i = 1,ntri_w
      jx_w(i) = phi0_w(i,1)*dxw(i,1)+phi0_w(i,2)*dxw(i,2)+phi0_w(i,3)*dxw(i,3)
      jy_w(i) = phi0_w(i,1)*dyw(i,1)+phi0_w(i,2)*dyw(i,2)+phi0_w(i,3)*dyw(i,3)
      jz_w(i) = phi0_w(i,1)*dzw(i,1)+phi0_w(i,2)*dzw(i,2)+phi0_w(i,3)*dzw(i,3)
     enddo



  call  ScaLAPACK_mapping_i(ncoil+1,i_loc,inside_i)
  if (inside_i) then! If not inside       

     ic0 =0
     do j  =1,ncoil
     call ScaLAPACK_mapping_j(j,j_loc,inside_j)
          if (inside_j) then! If not inside
          
                do ic =1,jtri_c(j)
                   ic1 = ic0 +ic
                   do i=1,ntri_w

                         dima_sca=0
                         dima_sca_b=0

                         call tri_induct_3_2(ntri_w,i,ic1,xw,yw,zw,dima_sca)  !dima
                         call tri_induct_3_2_b(ntri_c,ic1,i,x_coil,y_coil,z_coil,dima_sca_b) !dimb


                        temp =  (jx_w(i)*jx(ic1)                                   &
                               + jy_w(i)*jy(ic1)                                   &
                               + jz_w(i)*jz(ic1))*.5*(dima_sca+dima_sca_b)
                        a_ww_loc(i_loc,j_loc) = a_ww_loc(i_loc,j_loc) + temp
                   enddo
                 enddo
           endif
       ic0 = ic0+jtri_c(j)
      enddo
   endif


   allocate(arr(npot_w),arr_tot(npot_w))
   arr=0.0
   arr_tot=0.0

   do j=1,ncoil
      call ScaLAPACK_mapping_j(j,j_loc,inside_j)
          if (inside_j) then! If not inside
              do i=1,npot_w
                  call  ScaLAPACK_mapping_i(i+ncoil,i_loc,inside_i)
                  if (inside_i) then! If not inside      
                           arr(i)= a_ww_loc(i_loc,j_loc)
                  endif 
              enddo
           endif

        CALL MPI_ALLREDUCE(arr, arr_tot, npot_w, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)

        call  ScaLAPACK_mapping_i(j,i_loc,inside_i)
        if (inside_i) then! If not inside      
            do i=1,npot_w
                  call ScaLAPACK_mapping_j(i+ncoil,j_loc,inside_j)
                  if (inside_j) then! If not inside
                       a_ww_loc(i_loc,j_loc)= arr_tot(i) 
                  endif
            enddo 
        endif

        arr=0.0
        arr_tot=0.0
     
   enddo
      

      deallocate(arr,arr_tot)

      deallocate(nx,ny,nz,tx1,ty1,tz1,tx2,ty2, &
          tz2,tx3,ty3,tz3,d221,d232,d213,area,xm,ym,zm)

      deallocate(nx_b,ny_b,nz_b,tx1_b,ty1_b,tz1_b,tx2_b,ty2_b, &
          tz2_b,tx3_b,ty3_b,tz3_b,d221_b,d232_b,d213_b,area_b,xm_b,ym_b,zm_b)

      deallocate(jz_w,jy_w,jx_w,jz,jy,jx,dzw,dyw,dxw)

if(rank==0) write(*,*) 'matrix_wc done'
if(rank==0) write(*,*) '==============================================================='

 end subroutine  matrix_wc
