#!/bin/bash -l

JOBNAME=$1
jobid=$2
SUCCES="COMPLETED"

echo "Waiting for jobid $jobid to complete ..."
sleep 5s

while true; do
    status=$(sacct -j $jobid -o JobName,State | grep $JOBNAME | awk '{ print $2 }')
    STATUS2=`squeue | grep ${jobid}`
    if [ -z "$STATUS2" ]; then
	if [[ "$status" == *"$SUCCES"* ]]; then
            sleep 2s
	    exit 0
        else
            echo "Something WRONG:"		
	    sleep 2s
            exit 1
	fi    
    else	    
        sleep 10s
	echo ${jobid} ${JOBNAME} ${status}, ${STATUS2}
    fi
done

