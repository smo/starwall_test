#!/bin/bash

echo "------------------ $0 ------------"
cd run-ci

JOBID=`sbatch submit_script_marconi_SKL_STARWALL_big`
sleep 3

JOBID=`echo "$JOBID" | awk '{print $NF}'`

JOBNAME=`squeue |grep $JOBID`
JOBNAME=`echo "$JOBNAME" | awk '{print $3}'`

# now wait for completion -- loop until job in jobid finishes
../wait-for-completion-slurm.sh $JOBNAME $JOBID

if [[ "$?" == "0" ]]; then
    FILE=${JOBNAME}_${JOBID}/starwall-response.dat	
    # check if the results file exist
    if [ -f "$FILE" ]; then
        echo "$FILE results exist"
	echo "ALL FINE"
	exit 0
    else
        echo "Something WRONG Results file is not available:"
        exit 1	
    fi
else
    echo "Something WRONG with code execution"
    exit 1
fi
